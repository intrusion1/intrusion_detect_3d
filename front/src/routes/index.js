import loadable from '@/utils/loadable'

const Index = loadable(() => import(/* webpackChunkName: 'index' */ '@/views/Index'))

// 人脸实时监控
const VideoSurveillance = loadable(() =>
    import(/* webpackChunkName: 'videoSurveillance' */ '@/views/VideoSurveillance')
)

// 人脸底库管理
const FaceLibrary = loadable(() => import(/* webpackChunkName: 'faceLibrary' */ '@/views/FaceLibrary/FaceLibrary'))
const Personnel = loadable(() => import(/* webpackChunkName: 'faceLibrary' */ '@/views/FaceLibrary/Personnel'))
const Organization = loadable(() => import(/* webpackChunkName: 'faceLibrary' */ '@/views/FaceLibrary/Organization'))
const Label = loadable(() => import(/* webpackChunkName: 'faceLibrary' */ '@/views/FaceLibrary/Label'))

// 历史记录
const History = loadable(() => import(/* webpackChunkName: 'history' */ '@/views/History/History'))
const Statistic = loadable(() => import(/* webpackChunkName: 'history' */ '@/views/History/Statistic'))

// 用户管理
const User = loadable(() => import(/* webpackChunkName: 'user' */ '@/views/User'))

//设备管理
const Device = loadable(() => import(/* webpackChunkName: 'device' */ '@/views/Device'))

const FlowTrackScene = loadable(() => import(/* webpackChunkName: 'flow' */ '@/views/Flow/TrackScene'))
const FlowConfiguration = loadable(() => import(/* webpackChunkName: 'flow' */ '@/views/Flow/Configuration'))
const FlowHistory = loadable(() => import(/* webpackChunkName: 'flow' */ '@/views/Flow/History'))

//入侵检测
const IntrusionTrackScene = loadable(() => import(/* webpackChunkName: 'intrusion' */ '@/views/Intrusion/TrackScene'))
const IntrusionConfiguration = loadable(() => import(/* webpackChunkName: 'intrusion' */ '@/views/Intrusion/Configuration'))
const IntrusionHistory = loadable(() => import(/* webpackChunkName: 'intrusion' */ '@/views/Intrusion/History'))

const routes = [
    { path: '/index', exact: true, name: 'Index', component: Index, auth: [1] },

    {
        path: '/surveillance/videoSurveillance',
        exact: false,
        name: '人脸实时监控',
        component: VideoSurveillance,
        auth: [1]
    },

    {
        path: '/surveillance/faceLibrary/faceLibrary',
        exact: false,
        name: '人脸底库管理',
        component: FaceLibrary,
        auth: [1]
    },
    { path: '/surveillance/faceLibrary/personnel', exact: false, name: '人员管理', component: Personnel, auth: [1] },
    {
        path: '/surveillance/faceLibrary/organization',
        exact: false,
        name: '机构管理',
        component: Organization,
        auth: [1]
    },
    { path: '/surveillance/faceLibrary/label', exact: false, name: '标签管理', component: Label, auth: [1] },

    { path: '/surveillance/history/history', exact: false, name: '历史记录', component: History, auth: [1] },
    { path: '/surveillance/history/statistic', exact: false, name: '统计分析', component: Statistic, auth: [1] },
    { path: '/user', exact: false, name: '用户管理', component: User },
    { path: '/device', exact: false, name: '设备管理', component: Device, auth: [1] },

    { path: '/flow/trackScene', exact: false, name: '实时预览', component: FlowTrackScene },
    { path: '/flow/configuration', exact: false, name: '参数配置', component: FlowConfiguration },
    { path: '/flow/history', exact: false, name: '历史记录', component: FlowHistory },

    { path: '/intrusion/trackScene', exact: false, name: '实时预览', component: IntrusionTrackScene },
    { path: '/intrusion/configuration', exact: false, name: '参数配置', component: IntrusionConfiguration },
    { path: '/intrusion/history', exact: false, name: '历史记录', component: IntrusionHistory }
]

export default routes
