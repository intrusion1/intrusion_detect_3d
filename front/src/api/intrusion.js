import { get, post } from '@/utils/baseApi'

export const getIntrusion = params => {
    return get(`/api/intrusion`, params)
}

export const addIntrusion = params => {
    return post(`/api/add_intrusion`, params)
}

export const updateIntrusion = params => {
    return post(`/api/update_intrusion`, params)
}

export const delIntrusion = params => {
    return post(`/api/del_intrusion`, params)
}

export const muldelIntrusion = params => {
    return post(`/api/muldel_intrusion`, params)
}

export const queryIntrusion = params => {
    return post(`/api/query_intrusion`, params)
}

export const screenshotIntrusion = params => {
    return post(`/api/screenshot_intrusion`, params)
}

export const historyIntrusion = params => {
    return get(`/api/history_intrusion`, params)
}

export const queryHistoryIntrusion = params => {
    return post(`/api/query_history_intrusion`, params)
}

export const delHistoryIntrusion = params => {
    return post(`/api/del_history_intrusion`, params)
}

export const muldelHistoryIntrusion = params => {
    return post(`/api/muldel_history_intrusion`, params)
}

export const getIntrusionArea = params => {
    return post(`/api/get_intrusion_area`, params)
}