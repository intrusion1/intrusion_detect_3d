import { get, post } from '@/utils/baseApi'

export const getDevice = params => {
    return get(`/api/device_list`, params)
}

export const addDevice = params => {
    return post(`/api/add_device`, params)
}

export const updateDevice = params => {
    return post(`/api/update_device`, params)
}

export const delDevice = params => {
    return post(`/api/del_device`, params)
}

export const muldelDevice = params => {
    return post(`/api/muldel_device`, params)
}

export const queryDevice = params => {
    return post(`/api/query_device`, params)
}

export const faceRecognition = params => {
    return post(`/api/face_recognition`, params)
}

export const personDetection = params => {
    return post(`/api/update_num`, params)
}

export const intrusionDetect = params => {
    return post(`/api/intrusion_detect`, params)
}