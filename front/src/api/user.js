import { get, post } from '@/utils/baseApi'

export const getUser = params => {
    return get(`/api/user`, params)
}

export const addUser = params => {
    return post(`/api/add_user`, params)
}

export const updateUser = params => {
    return post(`/api/update_user`, params)
}

export const delUser = params => {
    return post(`/api/del_user`, params)
}

export const muldelUser = params => {
    return post(`/api/muldel_user`, params)
}

export const queryUser = params => {
    return post(`/api/query_user`, params)
}

export const userLogin = params => {
    return post(`/api/user_login`, params)
}
