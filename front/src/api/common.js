// export const API = 'http://rap2api.taobao.org/app/mock/234047'

import { post } from '@/utils/baseApi'

export const userLogin = params => {
    return post(`/api/user_login`, params)
}

export const userLogout = params => {
    return post(`/api/user_logout`, params)
}
