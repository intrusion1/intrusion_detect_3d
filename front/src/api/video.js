import { get, post } from '@/utils/baseApi'

export const switchChannel = params => {
    return post(`/api/switch_channel`, params)
}

export const videoFeed = params => {
    return get(`/api/video_feed`, params)
}

export const getHistoryBlacklist = params => {
    return get(`/api/history_blacklist`, params)
}

export const getHistoryWhitelist = params => {
    return get(`/api/history_whitelist`, params)
}

export const switchIntrusionChannel = params => {
    return post(`/api/switch_intrusion_channel`, params)
}
