import { get, post } from '@/utils/baseApi'
import settings from '@/settings'

export const getFaceLib = params => {
    return get(`/api/faceLib`, params)
}

export const addFace = params => {
    return post(`/api/add_face`, params)
}

export const delFace = params => {
    return post(`/api/del_face`, params)
}

export const muldelFace = params => {
    return post(`/api/muldel_face`, params)
}

export const updateFace = params => {
    return post(`/api/update_face`, params)
}

export const queryFace = params => {
    return post(`/api/query_face`, params)
}

export const importFaceApi = () => {
    return `${settings.url}/api/import_face`
}

export const exportFaceApi = () => {
    return `${settings.url}/api/export_face`
}

export const getStoreroom = params => {
    return get(`/api/storeroom`, params)
}

export const addStore = params => {
    return post(`/api/add_store`, params)
}

export const getStoreFace = params => {
    return post(`/api/storeFace`, params)
}

export const appendToStore = params => {
    return post(`/api/append_to_store`, params)
}

export const fromStoreDelFace = params => {
    return post(`/api/from_store_del_face`, params)
}

export const queryStore = params => {
    return post(`/api/query_store`, params)
}

export const delStore = params => {
    return post(`/api/del_store`, params)
}
//
export const muldelStore = params => {
    return post(`/api/muldel_store`, params)
}

export const updateStore = params => {
    return post(`/api/update_store`, params)
}

export const fromStoreMuldelFace = params => {
    return post(`/api/from_store_muldel_face`, params)
}

export const organizationTree = params => {
    return get(`/api/orgTree`, params)
}

export const addNode = params => {
    return post(`/api/add_node`, params)
}

export const delNode = params => {
    return post(`/api/del_node`, params)
}

export const updateNode = params => {
    return post(`/api/update_node`, params)
}

export const getChildrenNode = params => {
    return post(`/api/children_node`, params)
}

export const getLabel = params => {
    return get(`/api/label`, params)
}

export const getPersonLabel = params => {
    return get(`/api/personLabel`, params)
}

export const getStoreLabel = params => {
    return get(`/api/storeLabel`, params)
}

export const addLabel = params => {
    return post(`/api/add_label`, params)
}

export const delLabel = params => {
    return post(`/api/del_label`, params)
}

export const updateLabel = params => {
    return post(`/api/update_label`, params)
}

export const queryLabel = params => {
    return post(`/api/query_label`, params)
}

export const muldelLabel = params => {
    return post(`/api/muldel_label`, params)
}
