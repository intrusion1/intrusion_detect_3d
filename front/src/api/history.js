import { get, post } from '@/utils/baseApi'

export const getHistory = params => {
    return get(`/api/history_face`, params)
}

export const delHistory = params => {
    return post(`/api/del_history`, params)
}

export const muldelHistory = params => {
    return post(`/api/muldel_history`, params)
}

export const queryHistory = params => {
    return post(`/api/query_history`, params)
}

export const statisticHistory = params => {
    return post(`/api/statistic_history`, params)
}
