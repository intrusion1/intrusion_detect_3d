import { get, post } from '@/utils/baseApi'

export const getTraffic = params => {
    return get(`/api/traffic`, params)
}

export const addTraffic = params => {
    return post(`/api/add_traffic`, params)
}

export const updateTraffic = params => {
    return post(`/api/update_traffic`, params)
}

export const delTraffic = params => {
    return post(`/api/del_traffic`, params)
}

export const queryTraffic = params => {
    return post(`/api/query_traffic`, params)
}

export const screenshot = params => {
    return post(`/api/screenshot`, params)
}

export const queryAnydayFlow = params => {
    return post(`/api/query_anyday_flow`, params)
}

export const queryPeriodFlow = params => {
    return post(`/api/query_period_flow`, params)
}

export const getRealTimeFlow = params => {
    return get(`/api/real_time_flow`, params)
}
