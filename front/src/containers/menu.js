const menu = [
    {
        key: '/index',
        title: '首页',
        icon: '',
        auth: [1]
    },
    {
        title: '人脸识别',
        key: '/surveillance',
        icon: '',
        subs: [
            {
                title: '人脸实时监控',
                key: '/surveillance/videoSurveillance',
                icon: ''
            },
            {
                title: '人脸底库管理',
                key: '/surveillance/faceLibrary',
                icon: '',
                subs: [
                    { title: '人员管理', key: '/surveillance/faceLibrary/personnel', icon: '' },
                    { title: '人脸库管理', key: '/surveillance/faceLibrary/faceLibrary', icon: '' },
                    { title: '机构管理', key: '/surveillance/faceLibrary/organization', icon: '' },
                    { title: '标签管理', key: '/surveillance/faceLibrary/label', icon: '' }
                ]
            },
            {
                title: '历史记录',
                key: '/surveillance/history',
                icon: '',
                subs: [
                    { title: '历史', key: '/surveillance/history/history', icon: '' },
                    { title: '统计分析', key: '/surveillance/history/statistic', icon: '' }
                ]
            }
        ]
    },
    {
        title: '用户管理',
        key: '/user',
        icon: ''
    },
    {
        title: '设备管理',
        key: '/device',
        icon: ''
    },
    {
        title: '人流量分析',
        key: '/flow',
        icon: '',
        subs: [
            {
                title: '实时预览',
                key: '/flow/trackScene',
                icon: ''
            },
            {
                title: '参数配置',
                key: '/flow/configuration',
                icon: ''
            },
            {
                title: '历史记录',
                key: '/flow/history',
                icon: ''
            }
        ]
    },
    {
        title: '人员入侵检测',
        key: '/intrusion',
        icon: '',
        subs: [
            {
                title: '实时预览',
                key: '/intrusion/trackScene',
                icon: ''
            },
            {
                title: '参数配置',
                key: '/intrusion/configuration',
                icon: ''
            },
            {
                title: '历史记录',
                key: '/intrusion/history',
                icon: ''
            }
        ]
    }
]

export default menu
