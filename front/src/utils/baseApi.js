import request from './request'

export function get(url, params) {
    return new Promise((resolve, reject) => {
        request
            .get(url, { params })
            .then(res => {
                resolve(res.data)
            })
            .catch(err => {
                reject(err.data)
            })
    })
}

export function post(url, params) {
    return new Promise((resolve, reject) => {
        request
            .post(url, params)
            .then(res => {
                resolve(res.data)
            })
            .catch(err => {
                reject(err.data)
            })
    })
}
