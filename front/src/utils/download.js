import request from './request'

export function download(url, method = 'get') {
    request({
        method,
        url,
        responseType: 'blob'
    }).then(res => {
        console.log(res)
        const type = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8'
        const blob = new Blob([res.data], { type })
        const downloadElement = document.createElement('a')
        const href = window.URL.createObjectURL(blob)
        const contentDisposition = res.headers['content-disposition']
        const patt = new RegExp('filename=([^;]+\\.[^\\.;]+);*')
        const result = patt.exec(contentDisposition)
        const filename = decodeURI(result[1])
        downloadElement.style.display = 'none'
        downloadElement.href = href
        downloadElement.download = filename
        document.body.appendChild(downloadElement)
        downloadElement.click()
        document.body.removeChild(downloadElement)
        window.URL.revokeObjectURL(href)
    })
}
