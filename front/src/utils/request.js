import axios from 'axios'
import settings from '@/settings'
import { message } from 'antd'

// const token = localStorage.getItem('token')

const instance = axios.create({
    baseURL: settings.url,
    timeout: 5000
})

// instance.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded'

instance.interceptors.request.use(
    config => {
        // token && (config.headers.Authorization = token)
        return config
    },
    error => {
        return Promise.reject(error)
    }
)

instance.interceptors.response.use(
    response => {
        return response
    },
    error => {
        console.log(error)
        message.error(error.message)
        return Promise.reject(error)
    }
)

export default instance
