import React from 'react'
import { HashRouter as Router, Route, Switch } from 'react-router-dom'
import loadable from './utils/loadable'
import 'animate.css'
import './style/base.scss'
import './style/App.scss'
import './style/view-style/index.scss'

// 公共模块
const DefaultLayout = loadable(() => import(/* webpackChunkName: 'default' */ './containers'))

// 基础页面
//const View404 = loadable(() => import(/* webpackChunkName: '404' */ './views/Others/404'))
//const View500 = loadable(() => import(/* webpackChunkName: '500' */ './views/Others/500'))
const Login = loadable(() => import(/* webpackChunkName: 'login' */ './views/Login'))

const App = () => (
    <Router>
        <Switch>
            <Route path='/login' component={Login} />
            <Route component={DefaultLayout} />
        </Switch>
    </Router>
)

export default App
