import React, { Component } from 'react'
import { Layout, Row, Col, Divider, List, Select, Tag, Modal, Form, Image } from 'antd'
import { FullscreenOutlined } from '@ant-design/icons'
import { Link } from 'react-router-dom'
import screenfull from 'screenfull'
import ReactPlayer from 'react-player'

import avatar from '@/assets/images/user.jpg'
import middleImg from '@/assets/images/img.png'
import '@/style/view-style/index.scss'
import './index.scss'

import { getDevice } from '@/api/device'
import { switchChannel, videoFeed, getHistoryBlacklist, getHistoryWhitelist } from '@/api/video'

const { Option } = Select

class Index extends Component {
    state = {
        orgTreeList: [],
        deviceList: [],
        blacklist: [],
        whitelist: [],
        avatar,
        middleImg,
        value: null,
        visible: false,
        detailValues: {},
        channel: ''
    }
    componentDidMount = () => {
        this.getDevice()
        this.videoFeed()
        this.getHistoryBlacklist()
        this.getHistoryWhitelist()
        this.timer = setInterval(() => {
            this.getHistoryBlacklist()
            this.getHistoryWhitelist()
        }, 5000)
    }
    componentWillUnmount() {
        this.timer && clearInterval(this.timer)
    }
    getDevice = () => {
        getDevice().then(data => {
            this.setState({ deviceList: data.data })
        })
    }
    videoFeed = () => {
        videoFeed().then(data => {
            //
        })
    }
    getHistoryBlacklist = () => {
        getHistoryBlacklist().then(data => {
            this.setState({ blacklist: data.data })
        })
    }
    getHistoryWhitelist = () => {
        getHistoryWhitelist().then(data => {
            this.setState({ whitelist: data.data })
        })
    }
    fullToggle = () => {
        if (screenfull.isEnabled) {
            screenfull.request(document.getElementById('live'))
        }
    }
    viewInfo = item => {
        this.setState({
            visible: true,
            detailValues: item
        })
    }
    onClose = () => {
        this.setState({
            visible: false
        })
    }
    changeChannel = name => {
        switchChannel({ name }).then(data => {
            const { channel } = data
            this.setState({ channel })
        })
    }
    render() {
        const { visible, detailValues } = this.state

        const ItemList = ({ data, onClick }) => (
            <List
                itemLayout='horizontal'
                className='list'
                dataSource={data.slice(0, 30)}
                renderItem={item => (
                    <List.Item className='list-item'>
                        <List.Item.Meta
                            avatar={<Image className='list-img' src={item.photo} />}
                            title={
                                <span className='link' onClick={this.viewInfo.bind(this, item)}>
                                    {item.label}
                                </span>
                            }
                            description={item.time}
                        />
                    </List.Item>
                )}
            />
        )
        const DetailModal = props => {
            const { visible, values } = props
            const layout = {
                labelCol: { span: 6 },
                wrapperCol: { span: 18 }
            }
            const layoutItem = {
                labelCol: { span: 3 },
                wrapperCol: { span: 21 }
            }

            return (
                <Modal title='详细信息' visible={visible} footer={null} width={600} onCancel={this.onClose}>
                    <Form {...layout}>
                        <Row>
                            <Col span={12}>
                                <Form.Item label='姓名'>
                                    <b className='form-span'>{values.name}</b>
                                </Form.Item>
                            </Col>
                            <Col span={12}>
                                <Form.Item label='性别'>
                                    <span className='form-span'>{values.gender}</span>
                                </Form.Item>
                            </Col>
                            <Col span={12}>
                                <Form.Item label='机构'>
                                    <span className='form-span'>{values.part}</span>
                                </Form.Item>
                            </Col>
                            <Col span={12}>
                                <Form.Item label='年龄'>
                                    <span className='form-span'>{values.age}</span>
                                </Form.Item>
                            </Col>
                            <Col span={12}>
                                <Form.Item label='手机号'>
                                    <span className='form-span'>{values.phone}</span>
                                </Form.Item>
                            </Col>
                            <Col span={12}>
                                <Form.Item label='证件号'>
                                    <span className='form-span'>{values.id}</span>
                                </Form.Item>
                            </Col>
                            <Divider style={{ margin: '0 0 18px' }} />
                            <Col span={24}>
                                <Form.Item label='人脸照片' {...layoutItem}>
                                    <Image className='detail-img' src={values.photo} />
                                </Form.Item>
                            </Col>
                            <Col span={12}>
                                <Form.Item label='创建时间'>
                                    <span className='form-span'>{values.time}</span>
                                </Form.Item>
                            </Col>
                            <Col span={12}>
                                <Form.Item label='标签'>
                                    <Tag color='magenta'>{values.label}</Tag>
                                </Form.Item>
                            </Col>
                            <Col span={24}>
                                <Form.Item label='备注' {...layoutItem}>
                                    <span className='form-span'>{values.remark}</span>
                                </Form.Item>
                            </Col>
                        </Row>
                    </Form>
                </Modal>
            )
        }
        return (
            <Layout className='index animated fadeIn'>
                <div className='container'>
                    <Row className='container-content'>
                        <Col flex='230px' className='height_100'>
                            <div className='container-content_sidebar'>
                                <div className='list-title'>
                                    <span>人脸入侵检测</span>
                                    <Link className='list-title-button' to='/history/historyView'>
                                        更多
                                    </Link>
                                </div>
                                <ItemList data={this.state.blacklist} />
                            </div>
                        </Col>
                        <Col flex='auto' className='height_100'>
                            <div className='container-content_content'>
                                <div className='bar-header content-header'>
                                    <FullscreenOutlined
                                        type='fullscreen'
                                        style={{ cursor: 'pointer', lineHeight: '32px' }}
                                        onClick={this.fullToggle}
                                    />
                                    <div>
                                        <Select
                                            showSearch
                                            className='filter-item'
                                            placeholder='请选择视频流'
                                            optionFilterProp='children'
                                            allowClear
                                            onChange={this.changeChannel}
                                            filterOption={(input, option) =>
                                                option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                                            }>
                                            {this.state.deviceList.map(item => (
                                                <Option value={item.name} key={item.key}>
                                                    {item.name}
                                                </Option>
                                            ))}
                                        </Select>
                                    </div>
                                    <FullscreenOutlined
                                        type='fullscreen'
                                        style={{ cursor: 'pointer', lineHeight: '32px' }}
                                        onClick={this.fullToggle}
                                    />
                                </div>
                                <div className='content-cavans'>
                                    <ReactPlayer
                                        className='content-cavans-live'
                                        url={this.state.channel}
                                        playing
                                        width='100%'
                                        height='98%'
                                        controls
                                        config={{
                                            file: { forceHLS: true }
                                        }}
                                    />
                                    {/* <Image
                                        id='live'
                                        className='content-cavans-live'
                                        src={'http://82.156.19.17:5000/api/video_feed'}
                                        preview={false}
                                    /> */}
                                </div>
                            </div>
                        </Col>
                        <Col flex='230px' className='height_100'>
                            <div className='container-content_sidebar'>
                                <div className='list-title'>
                                    <span>实时识别结果</span>
                                    <Link className='list-title-button' to='/history/historyView'>
                                        更多
                                    </Link>
                                </div>
                                <ItemList data={this.state.whitelist} />
                            </div>
                        </Col>
                    </Row>
                </div>
                <DetailModal visible={visible} values={detailValues} />
                {/* <Modal title='详细信息' visible={visible} footer={null} width={600} onCancel={this.onClose}>
                    <Form {...layout}>
                        <Row>
                            <Col span={12}>
                                <Form.Item label='姓名'>
                                    <b className='form-span'>黄天烁</b>
                                </Form.Item>
                            </Col>
                            <Col span={12}>
                                <Form.Item label='性别'>
                                    <span className='form-span'>男</span>
                                </Form.Item>
                            </Col>
                            <Col span={12}>
                                <Form.Item label='区域'>
                                    <span className='form-span'>北京</span>
                                </Form.Item>
                            </Col>
                            <Col span={12}>
                                <Form.Item label='年龄'>
                                    <span className='form-span'>25</span>
                                </Form.Item>
                            </Col>
                            <Col span={12}>
                                <Form.Item label='证件号'>
                                    <span className='form-span'>1223435345453543</span>
                                </Form.Item>
                            </Col>
                            <Divider style={{ margin: '0 0 18px' }} />
                            <Col span={24}>
                                <Form.Item label='人脸照片' {...layoutItem}>
                                    <Zmage className='detail-img' src={this.state.avatar} />
                                </Form.Item>
                            </Col>
                            <Col span={12}>
                                <Form.Item label='创建时间'>
                                    <span className='form-span'>2021-4-6 11:52:45</span>
                                </Form.Item>
                            </Col>
                            <Col span={12}>
                                <Form.Item label='标签'>
                                    <Tag color='magenta'>magenta</Tag>
                                </Form.Item>
                            </Col>
                            <Col span={24}>
                                <Form.Item label='备注' {...layoutItem}>
                                    <span className='form-span'>
                                        撒旦发射点发，撒旦发射点射点发sdfsdf上射sdfsdfs是，点
                                    </span>
                                </Form.Item>
                            </Col>
                        </Row>
                    </Form>
                </Modal> */}
            </Layout>
        )
    }
}

export default Index
