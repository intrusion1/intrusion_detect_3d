import React, { Component, useEffect } from 'react'
import {
    Layout,
    Divider,
    Table,
    Button,
    Input,
    Modal,
    Form,
    Space,
    message,
    Popconfirm,
    Switch,
    Select,
    Row,
    Col
} from 'antd'
import { PlusOutlined, EditOutlined, DeleteOutlined } from '@ant-design/icons'
import CustomBreadcrumb from '@/components/CustomBreadcrumb'
import '@/style/view-style/table.scss'

import {
    getDevice,
    addDevice,
    updateDevice,
    delDevice,
    muldelDevice,
    queryDevice,
    faceRecognition,
    personDetection,
    intrusionDetect
} from '@/api/device'
import { getStoreroom } from '@/api/faceLibrary'

const { Search } = Input
const { Option } = Select

class Table4 extends Component {
    state = {
        data: [],
        storeData: [],
        filteredInfo: null,
        sortedInfo: null,
        selectedRowKeys: [],
        loading: false,
        visible: false,
        detailVisible: false,
        initialValues: null,
        detailValues: {}
    }
    componentDidMount = () => {
        this.getDevice()
        // this.getStoreroom()
    }
    resetLoading = () => {
        const _this = this
        setTimeout(() => {
            if (_this.state.loading) {
                this.setState({ loading: false })
            }
        }, 3000)
    }
    handleResponse = (data, str = '提交') => {
        if (data.status === 'success') {
            message.success(`${str}成功`)
            this.setState({ visible: false })
            this.getDevice()
        } else {
            message.error(`${str}失败`)
        }
    }
    getDevice = () => {
        this.setState({ loading: true })
        getDevice().then(data => {
            this.setState({ data: data.data, loading: false })
        })
        this.resetLoading()
    }
    getStoreroom = () => {
        getStoreroom().then(data => {
            this.setState({ storeData: data.data })
        })
    }
    addDevice = values => {
        const optApi = this.state.initialValues ? updateDevice : addDevice
        optApi({ ...values }).then(data => {
            this.handleResponse(data, this.state.initialValues ? '修改' : '添加')
        })
    }
    delDevice = record => {
        const { key } = record
        delDevice({ key }).then(data => {
            this.handleResponse(data, '删除')
        })
    }
    muldelDevice = () => {
        muldelDevice({ key: this.state.selectedRowKeys }).then(data => {
            this.handleResponse(data, '删除')
        })
    }
    queryDevice = data => {
        this.setState({ loading: true })
        queryDevice({ name: data }).then(data => {
            this.setState({ data: data.data, loading: false })
        })
        this.resetLoading()
    }
    faceRecognition = name => {
        faceRecognition({ name }).then(data => {
            this.handleResponse(data, '操作')
        })
    }
    personDetection = name => {
        personDetection({ name }).then(data => {
            this.handleResponse(data, '操作')
        })
    }
    intrusionDetect = name => {
        // const values = {
        //             ...this.state.initialValues
        //         }
        //         values.store = values.store ? values.store : undefined
        // if(values.intrusion == '关闭'){
            intrusionDetect({ name }).then(data => {
                this.handleResponse(data, '操作')
            })
        // }

    }

    showModal = initialValues => {
        this.setState({ visible: true, initialValues })
    }
    closeModel = () => {
        this.setState({ visible: false })
    }
    showDetailModal = detailValues => {
        this.setState({ detailVisible: true, detailValues })
    }
    closeDetailModal = () => {
        this.setState({ detailVisible: false })
    }
    onSelectChange = selectedRowKeys => {
        console.log('selectedRowKeys changed: ', selectedRowKeys)
        this.setState({ selectedRowKeys })
    }
    handleChange = (pagination, filters, sorter) => {
        console.log('Various parameters', pagination, filters, sorter)
        this.setState({ filteredInfo: filters, sortedInfo: sorter })
    }
    clearAll = () => {
        this.setState({ filteredInfo: null, sortedInfo: null })
        this.getDevice()
    }
    render() {
        const { visible, loading, data, initialValues, detailValues, detailVisible } = this.state
        let { sortedInfo, selectedRowKeys } = this.state
        sortedInfo = sortedInfo || {}
        const columns = [
            {
                title: '通道名',
                dataIndex: 'name',
                key: 'name',
                sorter: (a, b) => (a.name > b.name ? 1 : -1),
                sortOrder: sortedInfo.columnKey === 'name' && sortedInfo.order,
                ellipsis: true,
                align: 'center',
                render: (value, record) => (
                    <b className='table_title' onClick={this.showDetailModal.bind(this, record)}>
                        {value}
                    </b>
                )
            },
            {
                title: 'ip地址',
                dataIndex: 'address',
                key: 'address',
                align: 'center'
            },
            {
                title: '端口',
                dataIndex: 'port',
                key: 'port',
                align: 'center'
            },
            {
                title: '摄像机账户名',
                dataIndex: 'account',
                key: 'account',
                align: 'center'
            },
            {
                title: '区域',
                dataIndex: 'area',
                key: 'area',
                align: 'center'
            },
            {
                title: '人脸库',
                dataIndex: 'store',
                key: 'store',
                align: 'center'
            },
            {
                title: '人流量分析功能',
                dataIndex: 'detection',
                key: 'detection',
                align: 'center',
                render: (text, record) => (
                    <Switch
                        checkedChildren='开启'
                        unCheckedChildren='关闭'
                        checked={record.detection === '开启'}
                        onClick={this.personDetection.bind(this, record.name)}
                    />
                )
            },
            {
                title: '人脸识别功能',
                dataIndex: 'recognition',
                key: 'recognition',
                align: 'center',
                render: (text, record) => (
                    <Switch
                        checkedChildren='开启'
                        unCheckedChildren='关闭'
                        checked={record.recognition === '开启'}
                        onClick={this.faceRecognition.bind(this, record.name)}
                    />
                )
            },
            {
                title: '区域入侵检测功能',
                dataIndex: 'intrusion',
                key: 'intrusion',
                align: 'center',
                render: (text, record, values) => (
                        <Switch
                            checkedChildren='开启'
                            unCheckedChildren='关闭'
                            checked={record.intrusion === '开启'}
                            onClick={
                                this.intrusionDetect.bind(this, record.name)
                            }

                        />
                )
            },
            {
                title: '操作',
                key: 'action',
                align: 'center',
                render: (text, record) => (
                    <Space size='middle'>
                        <span className='link' onClick={this.showModal.bind(this, record)}>
                            <EditOutlined />
                        </span>
                        <Popconfirm
                            placement='topRight'
                            title='是否确认删除该条信息？'
                            okText='确认'
                            cancelText='取消'
                            onConfirm={this.delDevice.bind(this, record)}>
                            <span className='link danger'>
                                <DeleteOutlined />
                            </span>
                        </Popconfirm>
                    </Space>
                )
            }
        ]
        const rowSelection = {
            selectedRowKeys,
            onChange: this.onSelectChange
        }

        const FormModal = props => {
            const [form] = Form.useForm()
            const { visible, initialValues } = props
            const layout = {
                labelCol: { span: 4 },
                wrapperCol: { span: 19 }
            }
            const tailFormItemLayout = {
                wrapperCol: { span: 19, offset: 4 }
            }

            useEffect(() => {
                const values = {
                    ...initialValues
                }
                values.store = values.store ? values.store : undefined
                if (values.recognition) {
                    values.recognition = values.recognition === '开启'
                }
                if (values.detection) {
                    values.detection = values.detection === '开启'
                }
                if (values.intrusion) {
                    values.intrusion = values.intrusion === '开启'
                }
                form.setFieldsValue(values)
            })
            const resetForm = () => {
                form.resetFields()
            }
            return (
                <Modal
                    visible={visible}
                    title={initialValues ? '修改设备信息' : '添加设备信息'}
                    onCancel={this.closeModel}
                    footer={null}
                    width={700}>
                    <Form {...layout} form={form} layout='horizontal' onFinish={this.addDevice}>
                        <Form.Item name='key' style={{ display: 'none' }} />
                        <Form.Item label='通道名' name='name' rules={[{ required: true, message: '请输入通道名' }]}>
                            <Input allowClear placeholder='请输入通道名' />
                        </Form.Item>
                        <Form.Item label='ip地址' name='address' rules={[{ required: true, message: '请输入ip地址' }]}>
                            <Input allowClear placeholder='请输入ip地址' />
                        </Form.Item>
                        <Form.Item label='端口' name='port' rules={[{ required: true, message: '请输入端口' }]}>
                            <Input allowClear placeholder='请输入端口' />
                        </Form.Item>
                        <Form.Item
                            label='摄像机账户名'
                            name='account'
                            rules={[{ required: true, message: '请输入摄像机账户名' }]}>
                            <Input allowClear placeholder='请输入摄像机账户名' />
                        </Form.Item>
                        <Form.Item
                            label='摄像机密码'
                            name='password'
                            rules={[{ required: true, message: '请输入摄像机密码' }]}>
                            <Input allowClear placeholder='请输入摄像机密码' />
                        </Form.Item>
                        <Form.Item label='人流量分析功能' valuePropName='checked' name='detection'>
                            <Switch />
                        </Form.Item>
                        <Form.Item label='人脸识别功能' valuePropName='checked' name='recognition'>
                            <Switch />
                        </Form.Item>
                        <Form.Item label='区域入侵检测功能' valuePropName='checked' name='intrusion'>
                            <Switch />
                        </Form.Item>
                        <Form.Item label='人脸库' name='store'>
                            <Select
                                showSearch
                                placeholder='请选择人脸库'
                                optionFilterProp='children'
                                filterOption={(input, option) =>
                                    option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                                }>
                                {this.state.storeData.map(item => {
                                    return (
                                        <Option value={item.name} key={item.key}>
                                            {item.name}
                                        </Option>
                                    )
                                })}
                            </Select>
                        </Form.Item>
                        <Form.Item label='摄像机序列号' name='serial'>
                            <Input allowClear placeholder='请输入摄像机序列号' />
                        </Form.Item>
                        <Form.Item label='区域' name='area'>
                            <Input allowClear placeholder='请输入区域' />
                        </Form.Item>
                        <Form.Item label='设备品牌' name='brand'>
                            <Input allowClear placeholder='请输入设备品牌' />
                        </Form.Item>
                        <Form.Item label='设备型号' name='type'>
                            <Input allowClear placeholder='请输入设备型号' />
                        </Form.Item>
                        <Form.Item label='设备版本号' name='version'>
                            <Input allowClear placeholder='请输入设备版本号' />
                        </Form.Item>
                        <Form.Item label='备注' name='note'>
                            <Input.TextArea allowClear placeholder='请输入备注信息' />
                        </Form.Item>
                        <Form.Item {...tailFormItemLayout}>
                            <Button type='primary' loading={loading} style={{ marginRight: 10 }} htmlType='submit'>
                                提交
                            </Button>
                            {initialValues ? null : <Button onClick={resetForm}>清空</Button>}
                        </Form.Item>
                    </Form>
                </Modal>
            )
        }
        const DetailModal = props => {
            const { visible, initialValues } = props
            const values = initialValues
            const layout = {
                labelCol: { span: 6 },
                wrapperCol: { span: 18 }
            }

            return (
                <Modal title='详细信息' visible={visible} footer={null} width={800} onCancel={this.closeDetailModal}>
                    <Form {...layout}>
                        <Row>
                            <Col span={12}>
                                <Form.Item label='通道名'>
                                    <b className='form-span'>{values.name}</b>
                                </Form.Item>
                            </Col>
                            <Col span={12}>
                                <Form.Item label='ip地址'>
                                    <span className='form-span'>{values.address}</span>
                                </Form.Item>
                            </Col>
                            <Col span={12}>
                                <Form.Item label='端口'>
                                    <span className='form-span'>{values.port}</span>
                                </Form.Item>
                            </Col>
                            <Col span={12}>
                                <Form.Item label='摄像机账户名'>
                                    <span className='form-span'>{values.account}</span>
                                </Form.Item>
                            </Col>
                            <Col span={12}>
                                <Form.Item label='摄像机密码'>
                                    <span className='form-span'>{values.password}</span>
                                </Form.Item>
                            </Col>
                            <Col span={12}>
                                <Form.Item label='摄像机序列号'>
                                    <span className='form-span'>{values.serial}</span>
                                </Form.Item>
                            </Col>
                            <Col span={12}>
                                <Form.Item label='区域'>
                                    <span className='form-span'>{values.area}</span>
                                </Form.Item>
                            </Col>
                            <Col span={12}>
                                <Form.Item label='设备品牌'>
                                    <span className='form-span'>{values.brand}</span>
                                </Form.Item>
                            </Col>
                            <Col span={12}>
                                <Form.Item label='设备型号'>
                                    <span className='form-span'>{values.type}</span>
                                </Form.Item>
                            </Col>
                            <Col span={12}>
                                <Form.Item label='设备版本号'>
                                    <span className='form-span'>{values.version}</span>
                                </Form.Item>
                            </Col>
                            <Col span={12}>
                                <Form.Item label='人流量检测功能'>
                                    <span className='form-span'>{values.detection}</span>
                                </Form.Item>
                            </Col>
                            <Col span={12}>
                                <Form.Item label='人脸识别功能'>
                                    <span className='form-span'>{values.recognition}</span>
                                </Form.Item>
                            </Col>
                            <Col span={12}>
                                <Form.Item label='区域入侵检测功能'>
                                    <span className='form-span'>{values.intrusion}</span>
                                </Form.Item>
                            </Col>
                            <Col span={12}>
                                <Form.Item label='人脸库'>
                                    <span className='form-span'>{values.store}</span>
                                </Form.Item>
                            </Col>
                            <Col span={12}>
                                <Form.Item label='备注'>
                                    <span className='form-span'>{values.note}</span>
                                </Form.Item>
                            </Col>
                        </Row>
                    </Form>
                </Modal>
            )
        }
        const TableOpt = () => {
            return (
                <div className='table-operations'>
                    <Search
                        style={{ marginBottom: 10, width: 300 }}
                        allowClear
                        placeholder='搜索'
                        onSearch={this.queryDevice}
                    />
                    <Divider type='vertical' />
                    <Button onClick={this.clearAll}>清除筛选或排序</Button>

                    <Space style={{ float: 'right' }}>
                        {this.state.selectedRowKeys.length ? (
                            <Popconfirm
                                title='是否确认删除选中条目？'
                                okText='确认'
                                cancelText='取消'
                                onConfirm={this.muldelDevice}>
                                <Button
                                    type='primary'
                                    icon={<DeleteOutlined />}
                                    danger
                                    disabled={!this.state.selectedRowKeys.length}>
                                    批量删除
                                </Button>
                            </Popconfirm>
                        ) : (
                            <Button type='primary' icon={<DeleteOutlined />} danger disabled>
                                批量删除
                            </Button>
                        )}
                        <Button type='primary' icon={<PlusOutlined />} onClick={this.showModal.bind(this, null)}>
                            添加
                        </Button>
                    </Space>
                </div>
            )
        }

        return (
            <div>
                <TableOpt />
                <Table
                    className='table'
                    size='middle'
                    columns={columns}
                    dataSource={data}
                    loading={loading}
                    rowSelection={rowSelection}
                    onChange={this.handleChange}
                />
                <FormModal visible={visible} initialValues={initialValues} />
                <DetailModal visible={detailVisible} initialValues={detailValues} />
            </div>
        )
    }
}

const TableView = () => (
    <Layout className='index animated fadeIn'>
        <div>
            <CustomBreadcrumb arr={['设备管理']} />
        </div>
        <div className='face_lib'>
            <div className='base-style'>
                <Table4 />
            </div>
        </div>
    </Layout>
)

export default TableView
