import React, { Component, useEffect } from 'react'
import { Layout, Table, Button, Input, Modal, Form, Space, message, Popconfirm, Select, Row, Col } from 'antd'
import { PlusOutlined, DeleteOutlined, EditOutlined, ReloadOutlined, SearchOutlined } from '@ant-design/icons'
import CustomBreadcrumb from '@/components/CustomBreadcrumb'
import PersonnelTable from './components/Personnel'
import '@/style/view-style/table.scss'

import {
    getStoreroom,
    addStore,
    updateStore,
    delStore,
    muldelStore,
    queryStore,
    getStoreLabel
} from '@/api/faceLibrary'

const { Option } = Select

class Table4 extends Component {
    state = {
        data: [],
        storeLabelData: [],
        filteredInfo: null,
        sortedInfo: null,
        selectedRowKeys: [],
        loading: false,
        visible: false,
        personnelVisible: false,
        initialValues: null,
        curStore: {}
    }
    componentDidMount = () => {
        this.getStoreroom()
        this.getStoreLabel()
    }
    resetLoading = () => {
        const _this = this
        setTimeout(() => {
            if (_this.state.loading) {
                this.setState({ loading: false })
            }
        }, 3000)
    }
    handleResponse = (data, str = '提交') => {
        if (data.status === 'success') {
            message.success(`${str}成功`)
            this.setState({ visible: false })
            this.getStoreroom()
        } else {
            message.error(`${str}失败`)
        }
    }
    getStoreroom = () => {
        this.setState({ loading: true })
        getStoreroom().then(data => {
            this.setState({ data: data.data, loading: false })
        })
        this.resetLoading()
    }
    getStoreLabel = () => {
        getStoreLabel().then(data => {
            this.setState({ storeLabelData: data.data })
        })
    }
    addStore = values => {
        const optApi = this.state.initialValues ? updateStore : addStore
        optApi({ note: '', ...values }).then(data => {
            this.handleResponse(data, this.state.initialValues ? '修改' : '添加')
        })
    }
    delStore = record => {
        const { key } = record
        delStore({ key }).then(data => {
            this.handleResponse(data, '删除')
        })
    }
    muldelStore = () => {
        muldelStore({ key: this.state.selectedRowKeys }).then(data => {
            this.handleResponse(data, '删除')
        })
    }
    queryStore = data => {
        this.setState({ loading: true })
        queryStore(data).then(data => {
            this.setState({ data: data.data, loading: false })
        })
        this.resetLoading()
    }
    showModal = initialValues => {
        this.setState({ visible: true, initialValues })
    }
    closeModel = () => {
        this.setState({ visible: false })
    }
    showPersonnelModal = curStore => {
        this.setState({ personnelVisible: true, curStore })
    }
    closePersonnelModal = () => {
        this.setState({ personnelVisible: false })
    }
    onSelectChange = selectedRowKeys => {
        console.log('selectedRowKeys changed: ', selectedRowKeys)
        this.setState({ selectedRowKeys })
    }
    handleChange = (pagination, filters, sorter) => {
        console.log('Various parameters', pagination, filters, sorter)
        this.setState({ filteredInfo: filters, sortedInfo: sorter })
    }
    clearAll = () => {
        this.setState({ filteredInfo: null, sortedInfo: null })
        this.getStoreroom()
    }
    render() {
        const { visible, personnelVisible, loading, data, initialValues } = this.state
        let { sortedInfo, selectedRowKeys } = this.state
        sortedInfo = sortedInfo || {}
        const columns = [
            {
                title: '人脸库名',
                dataIndex: 'name',
                key: 'name',
                sorter: (a, b) => (a.name > b.name ? 1 : -1),
                sortOrder: sortedInfo.columnKey === 'name' && sortedInfo.order,
                ellipsis: true,
                align: 'center',
                render: (value, record) => (
                    <b className='table_title' onClick={this.showPersonnelModal.bind(this, record)}>
                        {value}
                    </b>
                )
            },
            {
                title: '标签',
                dataIndex: 'label',
                key: 'label',
                align: 'center'
            },
            {
                title: '创建时间',
                dataIndex: 'time',
                key: 'time',
                align: 'center',
                sorter: (a, b) => (a.time > b.time ? 1 : -1),
                sortOrder: sortedInfo.columnKey === 'time' && sortedInfo.order
            },
            {
                title: '备注',
                dataIndex: 'note',
                key: 'note',
                align: 'center'
            },
            {
                title: '操作',
                key: 'action',
                align: 'center',
                render: (text, record) => (
                    <Space size='middle'>
                        <span className='link' onClick={this.showModal.bind(this, record)}>
                            <EditOutlined />
                        </span>
                        <Popconfirm
                            placement='topRight'
                            title='是否确认删除该条信息？'
                            okText='确认'
                            cancelText='取消'
                            onConfirm={this.delStore.bind(this, record)}>
                            <span className='link danger'>
                                <DeleteOutlined />
                            </span>
                        </Popconfirm>
                    </Space>
                )
            }
        ]
        const rowSelection = {
            selectedRowKeys,
            onChange: this.onSelectChange
        }

        const FormModal = props => {
            const [form] = Form.useForm()
            const { visible, initialValues } = props
            const layout = {
                labelCol: { span: 4 },
                wrapperCol: { span: 19 }
            }
            const tailFormItemLayout = {
                wrapperCol: { span: 19, offset: 4 }
            }

            useEffect(() => {
                form.setFieldsValue(initialValues)
            })
            const resetForm = () => {
                form.resetFields()
            }

            return (
                <Modal
                    visible={visible}
                    title={initialValues ? '修改人脸库' : '创建人脸库'}
                    onCancel={this.closeModel}
                    footer={null}>
                    <Form
                        {...layout}
                        form={form}
                        initialValues={initialValues}
                        layout='horizontal'
                        onFinish={this.addStore}>
                        <Form.Item name='key' style={{ display: 'none' }} />
                        <Form.Item label='人脸库名' name='name' rules={[{ required: true, message: '请输入人脸库名' }]}>
                            <Input allowClear placeholder='请输入人脸库名' />
                        </Form.Item>
                        <Form.Item label='标签' name='label' rules={[{ required: true, message: '请选择标签' }]}>
                            <Select showSearch allowClear placeholder='请选择标签'>
                                {this.state.storeLabelData.map(item => (
                                    <Option value={item.name} key={item.key}>
                                        {item.name}
                                    </Option>
                                ))}
                            </Select>
                        </Form.Item>
                        <Form.Item label='备注' name='note'>
                            <Input.TextArea allowClear placeholder='请输入备注信息' />
                        </Form.Item>
                        <Form.Item {...tailFormItemLayout}>
                            <Button type='primary' loading={loading} style={{ marginRight: 10 }} htmlType='submit'>
                                提交
                            </Button>
                            {initialValues ? null : <Button onClick={resetForm}>清空</Button>}
                        </Form.Item>
                    </Form>
                </Modal>
            )
        }
        const PersonnelModal = props => {
            const { visible } = props

            return (
                <Modal
                    visible={visible}
                    footer={null}
                    width={1200}
                    title={this.state.curStore.name}
                    onCancel={this.closePersonnelModal}>
                    <PersonnelTable storeKey={this.state.curStore.key} />
                </Modal>
            )
        }
        const TableOpt = () => {
            const [form] = Form.useForm()
            return (
                <div className='table-operations'>
                    <Form form={form} onFinish={this.queryStore}>
                        <Row gutter={24}>
                            <Col xs={24} sm={12} md={12} lg={8} xl={4}>
                                <Form.Item label='人脸库名' name='name'>
                                    <Input />
                                </Form.Item>
                            </Col>
                            <Col xs={24} sm={12} md={12} lg={8} xl={4}>
                                <Form.Item label='标签' name='label'>
                                    <Select showSearch allowClear>
                                        {this.state.storeLabelData.map(item => (
                                            <Option value={item.name} key={item.key}>
                                                {item.name}
                                            </Option>
                                        ))}
                                    </Select>
                                </Form.Item>
                            </Col>
                            <Col xs={24} sm={12} md={12} lg={8} xl={16}>
                                <Space style={{ float: 'right' }}>
                                    <Button type='primary' htmlType='submit' icon={<SearchOutlined />}>
                                        搜索
                                    </Button>
                                </Space>
                            </Col>
                            <Col span={24} style={{ marginBottom: 10 }}>
                                <Space>
                                    <Button icon={<ReloadOutlined />} onClick={this.clearAll}>
                                        清除筛选或排序
                                    </Button>
                                </Space>
                                <Space style={{ float: 'right' }}>
                                    {this.state.selectedRowKeys.length ? (
                                        <Popconfirm
                                            title='是否确认删除选中条目？'
                                            okText='确认'
                                            cancelText='取消'
                                            onConfirm={this.muldelStore}>
                                            <Button
                                                type='primary'
                                                icon={<DeleteOutlined />}
                                                danger
                                                disabled={!this.state.selectedRowKeys.length}>
                                                批量删除
                                            </Button>
                                        </Popconfirm>
                                    ) : (
                                        <Button type='primary' icon={<DeleteOutlined />} danger disabled>
                                            批量删除
                                        </Button>
                                    )}
                                    <Button
                                        type='primary'
                                        icon={<PlusOutlined />}
                                        onClick={this.showModal.bind(this, null)}>
                                        创建人脸库
                                    </Button>
                                </Space>
                            </Col>
                        </Row>
                    </Form>
                </div>
            )
        }

        return (
            <div>
                <TableOpt />
                <Table
                    className='table'
                    size='middle'
                    columns={columns}
                    dataSource={data}
                    loading={loading}
                    rowSelection={rowSelection}
                    onChange={this.handleChange}
                />
                <FormModal visible={visible} initialValues={initialValues} />
                <PersonnelModal visible={personnelVisible} />
            </div>
        )
    }
}

const TableView = () => (
    <Layout className='index animated fadeIn'>
        <div>
            <CustomBreadcrumb arr={['人脸库管理']} />
        </div>
        <div className='face_lib'>
            <div className='base-style'>
                <Table4 />
            </div>
        </div>
    </Layout>
)

export default TableView
