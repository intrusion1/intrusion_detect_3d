import React, { Component, useEffect } from 'react'
import { Layout, Table, Button, Input, Modal, Form, Space, message, Popconfirm, Select, Row, Col } from 'antd'
import { PlusOutlined, EditOutlined, DeleteOutlined, ReloadOutlined, SearchOutlined } from '@ant-design/icons'
import CustomBreadcrumb from '@/components/CustomBreadcrumb'
import '@/style/view-style/table.scss'

import { addLabel, updateLabel, delLabel, muldelLabel, getLabel, queryLabel, getStoreroom } from '@/api/faceLibrary'

const { Option } = Select

class Table4 extends Component {
    state = {
        data: [],
        storeData: [],
        filteredInfo: null,
        sortedInfo: null,
        selectedRowKeys: [],
        loading: false,
        visible: false,
        detailVisible: false,
        initialValues: null,
        detailValues: {}
    }
    componentDidMount = () => {
        this.getLabel()
        this.getStoreroom()
    }
    resetLoading = () => {
        const _this = this
        setTimeout(() => {
            if (_this.state.loading) {
                this.setState({ loading: false })
            }
        }, 3000)
    }
    handleResponse = (data, str = '提交') => {
        if (data.status === 'success') {
            message.success(`${str}成功`)
            this.setState({ visible: false })
            this.getLabel()
        } else {
            message.error(`${str}失败`)
        }
    }
    getLabel = () => {
        this.setState({ loading: true })
        getLabel().then(data => {
            this.setState({ data: data.data, loading: false })
        })
        this.resetLoading()
    }
    getStoreroom = () => {
        getStoreroom().then(data => {
            this.setState({ storeData: data.data })
        })
    }
    addLabel = values => {
        const optApi = this.state.initialValues ? updateLabel : addLabel
        optApi({ ...values }).then(data => {
            this.handleResponse(data, this.state.initialValues ? '修改' : '添加')
        })
    }
    delLabel = record => {
        const { key } = record
        delLabel({ key }).then(data => {
            this.handleResponse(data, '删除')
        })
    }
    muldelLabel = () => {
        muldelLabel({ key: this.state.selectedRowKeys }).then(data => {
            this.handleResponse(data, '删除')
        })
    }
    queryLabel = data => {
        this.setState({ loading: true })
        queryLabel(data).then(data => {
            this.setState({ data: data.data, loading: false })
        })
        this.resetLoading()
    }
    showModal = initialValues => {
        this.setState({ visible: true, initialValues })
    }
    closeModel = () => {
        this.setState({ visible: false })
    }
    showDetailModal = detailValues => {
        this.setState({ detailVisible: true, detailValues })
    }
    closeDetailModal = () => {
        this.setState({ detailVisible: false })
    }
    onSelectChange = selectedRowKeys => {
        console.log('selectedRowKeys changed: ', selectedRowKeys)
        this.setState({ selectedRowKeys })
    }
    handleChange = (pagination, filters, sorter) => {
        console.log('Various parameters', pagination, filters, sorter)
        this.setState({ filteredInfo: filters, sortedInfo: sorter })
    }
    clearAll = () => {
        this.setState({ filteredInfo: null, sortedInfo: null })
        this.getLabel()
    }
    render() {
        const { visible, loading, data, initialValues, detailValues, detailVisible } = this.state
        let { sortedInfo, selectedRowKeys } = this.state
        sortedInfo = sortedInfo || {}
        const columns = [
            {
                title: '标签名',
                dataIndex: 'name',
                key: 'name',
                sorter: (a, b) => (a.name > b.name ? 1 : -1),
                sortOrder: sortedInfo.columnKey === 'name' && sortedInfo.order,
                ellipsis: true,
                align: 'center',
                render: (value, record) => <b className='table_title'>{value}</b>
            },
            {
                title: '标签类型',
                dataIndex: 'type',
                key: 'type',
                align: 'center'
            },
            {
                title: '时间',
                dataIndex: 'time',
                key: 'time',
                align: 'center'
            },
            {
                title: '操作',
                key: 'action',
                align: 'center',
                render: (text, record) => (
                    <Space size='middle'>
                        <span className='link' onClick={this.showModal.bind(this, record)}>
                            <EditOutlined />
                        </span>
                        <Popconfirm
                            placement='topRight'
                            title='是否确认删除该条信息？'
                            okText='确认'
                            cancelText='取消'
                            onConfirm={this.delLabel.bind(this, record)}>
                            <span className='link danger'>
                                <DeleteOutlined />
                            </span>
                        </Popconfirm>
                    </Space>
                )
            }
        ]
        const rowSelection = {
            selectedRowKeys,
            onChange: this.onSelectChange
        }

        const FormModal = props => {
            const [form] = Form.useForm()
            const { visible, initialValues } = props
            const layout = {
                labelCol: { span: 4 },
                wrapperCol: { span: 19 }
            }
            const tailFormItemLayout = {
                wrapperCol: { span: 19, offset: 4 }
            }

            useEffect(() => {
                const values = {
                    ...initialValues
                }
                values.store = values.store ? values.store : undefined
                if (values.recognition) {
                    values.recognition = values.recognition === '开启' ? true : false
                }
                form.setFieldsValue(values)
            })
            const resetForm = () => {
                form.resetFields()
            }

            return (
                <Modal
                    visible={visible}
                    title={initialValues ? '修改标签信息' : '添加标签信息'}
                    onCancel={this.closeModel}
                    footer={null}
                    width={600}>
                    <Form {...layout} form={form} layout='horizontal' onFinish={this.addLabel}>
                        <Form.Item name='key' style={{ display: 'none' }} />
                        <Form.Item label='标签名' name='name' rules={[{ required: true, message: '请输入标签名' }]}>
                            <Input allowClear placeholder='请输入标签名' />
                        </Form.Item>
                        <Form.Item label='标签类型' name='type' rules={[{ required: true, message: '请选择标签类型' }]}>
                            <Select showSearch allowClear placeholder='请选择标签类型'>
                                <Option value='人员标签' key='人员标签'>
                                    人员标签
                                </Option>
                                <Option value='人脸库标签' key='人脸库标签'>
                                    人脸库标签
                                </Option>
                            </Select>
                        </Form.Item>
                        <Form.Item {...tailFormItemLayout}>
                            <Button type='primary' loading={loading} style={{ marginRight: 10 }} htmlType='submit'>
                                提交
                            </Button>
                            {initialValues ? null : <Button onClick={resetForm}>清空</Button>}
                        </Form.Item>
                    </Form>
                </Modal>
            )
        }
        const DetailModal = props => {
            const { visible, initialValues } = props
            const values = initialValues
            const layout = {
                labelCol: { span: 6 },
                wrapperCol: { span: 18 }
            }

            return (
                <Modal title='详细信息' visible={visible} footer={null} width={800} onCancel={this.closeDetailModal}>
                    <Form {...layout}>
                        <Row>
                            <Col span={12}>
                                <Form.Item label='通道名'>
                                    <b className='form-span'>{values.name}</b>
                                </Form.Item>
                            </Col>
                            <Col span={12}>
                                <Form.Item label='协议'>
                                    <span className='form-span'>{values.protocol}</span>
                                </Form.Item>
                            </Col>
                            <Col span={12}>
                                <Form.Item label='ip地址'>
                                    <span className='form-span'>{values.address}</span>
                                </Form.Item>
                            </Col>
                            <Col span={12}>
                                <Form.Item label='端口'>
                                    <span className='form-span'>{values.port}</span>
                                </Form.Item>
                            </Col>
                            <Col span={12}>
                                <Form.Item label='摄像机账户名'>
                                    <span className='form-span'>{values.account}</span>
                                </Form.Item>
                            </Col>
                            <Col span={12}>
                                <Form.Item label='摄像机密码'>
                                    <span className='form-span'>{values.password}</span>
                                </Form.Item>
                            </Col>
                            <Col span={12}>
                                <Form.Item label='摄像机序列号'>
                                    <span className='form-span'>{values.serial}</span>
                                </Form.Item>
                            </Col>
                            <Col span={12}>
                                <Form.Item label='区域'>
                                    <span className='form-span'>{values.area}</span>
                                </Form.Item>
                            </Col>
                            <Col span={12}>
                                <Form.Item label='设备品牌'>
                                    <span className='form-span'>{values.brand}</span>
                                </Form.Item>
                            </Col>
                            <Col span={12}>
                                <Form.Item label='设备型号'>
                                    <span className='form-span'>{values.type}</span>
                                </Form.Item>
                            </Col>
                            <Col span={12}>
                                <Form.Item label='设备版本号'>
                                    <span className='form-span'>{values.version}</span>
                                </Form.Item>
                            </Col>
                            {/* <Col span={12}>
                                <Form.Item label='人脸识别功能'>
                                    <span className='form-span'>{values.recognition}</span>
                                </Form.Item>
                            </Col>
                            <Col span={12}>
                                <Form.Item label='人脸库'>
                                    <span className='form-span'>{values.store}</span>
                                </Form.Item>
                            </Col> */}
                            <Col span={12}>
                                <Form.Item label='备注'>
                                    <span className='form-span'>{values.note}</span>
                                </Form.Item>
                            </Col>
                        </Row>
                    </Form>
                </Modal>
            )
        }
        const TableOpt = () => {
            const [form] = Form.useForm()
            return (
                <div className='table-operations'>
                    <Form form={form} onFinish={this.queryLabel}>
                        <Row gutter={24}>
                            <Col xs={24} sm={12} md={12} lg={8} xl={4}>
                                <Form.Item label='标签名' name='name'>
                                    <Input />
                                </Form.Item>
                            </Col>
                            <Col xs={24} sm={12} md={12} lg={8} xl={4}>
                                <Form.Item label='标签类型' name='type'>
                                    <Select showSearch allowClear>
                                        <Option value='人员标签' key='人员标签'>
                                            人员标签
                                        </Option>
                                        <Option value='人脸库标签' key='人脸库标签'>
                                            人脸库标签
                                        </Option>
                                    </Select>
                                </Form.Item>
                            </Col>
                            <Col xs={24} sm={12} md={12} lg={8} xl={16}>
                                <Space style={{ float: 'right' }}>
                                    <Button type='primary' htmlType='submit' icon={<SearchOutlined />}>
                                        搜索
                                    </Button>
                                </Space>
                            </Col>
                            <Col span={24} style={{ marginBottom: 10 }}>
                                <Space>
                                    <Button icon={<ReloadOutlined />} onClick={this.clearAll}>
                                        清除筛选或排序
                                    </Button>
                                </Space>
                                <Space style={{ float: 'right' }}>
                                    {this.state.selectedRowKeys.length ? (
                                        <Popconfirm
                                            title='是否确认删除选中条目？'
                                            okText='确认'
                                            cancelText='取消'
                                            onConfirm={this.muldelLabel}>
                                            <Button
                                                type='primary'
                                                icon={<DeleteOutlined />}
                                                danger
                                                disabled={!this.state.selectedRowKeys.length}>
                                                批量删除
                                            </Button>
                                        </Popconfirm>
                                    ) : (
                                        <Button type='primary' icon={<DeleteOutlined />} danger disabled>
                                            批量删除
                                        </Button>
                                    )}
                                    <Button
                                        type='primary'
                                        icon={<PlusOutlined />}
                                        onClick={this.showModal.bind(this, null)}>
                                        添加
                                    </Button>
                                </Space>
                            </Col>
                        </Row>
                    </Form>
                </div>
            )
        }

        return (
            <div>
                <TableOpt />
                <Table
                    className='table'
                    size='middle'
                    columns={columns}
                    dataSource={data}
                    loading={loading}
                    rowSelection={rowSelection}
                    onChange={this.handleChange}
                />
                <FormModal visible={visible} initialValues={initialValues} />
                <DetailModal visible={detailVisible} initialValues={detailValues} />
            </div>
        )
    }
}

const TableView = () => (
    <Layout className='index animated fadeIn'>
        <div>
            <CustomBreadcrumb arr={['标签管理']} />
        </div>
        <div className='face_lib'>
            <div className='base-style'>
                <Table4 />
            </div>
        </div>
    </Layout>
)

export default TableView
