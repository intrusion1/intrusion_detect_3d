import React, { Component, useEffect } from 'react'
import {
    Layout,
    Table,
    Button,
    Divider,
    Input,
    Modal,
    Form,
    Select,
    TreeSelect,
    InputNumber,
    Upload,
    Space,
    message,
    Tag,
    Popconfirm,
    Row,
    Col,
    Image
} from 'antd'
import {
    DownloadOutlined,
    UploadOutlined,
    PlusOutlined,
    EditOutlined,
    DeleteOutlined,
    SelectOutlined,
    SearchOutlined,
    ReloadOutlined
} from '@ant-design/icons'
import CustomBreadcrumb from '@/components/CustomBreadcrumb'
import '@/style/view-style/table.scss'

import {
    getStoreroom,
    getPersonLabel,
    getFaceLib,
    addFace,
    updateFace,
    delFace,
    muldelFace,
    queryFace,
    importFaceApi,
    exportFaceApi,
    appendToStore,
    organizationTree
} from '@/api/faceLibrary'

const { Option } = Select
const { TreeNode } = TreeSelect

class Table4 extends Component {
    state = {
        data: [],
        storeData: [],
        orgTree: [],
        personLabelData: [],
        filteredInfo: null,
        sortedInfo: null,
        selectedRowKeys: [],
        loading: false,
        visible: false,
        detailVisible: false,
        storeVisible: false,
        initialValues: null,
        detailValues: {},

        treeValue: null
    }
    componentDidMount = () => {
        this.getFaceLib()
        this.getStoreroom()
        this.organizationTree()
        this.getPersonLabel()
    }
    resetLoading = () => {
        const _this = this
        setTimeout(() => {
            if (_this.state.loading) {
                this.setState({ loading: false })
            }
        }, 3000)
    }
    handleResponse = (data, str = '提交') => {
        if (data.status === 'success') {
            message.success(`${str}成功`)
            this.setState({ visible: false })
            this.getFaceLib()
        } else {
            message.error(`${str}失败`)
        }
    }
    getFaceLib = () => {
        this.setState({ loading: true })
        getFaceLib().then(data => {
            this.setState({ data: data.data, loading: false })
        })
        this.resetLoading()
    }
    getStoreroom = () => {
        getStoreroom().then(data => {
            this.setState({ storeData: data.data })
        })
    }
    organizationTree = () => {
        organizationTree().then(data => {
            this.setState({ orgTree: data.data })
        })
    }
    getPersonLabel = () => {
        getPersonLabel().then(data => {
            this.setState({ personLabelData: data.data })
        })
    }
    addFace = values => {
        if (values.photo[0].thumbUrl) {
            values.photo = values.photo[0].thumbUrl
        } else if (values.photo[0].url) {
            values.photo = values.photo[0].url
        }
        const optApi = this.state.initialValues ? updateFace : addFace
        optApi({ ...values }).then(data => {
            this.handleResponse(data, this.state.initialValues ? '修改' : '添加')
        })
    }
    delFace = record => {
        const { key } = record
        delFace({ key }).then(data => {
            this.handleResponse(data, '删除')
        })
    }
    muldelFace = () => {
        muldelFace({ key: this.state.selectedRowKeys }).then(data => {
            this.handleResponse(data, '删除')
        })
    }
    queryFace = data => {
        this.setState({ loading: true })
        queryFace(data).then(data => {
            this.setState({ data: data.data, loading: false })
        })
        this.resetLoading()
    }
    exportFace = () => {
        window.open(exportFaceApi(), '_blank')
    }
    appendToStore = values => {
        const params = {
            key_face: this.state.selectedRowKeys.join(','),
            key_store: values.storeKey.join(',')
        }
        appendToStore(params).then(data => {
            this.handleResponse(data, '添加至人脸库')
            if (data.status === 'success') {
                this.setState({ storeVisible: false })
            }
        })
    }
    showModal = initialValues => {
        this.setState({ visible: true, initialValues })
    }
    closeModel = () => {
        this.setState({ visible: false })
    }
    showDetailModal = detailValues => {
        this.setState({ detailVisible: true, detailValues })
    }
    closeDetailModal = () => {
        this.setState({ detailVisible: false })
    }
    showStoreModal = () => {
        this.setState({ storeVisible: true })
    }
    closeStoreModal = () => {
        this.setState({ storeVisible: false })
    }
    onSelectChange = selectedRowKeys => {
        console.log('selectedRowKeys changed: ', selectedRowKeys)
        this.setState({ selectedRowKeys })
    }
    handleChange = (pagination, filters, sorter) => {
        console.log('Various parameters', pagination, filters, sorter)
        this.setState({ filteredInfo: filters, sortedInfo: sorter })
    }
    clearAll = () => {
        this.setState({ filteredInfo: null, sortedInfo: null })
        this.getFaceLib()
    }
    renderTreeNodes = data =>
        data.map(item => {
            if (item.children) {
                return (
                    <TreeNode title={item.title} value={item.title} key={item.key} dataRef={item}>
                        {this.renderTreeNodes(item.children)}
                    </TreeNode>
                )
            }
            return <TreeNode value={item.title} key={item.key} {...item} />
        })
    render() {
        const { visible, detailVisible, storeVisible, loading, data, initialValues, detailValues } = this.state
        let { sortedInfo, filteredInfo, selectedRowKeys } = this.state
        sortedInfo = sortedInfo || {}
        filteredInfo = filteredInfo || {}
        const columns = [
            {
                title: '姓名',
                dataIndex: 'name',
                key: 'name',
                sorter: (a, b) => (a.name > b.name ? 1 : -1),
                sortOrder: sortedInfo.columnKey === 'name' && sortedInfo.order,
                ellipsis: true,
                align: 'center',
                render: (value, record) => (
                    <b className='table_title' onClick={this.showDetailModal.bind(this, record)}>
                        {value}
                    </b>
                )
            },
            {
                title: '性别',
                dataIndex: 'gender',
                key: 'gender',
                filters: [
                    { text: '男', value: '男' },
                    { text: '女', value: '女' }
                ],
                filteredValue: filteredInfo.gender || null,
                onFilter: (value, record) => record.gender.includes(value),
                align: 'center'
            },
            {
                title: '年龄',
                dataIndex: 'age',
                key: 'age',
                sorter: (a, b) => a.age - b.age,
                sortOrder: sortedInfo.columnKey === 'age' && sortedInfo.order,
                align: 'center'
            },
            {
                title: '照片',
                dataIndex: 'photo',
                key: 'photo',
                align: 'center',
                render: value => <Image width={50} src={value} />
            },
            {
                title: '证件号',
                dataIndex: 'id',
                key: 'id',
                sorter: (a, b) => (a.id > b.id ? 1 : -1),
                sortOrder: sortedInfo.columnKey === 'id' && sortedInfo.order,
                align: 'center'
            },
            {
                title: '机构',
                dataIndex: 'part',
                key: 'part',
                align: 'center',
                render: value => (
                    <Tag color='#108ee9' style={{ marginRight: 0 }}>
                        {value}
                    </Tag>
                )
            },
            {
                title: '号码',
                dataIndex: 'phone',
                key: 'phone',
                align: 'center',
                sorter: (a, b) => (a.phone > b.phone ? 1 : -1),
                sortOrder: sortedInfo.columnKey === 'phone' && sortedInfo.order
            },
            {
                title: '标签',
                dataIndex: 'label',
                key: 'label',
                align: 'center'
            },
            {
                title: '备注',
                dataIndex: 'remark',
                key: 'remark',
                align: 'center'
            },
            {
                title: '操作',
                key: 'action',
                align: 'center',
                render: (text, record) => (
                    <Space size='middle'>
                        <span className='link' onClick={this.showModal.bind(this, record)}>
                            <EditOutlined />
                        </span>
                        <Popconfirm
                            placement='topRight'
                            title='是否确认删除该条信息？'
                            okText='确认'
                            cancelText='取消'
                            onConfirm={this.delFace.bind(this, record)}>
                            <span className='link danger'>
                                <DeleteOutlined />
                            </span>
                        </Popconfirm>
                    </Space>
                )
            }
        ]
        const rowSelection = {
            selectedRowKeys,
            onChange: this.onSelectChange
        }

        const StoreFormModal = props => {
            const [form] = Form.useForm()
            const { visible } = props
            const layout = {
                labelCol: { span: 4 },
                wrapperCol: { span: 19 }
            }
            const tailFormItemLayout = {
                wrapperCol: { span: 19, offset: 4 }
            }

            const resetForm = () => {
                form.resetFields()
            }

            return (
                <Modal visible={visible} title='批量添加至人脸库' onCancel={this.closeStoreModal} footer={null}>
                    <Form {...layout} form={form} layout='horizontal' onFinish={this.appendToStore}>
                        <Form.Item {...tailFormItemLayout}>
                            <span style={{ color: '#999' }}>将选中的人员添加到以下人脸库中</span>
                        </Form.Item>
                        <Form.Item label='人脸库' name='storeKey'>
                            <Select
                                showSearch
                                mode='multiple'
                                placeholder='请选择人脸库'
                                optionFilterProp='children'
                                style={{ width: '100%' }}
                                filterOption={(input, option) =>
                                    option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                                }>
                                {this.state.storeData.map(store => (
                                    <Option key={store.key}>{store.name}</Option>
                                ))}
                            </Select>
                        </Form.Item>
                        <Form.Item {...tailFormItemLayout}>
                            <Button type='primary' loading={loading} style={{ marginRight: 10 }} htmlType='submit'>
                                提交
                            </Button>
                            <Button onClick={resetForm}>清空</Button>
                        </Form.Item>
                    </Form>
                </Modal>
            )
        }
        const FormModal = props => {
            const [form] = Form.useForm()
            const { visible, initialValues } = props
            const layout = {
                labelCol: { span: 4 },
                wrapperCol: { span: 19 }
            }
            const tailFormItemLayout = {
                wrapperCol: { span: 19, offset: 4 }
            }
            const normFile = e => {
                console.log('Upload event:', e)
                if (Array.isArray(e)) {
                    return e
                }
                return e && e.fileList
            }
            const uploadProps = {
                beforeUpload: file => {
                    const isJPG = file.type === 'image/jpeg'
                    const isPNG = file.type === 'image/png'
                    if (!(isJPG || isPNG)) {
                        message.error(`只能上传 JPG 、JPEG 、PNG 格式的图片`)
                        return Upload.LIST_IGNORE
                    }
                    if (file.size / 1024 < 10 || file.size / 1024 > 200) {
                        message.error(`只能上传 10 KB ~ 200 KB 大小的图片`)
                        return Upload.LIST_IGNORE
                    }
                    return true
                }
            }

            useEffect(() => {
                const values = { ...initialValues }
                if (initialValues) {
                    values.photo = [
                        {
                            uid: '-1',
                            name: 'image.png',
                            status: 'done',
                            url: initialValues.photo
                        }
                    ]
                }
                form.setFieldsValue(values)
            })
            const resetForm = () => {
                form.resetFields()
            }

            return (
                <Modal
                    visible={visible}
                    title={initialValues ? '修改人脸信息' : '添加人脸信息'}
                    onCancel={this.closeModel}
                    width={700}
                    footer={null}>
                    <Form {...layout} form={form} layout='horizontal' onFinish={this.addFace}>
                        <Form.Item name='key' style={{ display: 'none' }} />
                        <Form.Item label='姓名' name='name' rules={[{ required: true, message: '请输入姓名' }]}>
                            <Input allowClear placeholder='请输入姓名' />
                        </Form.Item>
                        <Form.Item label='性别' name='gender' rules={[{ required: true, message: '请选择性别' }]}>
                            <Select placeholder='请选择性别'>
                                <Select.Option value='男'>男</Select.Option>
                                <Select.Option value='女'>女</Select.Option>
                            </Select>
                        </Form.Item>
                        <Form.Item label='年龄' name='age' rules={[{ required: true, message: '请输入年龄' }]}>
                            <InputNumber placeholder='请输入年龄' style={{ width: '100%' }} />
                        </Form.Item>
                        <Form.Item label='证件号' name='id' rules={[{ required: true, message: '请输入证件号' }]}>
                            <Input allowClear placeholder='请输入证件号' />
                        </Form.Item>
                        <Form.Item label='机构' name='part' rules={[{ required: true, message: '请选择机构' }]}>
                            <TreeSelect
                                showSearch
                                className='filter-item'
                                dropdownStyle={{ maxHeight: 600, overflow: 'auto' }}
                                placeholder='请选择机构'
                                allowClear
                                treeDefaultExpandAll>
                                {this.renderTreeNodes(this.state.orgTree)}
                            </TreeSelect>
                        </Form.Item>
                        <Form.Item label='号码' name='phone' rules={[{ required: true, message: '请输入电话号码' }]}>
                            <Input allowClear placeholder='请输入电话号码' />
                        </Form.Item>
                        <Form.Item label='标签' name='label' rules={[{ required: true, message: '请选择标签' }]}>
                            <Select showSearch allowClear placeholder='请选择标签'>
                                {this.state.personLabelData.map(item => (
                                    <Option value={item.name} key={item.key}>
                                        {item.name}
                                    </Option>
                                ))}
                            </Select>
                        </Form.Item>
                        <Form.Item
                            label='照片'
                            name='photo'
                            valuePropName='fileList'
                            getValueFromEvent={normFile}
                            rules={[{ required: true, message: '请上传照片' }]}>
                            <Upload
                                name='logo'
                                action='/upload.do'
                                listType='picture-card'
                                maxCount={1}
                                {...uploadProps}>
                                <span style={{ color: '#999', fontSize: 24 }}>+</span>
                            </Upload>
                        </Form.Item>
                        <div style={{ position: 'relative' }}>
                            <div
                                style={{
                                    color: 'rgba(223, 64, 42, .7)',
                                    position: 'absolute',
                                    right: '20px',
                                    top: '-124px',
                                    lineHeight: '25px',
                                    fontSize: '12px'
                                }}>
                                <div>请上传正面免冠照，露出五官；</div>
                                <div>图片文件支持 .jpg 格式，.png 格式；</div>
                                <div>图片文件大小 10 KB ~ 200 KB。</div>
                            </div>
                        </div>
                        <Form.Item label='备注' name='remark'>
                            <Input.TextArea allowClear placeholder='请输入备注信息' />
                        </Form.Item>
                        <Form.Item {...tailFormItemLayout}>
                            <Button type='primary' loading={loading} style={{ marginRight: 10 }} htmlType='submit'>
                                提交
                            </Button>
                            {initialValues ? null : <Button onClick={resetForm}>清空</Button>}
                        </Form.Item>
                    </Form>
                </Modal>
            )
        }
        const DetailModal = props => {
            const { visible, values } = props
            const layout = {
                labelCol: { span: 6 },
                wrapperCol: { span: 18 }
            }
            const layoutItem = {
                labelCol: { span: 3 },
                wrapperCol: { span: 21 }
            }

            return (
                <Modal title='详细信息' visible={visible} footer={null} width={600} onCancel={this.closeDetailModal}>
                    <Form {...layout}>
                        <Row>
                            <Col span={12}>
                                <Form.Item label='姓名'>
                                    <b className='form-span'>{values.name}</b>
                                </Form.Item>
                            </Col>
                            <Col span={12}>
                                <Form.Item label='性别'>
                                    <span className='form-span'>{values.gender}</span>
                                </Form.Item>
                            </Col>
                            <Col span={12}>
                                <Form.Item label='机构'>
                                    <span className='form-span'>{values.part}</span>
                                </Form.Item>
                            </Col>
                            <Col span={12}>
                                <Form.Item label='年龄'>
                                    <span className='form-span'>{values.age}</span>
                                </Form.Item>
                            </Col>
                            <Col span={12}>
                                <Form.Item label='证件号'>
                                    <span className='form-span'>{values.id}</span>
                                </Form.Item>
                            </Col>
                            <Divider style={{ margin: '0 0 18px' }} />
                            <Col span={24}>
                                <Form.Item label='人脸照片' {...layoutItem}>
                                    <Image className='detail-img' src={values.photo} />
                                </Form.Item>
                            </Col>
                            <Col span={12}>
                                <Form.Item label='创建时间'>
                                    <span className='form-span'>{values.time}</span>
                                </Form.Item>
                            </Col>
                            <Col span={12}>
                                <Form.Item label='标签'>
                                    <Tag color='magenta'>无</Tag>
                                </Form.Item>
                            </Col>
                            <Col span={24}>
                                <Form.Item label='备注' {...layoutItem}>
                                    <span className='form-span'>{values.remark}</span>
                                </Form.Item>
                            </Col>
                        </Row>
                    </Form>
                </Modal>
            )
        }
        const TableOpt = () => {
            const uploadProps = {
                name: 'file',
                action: importFaceApi(),
                showUploadList: false,
                headers: {
                    authorization: 'authorization-text'
                },
                onChange(info) {
                    if (info.file.status !== 'uploading') {
                        console.log(info.file, info.fileList)
                    }
                    if (info.file.status === 'done') {
                        message.success(`${info.file.name} 上传成功`)
                        this.getFaceLib()
                    } else if (info.file.status === 'error') {
                        message.error(`${info.file.name} 上传失败`)
                    }
                }
            }
            const [form] = Form.useForm()
            return (
                <div className='table-operations'>
                    <Form form={form} onFinish={this.queryFace}>
                        <Row gutter={24}>
                            <Col xs={24} sm={12} md={12} lg={8} xl={3}>
                                <Form.Item label='姓名' name='name'>
                                    <Input />
                                </Form.Item>
                            </Col>
                            <Col xs={24} sm={12} md={12} lg={8} xl={3}>
                                <Form.Item label='性别' name='gender'>
                                    <Select showSearch allowClear>
                                        <Option value='男' key='男'>
                                            男
                                        </Option>
                                        <Option value='女' key='女'>
                                            女
                                        </Option>
                                    </Select>
                                </Form.Item>
                            </Col>
                            <Col xs={24} sm={12} md={12} lg={8} xl={6}>
                                <Form.Item label='机构' name='part'>
                                    <TreeSelect
                                        showSearch
                                        dropdownStyle={{ maxHeight: 600, overflow: 'auto' }}
                                        allowClear
                                        treeDefaultExpandAll>
                                        {this.renderTreeNodes(this.state.orgTree)}
                                    </TreeSelect>
                                </Form.Item>
                            </Col>
                            <Col xs={24} sm={12} md={12} lg={8} xl={4}>
                                <Form.Item label='标签' name='label'>
                                    <Select showSearch allowClear>
                                        {this.state.personLabelData.map(item => (
                                            <Option value={item.name} key={item.key}>
                                                {item.name}
                                            </Option>
                                        ))}
                                    </Select>
                                </Form.Item>
                            </Col>
                            <Col xs={24} sm={12} md={10} lg={8} xl={6}>
                                <Form.Item label='年龄'>
                                    <Input.Group compact>
                                        <Form.Item name='minage' noStyle>
                                            <Input
                                                type='number'
                                                style={{ width: 100, borderRight: 0, textAlign: 'center' }}
                                            />
                                        </Form.Item>
                                        <Input
                                            style={{
                                                width: 30,
                                                borderLeft: 0,
                                                borderRight: 0,
                                                pointerEvents: 'none',
                                                backgroundColor: '#fff'
                                            }}
                                            placeholder='~'
                                            disabled
                                        />
                                        <Form.Item name='maxage' noStyle>
                                            <Input
                                                style={{
                                                    width: 100,
                                                    textAlign: 'center',
                                                    borderLeftWidth: 0
                                                }}
                                                type='number'
                                            />
                                        </Form.Item>
                                    </Input.Group>
                                </Form.Item>
                            </Col>
                            <Col xs={24} sm={12} md={12} lg={8} xl={2}>
                                <Space style={{ float: 'right' }}>
                                    <Button type='primary' htmlType='submit' icon={<SearchOutlined />}>
                                        搜索
                                    </Button>
                                </Space>
                            </Col>
                            <Col span={24} style={{ marginBottom: 10 }}>
                                <Space>
                                    <Button icon={<ReloadOutlined />} onClick={this.clearAll}>
                                        清除筛选或排序
                                    </Button>
                                </Space>
                                <Space style={{ float: 'right' }}>
                                    {this.state.selectedRowKeys.length ? (
                                        <Popconfirm
                                            title='是否确认删除选中条目？'
                                            okText='确认'
                                            cancelText='取消'
                                            onConfirm={this.muldelFace}>
                                            <Button
                                                type='primary'
                                                icon={<DeleteOutlined />}
                                                danger
                                                disabled={!this.state.selectedRowKeys.length}>
                                                批量删除
                                            </Button>
                                        </Popconfirm>
                                    ) : (
                                        <Button type='primary' icon={<DeleteOutlined />} danger disabled>
                                            批量删除
                                        </Button>
                                    )}
                                    <Button
                                        type='primary'
                                        icon={<SelectOutlined />}
                                        disabled={!this.state.selectedRowKeys.length}
                                        onClick={this.showStoreModal}>
                                        批量添加至人脸库
                                    </Button>
                                    <Upload {...uploadProps}>
                                        <Button type='primary' icon={<UploadOutlined />} onClick={this.importFace}>
                                            导入
                                        </Button>
                                    </Upload>
                                    <Button type='primary' icon={<DownloadOutlined />} onClick={this.exportFace}>
                                        导出
                                    </Button>
                                    <Button
                                        type='primary'
                                        icon={<PlusOutlined />}
                                        onClick={this.showModal.bind(this, null)}>
                                        添加
                                    </Button>
                                </Space>
                            </Col>
                        </Row>
                    </Form>
                </div>
            )
        }

        return (
            <div>
                <TableOpt />
                <Table
                    className='table'
                    size='middle'
                    columns={columns}
                    dataSource={data}
                    loading={loading}
                    rowSelection={rowSelection}
                    onChange={this.handleChange}
                />
                <FormModal visible={visible} initialValues={initialValues} />
                <DetailModal visible={detailVisible} values={detailValues} />
                <StoreFormModal visible={storeVisible} />
            </div>
        )
    }
}

const TableView = () => (
    <Layout className='index animated fadeIn'>
        <div>
            <CustomBreadcrumb arr={['人员管理']} />
        </div>
        <div className='face_lib'>
            <div className='base-style'>
                <Table4 />
            </div>
        </div>
    </Layout>
)

export default TableView
