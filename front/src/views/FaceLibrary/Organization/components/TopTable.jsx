import React, { Component, useEffect } from 'react'
import { Table, Button, Input, Modal, Form, Space, message, Popconfirm } from 'antd'
import { PlusOutlined, EditOutlined, DeleteOutlined } from '@ant-design/icons'
import '@/style/view-style/table.scss'

import { addNode, updateNode, delNode } from '@/api/faceLibrary'

class TopTable extends Component {
    constructor(props) {
        super(props)
        this.state = {
            data: props.curOrg,
            loading: false,
            visible: false,
            updateVisible: false,
            initialValues: null,
            updateInitialValues: null
        }
    }
    componentWillReceiveProps = props => {
        this.setState({ data: props.curOrg })
    }
    handleResponse = (data, str = '提交') => {
        if (data.status === 'success') {
            message.success(`${str}成功`)
            this.setState({ visible: false, updateVisible: false })
        } else {
            message.error(`${str}失败`)
        }
    }
    addNode = values => {
        addNode(values).then(data => {
            this.handleResponse(data, '添加')
            this.props.onTableChange()
        })
    }
    updateNode = values => {
        updateNode(values).then(data => {
            this.handleResponse(data, '修改')
            this.props.onTableChange()
        })
    }
    delNode = record => {
        const { id } = record
        delNode({ id }).then(data => {
            this.handleResponse(data, '删除')
            this.setState({ data: [] })
            this.props.onTableChange()
        })
    }
    showModal = initialValues => {
        this.setState({ visible: true, initialValues })
    }
    closeModel = () => {
        this.setState({ visible: false })
    }
    showUpdateModal = updateInitialValues => {
        this.setState({ updateVisible: true, updateInitialValues })
    }
    closeUpdateModal = () => {
        this.setState({ updateVisible: false })
    }
    render() {
        const { visible, updateVisible, loading, data, initialValues, updateInitialValues } = this.state
        const columns = [
            {
                title: '序号',
                dataIndex: 'id',
                key: 'id',
                align: 'center',
                width: '80px'
            },
            {
                title: '名称',
                dataIndex: 'name',
                key: 'name',
                align: 'center'
            },
            {
                title: '操作',
                key: 'action',
                align: 'center',
                width: '160px',
                render: (text, record) => (
                    <Space size='middle'>
                        <span className='link' onClick={this.showModal.bind(this, record)}>
                            添加子节点
                        </span>
                        <span className='link' onClick={this.showUpdateModal.bind(this, record)}>
                            <EditOutlined />
                        </span>
                        <Popconfirm
                            placement='topRight'
                            title='是否确认删除该条信息？'
                            okText='确认'
                            cancelText='取消'
                            onConfirm={this.delNode.bind(this, record)}>
                            <span className='link danger'>
                                <DeleteOutlined />
                            </span>
                        </Popconfirm>
                    </Space>
                )
            }
        ]

        const UpdateFormModal = props => {
            const [form] = Form.useForm()
            const { visible, initialValues } = props
            const layout = {
                labelCol: { span: 4 },
                wrapperCol: { span: 19 }
            }
            const tailFormItemLayout = {
                wrapperCol: { span: 19, offset: 4 }
            }

            useEffect(() => {
                form.setFieldsValue(initialValues)
            })

            return (
                <Modal visible={visible} title='修改节点' onCancel={this.closeUpdateModal} footer={null}>
                    <Form {...layout} form={form} layout='horizontal' onFinish={this.updateNode}>
                        <Form.Item name='id' style={{ display: 'none' }} />
                        <Form.Item label='机构名' name='name' rules={[{ required: true, message: '请输入机构名' }]}>
                            <Input allowClear placeholder='请输入机构名' />
                        </Form.Item>
                        <Form.Item {...tailFormItemLayout}>
                            <Button type='primary' loading={loading} style={{ marginRight: 10 }} htmlType='submit'>
                                提交
                            </Button>
                        </Form.Item>
                    </Form>
                </Modal>
            )
        }
        const FormModal = props => {
            const [form] = Form.useForm()
            const { visible, initialValues } = props
            const layout = {
                labelCol: { span: 4 },
                wrapperCol: { span: 19 }
            }
            const tailFormItemLayout = {
                wrapperCol: { span: 19, offset: 4 }
            }

            useEffect(() => {
                let values = null
                if (initialValues) {
                    values = { id: initialValues.id }
                }
                form.setFieldsValue(values)
            })

            return (
                <Modal
                    visible={visible}
                    title={initialValues ? '添加子节点' : '添加根节点'}
                    onCancel={this.closeModel}
                    footer={null}>
                    <Form {...layout} form={form} layout='horizontal' onFinish={this.addNode}>
                        <Form.Item name='id' style={{ display: 'none' }} />
                        {initialValues ? (
                            <Form.Item label='父节点'>
                                <span>{initialValues.name}</span>
                            </Form.Item>
                        ) : null}
                        <Form.Item label='机构名' name='name' rules={[{ required: true, message: '请输入机构名' }]}>
                            <Input allowClear placeholder='请输入机构名' />
                        </Form.Item>
                        <Form.Item {...tailFormItemLayout}>
                            <Button type='primary' loading={loading} style={{ marginRight: 10 }} htmlType='submit'>
                                提交
                            </Button>
                        </Form.Item>
                    </Form>
                </Modal>
            )
        }
        const TableOpt = () => {
            return (
                <div className='table-operations'>
                    <Space style={{ marginBottom: '10px' }}>
                        <Button type='primary' icon={<PlusOutlined />} onClick={this.showModal.bind(this, null)}>
                            添加根节点
                        </Button>
                    </Space>
                </div>
            )
        }

        return (
            <div>
                <TableOpt />
                <Table
                    className='table'
                    size='middle'
                    columns={columns}
                    dataSource={data}
                    loading={loading}
                    pagination={false}
                />
                <FormModal visible={visible} initialValues={initialValues} />
                <UpdateFormModal visible={updateVisible} initialValues={updateInitialValues} />
            </div>
        )
    }
}

export default TopTable
