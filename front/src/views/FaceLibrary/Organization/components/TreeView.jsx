import React, { Component } from 'react'
import { Input, Tree } from 'antd'
import '@/style/view-style/table.scss'

import { organizationTree } from '@/api/faceLibrary'

const { Search } = Input
const { TreeNode } = Tree

const gData = []
const data = []

const x = 3
const y = 2
const z = 1
const generateData = (_level, _preKey, _tns) => {
    const preKey = _preKey || '0'
    const tns = _tns || gData

    const children = []
    for (let i = 0; i < x; i++) {
        const key = `${preKey}-${i}`
        tns.push({ title: key, key })
        if (i < y) {
            children.push(key)
        }
    }
    if (_level < 0) {
        return tns
    }
    const level = _level - 1
    children.forEach((key, index) => {
        tns[index].children = []
        return generateData(level, key, tns[index].children)
    })
}
generateData(z)

const dataList = []
const generateList = data => {
    for (let i = 0; i < data.length; i++) {
        const node = data[i]
        const { key, title } = node
        dataList.push({ key, title })
        if (node.children) {
            generateList(node.children)
        }
    }
}

const getParentKey = (key, tree) => {
    let parentKey
    for (let i = 0; i < tree.length; i++) {
        const node = tree[i]
        if (node.children) {
            if (node.children.some(item => item.key === key)) {
                parentKey = node.key
            } else if (getParentKey(key, node.children)) {
                parentKey = getParentKey(key, node.children)
            }
        }
    }
    return parentKey
}
for (let i = 0; i < 46; i++) {
    data.push({
        key: i,
        name: `Edward King ${i}`,
        gender: '男',
        age: `${i + 1}`,
        part: `北京邮电大学. ${i}`,
        time: '2021-2-26'
    })
}

class TreeView extends Component {
    state = {
        expandedKeys: [1, 2],
        autoExpandParent: true,
        checkedKeys: ['0-0-0'],
        selectedKeys: [],
        searchValue: '',
        gData,
        data: []
    }
    componentDidMount = () => {
        this.props.onRef && this.props.onRef(this)
        this.organizationTree()
    }
    organizationTree = () => {
        organizationTree().then(data => {
            this.setState({ data: data.data })
            generateList(this.state.data)
        })
    }
    onDragEnter = info => {
        console.log(info)
        // expandedKeys 需要受控时设置
        this.setState({
            expandedKeys: info.expandedKeys
        })
    }
    onExpand = expandedKeys => {
        console.log('onExpand', expandedKeys)
        this.setState({
            expandedKeys,
            autoExpandParent: true
        })
    }
    onDrop = info => {
        console.log(info)
    }
    onCheck = checkedKeys => {
        console.log('onCheck', checkedKeys)
        this.setState({ checkedKeys })
    }
    onSelect = (selectedKeys, info) => {
        console.log('onSelect', selectedKeys, info)
        const curSelect = {
            id: parseInt(info.node.key),
            name: info.node.title.props.children[info.node.title.props.children.length - 1]
        }
        this.props.onSubmit([curSelect])
        this.setState({ selectedKeys })
    }

    renderTreeNodes = data =>
        data.map(item => {
            if (item.children) {
                return (
                    <TreeNode title={item.title} key={item.key} dataRef={item}>
                        {this.renderTreeNodes(item.children)}
                    </TreeNode>
                )
            }
            return <TreeNode key={item.key} {...item} />
        })

    onChange = e => {
        const { value } = e.target
        const expandedKeys = dataList
            .map(item => {
                if (item.title.indexOf(value) > -1) {
                    return getParentKey(item.key, this.state.data)
                }
                return null
            })
            .filter((item, i, self) => item && self.indexOf(item) === i)
        this.setState({
            expandedKeys,
            searchValue: value,
            autoExpandParent: true
        })
    }
    render() {
        const { searchValue, expandedKeys, autoExpandParent } = this.state
        const loop = data =>
            data.map(item => {
                const index = item.title.indexOf(searchValue)
                const beforeStr = item.title.substr(0, index)
                const afterStr = item.title.substr(index + searchValue.length)
                const title =
                    index > -1 ? (
                        <span>
                            {beforeStr}
                            <span style={{ color: '#f50' }}>{searchValue}</span>
                            {afterStr}
                        </span>
                    ) : (
                        <span>{item.title}</span>
                    )
                if (item.children) {
                    return (
                        <TreeNode key={item.key} title={title}>
                            {loop(item.children)}
                        </TreeNode>
                    )
                }
                return <TreeNode key={item.key} title={title} />
            })
        return (
            <div className='left'>
                <Search style={{ marginBottom: 8 }} placeholder='Search' onChange={this.onChange} />
                <Tree
                    showLine
                    onExpand={this.onExpand}
                    expandedKeys={expandedKeys}
                    autoExpandParent={autoExpandParent}
                    onSelect={this.onSelect}>
                    {loop(this.state.data)}
                </Tree>
            </div>
        )
    }
}

export default TreeView
