import React, { Component } from 'react'
import { Layout, Table, Space, message, Popconfirm } from 'antd'
import { DeleteOutlined } from '@ant-design/icons'
import CustomBreadcrumb from '@/components/CustomBreadcrumb'
import TopTable from './components/TopTable'
import TreeView from './components/TreeView'
import '@/style/view-style/table.scss'

import { getChildrenNode, delNode } from '@/api/faceLibrary'

class Table4 extends Component {
    constructor(props) {
        super(props)
        this.state = {
            data: [],
            curOrg: props.curOrg,
            loading: false
        }
    }
    componentDidMount = () => {
        this.props.onRef && this.props.onRef(this)
    }
    componentWillReceiveProps = props => {
        this.setState({ curOrg: props.curOrg })
        if (props.curOrg.length) {
            this.getChildrenNode(props.curOrg[0].id)
        }
    }
    resetLoading = () => {
        const _this = this
        setTimeout(() => {
            if (_this.state.loading) {
                this.setState({ loading: false })
            }
        }, 3000)
    }
    handleResponse = (data, str = '提交') => {
        if (data.status === 'success') {
            message.success(`${str}成功`)
            this.setState({ visible: false })
            this.refreshTable()
        } else {
            message.error(`${str}失败`)
        }
    }
    getChildrenNode = id => {
        this.setState({ loading: true })
        getChildrenNode({ id }).then(data => {
            this.setState({ data: data.data, loading: false })
        })
        this.resetLoading()
    }
    refreshTable = () => {
        this.getChildrenNode(this.state.curOrg[0].id)
    }
    delNode = record => {
        const { key } = record
        delNode({ id: key }).then(data => {
            this.handleResponse(data, '删除')
            this.props.onTableChange()
        })
    }
    render() {
        const { loading, data } = this.state
        const columns = [
            {
                title: '序号',
                dataIndex: 'key',
                key: 'key',
                align: 'center',
                width: '80px'
            },
            {
                title: '子节点',
                dataIndex: 'title',
                key: 'title',
                align: 'center'
            },
            {
                title: '操作',
                key: 'action',
                align: 'center',
                width: '160px',
                render: (text, record) => (
                    <Space size='middle'>
                        <Popconfirm
                            placement='topRight'
                            title='是否确认删除该条信息？'
                            okText='确认'
                            cancelText='取消'
                            onConfirm={this.delNode.bind(this, record)}>
                            <span className='link danger'>
                                <DeleteOutlined />
                            </span>
                        </Popconfirm>
                    </Space>
                )
            }
        ]

        return <Table className='table' size='middle' columns={columns} dataSource={data} loading={loading} />
    }
}

class TableView extends Component {
    state = {
        curOrg: []
    }
    handleTreeSelect = curOrg => {
        this.setState({ curOrg })
    }
    onTreeRef = ref => {
        this.TreeView = ref
    }
    onTableRef = ref => {
        this.TableView = ref
    }
    render() {
        return (
            <Layout className='index animated fadeIn'>
                <div>
                    <CustomBreadcrumb arr={['机构管理']} />
                </div>
                <div className='face_lib'>
                    <div className='lib_left'>
                        <div className='base-style'>
                            <TreeView onRef={this.onTreeRef} onSubmit={this.handleTreeSelect.bind(this)} />
                        </div>
                    </div>
                    <div className='lib_right'>
                        <div className='base-style'>
                            <TopTable
                                curOrg={this.state.curOrg}
                                onTableChange={() => {
                                    this.TreeView.organizationTree()
                                    this.TableView.refreshTable()
                                }}
                            />
                        </div>
                        <div className='base-style'>
                            <Table4
                                curOrg={this.state.curOrg}
                                onRef={this.onTableRef}
                                onTableChange={() => {
                                    this.TreeView.organizationTree()
                                }}
                            />
                        </div>
                    </div>
                </div>
            </Layout>
        )
    }
}

export default TableView
