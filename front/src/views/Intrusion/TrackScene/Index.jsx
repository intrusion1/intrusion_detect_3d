import React, { Component } from 'react'
import { Layout, Row, Col, Divider, List, Select, Tag, Modal, Form, Image } from 'antd'
import { FullscreenOutlined } from '@ant-design/icons'
import { Link } from 'react-router-dom'
import screenfull from 'screenfull'
import ReactPlayer from 'react-player'

import avatar from '@/assets/images/user.jpg'
import middleImg from '@/assets/images/img.png'
import '@/style/view-style/index.scss'
import './index.scss'

import { getIntrusionArea, historyIntrusion } from "@/api/intrusion";
import { getDevice } from '@/api/device'
import { switchIntrusionChannel, videoFeed} from '@/api/video'
import Zmage from "react-zmage";

const { Option } = Select

class Index extends Component {
    state = {
        orgTreeList: [],
        deviceList: [],
        intrusionhistoryList: [],
        avatar,
        middleImg,
        value: null,
        visible: false,
        detailValues: {},
        channel: '',
        area: ''
    }
    componentDidMount = () => {
        this.getDevice()
        this.videoFeed()
        this.historyIntrusion()
        // this.getIntrusion()
        this.timer = setInterval(() => {
            this.historyIntrusion()
            // this.getHistoryWhitelist()
        }, 10000)
    }
    componentWillUnmount() {
        this.timer && clearInterval(this.timer)
    }
    getDevice = () => {
        getDevice().then(data => {
            this.setState({ deviceList: data.data })
        })
    }
    // getIntrusion = () => {
    //     getIntrusion().then(data => {
    //         this.setState({ data: data.data})
    //     })
    // }
    videoFeed = () => {
        videoFeed().then(data => {
            //
        })
    }
    historyIntrusion = () => {
        historyIntrusion().then(data => {
            this.setState({ intrusionhistoryList: data.data})
        })
    }
    showModal = initialValues => {
        this.setState({ visible: true, initialValues })
    }
    closeModel = () => {
        this.setState({ visible: false })
    }
    fullToggle = () => {
        if (screenfull.isEnabled) {
            screenfull.request(document.getElementById('live'))
        }
    }
    viewInfo = item => {
        this.setState({
            visible: true,
            detailValues: item
        })
    }
    onClose = () => {
        this.setState({
            visible: false
        })
    }
    changeChannel = name => {
        switchIntrusionChannel({ name }).then(data => {
            const { channel } = data
            this.setState({ channel })
        })
        getIntrusionArea({ name }).then(data => {
            const { area } = data
            this.setState({ area })
        })
    }
    // getIntrusionArea = name => {
    //     getIntrusionArea({ name }).then(data => {
    //         const { area } = data
    //         this.setState({ area })
    //     })
    // }
    render() {
        const { visible, detailValues, data } = this.state

        const ItemList = ({ data, onClick }) => (
            <List
                itemLayout='horizontal'
                className='list'
                dataSource={data.slice(0, 30)}
                renderItem={item => (
                    <List.Item className='list-item'>
                        <List.Item.Meta
                            avatar={<Image className='list-img' src={item.photo} />}
                            title={
                                <span className='link'
                                      onClick={this.viewInfo.bind(this, item)}
                                >
                                    {item.label}
                                </span>
                            }
                            description={item.time}
                        />
                    </List.Item>
                )}
            />
        )
        // const DetailModal = props => {
        //     const { visible, values } = props
        //     const layout = {
        //         labelCol: { span: 6 },
        //         wrapperCol: { span: 18 }
        //     }
        //     const layoutItem = {
        //         labelCol: { span: 3 },
        //         wrapperCol: { span: 21 }
        //     }
        //
        //     return (
        //         <Modal title='详细信息' visible={visible} footer={null} width={600} onCancel={this.onClose}>
        //             {/*<Form {...layout}>*/}
        //             {/*    <Row>*/}
        //             {/*        <Col span={12}>*/}
        //             {/*            <Form.Item label='标签' name='label'>*/}
        //             {/*                <b className='form-span'>{this.state.detailValues.label}*/}
        //             {/*                        </b>*/}
        //             {/*            </Form.Item>*/}
        //             {/*        </Col>*/}
        //             {/*        <Divider style={{ margin: '0 0 18px' }} />*/}
        //             {/*        <Col span={24}>*/}
        //             {/*            <Form.Item label='入侵物照片' {...layoutItem}>*/}
        //             {/*                <Image className='detail-img' src={values.photo} />*/}
        //             {/*            </Form.Item>*/}
        //             {/*        </Col>*/}
        //             {/*        <Col span={12}>*/}
        //             {/*            <Form.Item label='创建时间'>*/}
        //             {/*                <span className='form-span'>{values.time}</span>*/}
        //             {/*            </Form.Item>*/}
        //             {/*        </Col>*/}
        //             {/*        <Col span={24}>*/}
        //             {/*            <Form.Item label='备注' {...layoutItem}>*/}
        //             {/*                <span className='form-span'>{values.remark}</span>*/}
        //             {/*            </Form.Item>*/}
        //             {/*        </Col>*/}
        //             {/*    </Row>*/}
        //             {/*</Form>*/}
        //         </Modal>
        //     )
        // }
        return (
            <Layout className='index animated fadeIn'>
                <div className='container'>
                    <Row className='container-content'>
                        <Col flex='230px' className='height_100'>
                            <div className='container-content_sidebar'>
                                <div className='list-title'>
                                    <span>区域入侵检测</span>

                                    {/*<Link className='list-title-button'*/}
                                    {/*      onClick={this.showModal.bind(this.props.value)}*/}
                                    {/*>*/}
                                    {/*    更多*/}
                                    {/*</Link>*/}
                                </div>
                                <ItemList data={this.state.intrusionhistoryList}/>
                            </div>
                        </Col>
                        <Col flex='auto' className='height_100'>
                            <div className='container-content_content'>
                                <div className='bar-header content-header'>
                                    <FullscreenOutlined
                                        type='fullscreen'
                                        style={{cursor: 'pointer', lineHeight: '32px'}}
                                        onClick={this.fullToggle}
                                    />
                                    <div>
                                        <Select
                                            showSearch
                                            className='filter-item'
                                            placeholder='请选择视频流'
                                            optionFilterProp='children'
                                            allowClear
                                            onChange={this.changeChannel }
                                            filterOption={(input, option) =>
                                                option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                                            }>
                                            {this.state.deviceList.map(item => (
                                                <Option value={item.name} key={item.key}>
                                                    {item.name}
                                                </Option>
                                            ))}
                                        </Select>
                                    </div>
                                    <FullscreenOutlined
                                        type='fullscreen'
                                        style={{cursor: 'pointer', lineHeight: '32px'}}
                                        onClick={this.fullToggle}
                                    />
                                </div>
                                <div className='content-cavans'>
                                    <ReactPlayer
                                        className='content-cavans-live'
                                        url={this.state.channel}
                                        playing
                                        width='100%'
                                        height='100%'
                                        controls
                                        config={{
                                            file: {forceHLS: true}
                                        }}

                                    />

                                    <div
                                        style={{
                                             border: '1px solid red',
                                             width: '1220px',
                                             height: '685px',
                                             position: 'relative',
                                             left: 72 + 'px',
                                             top: -685 + 'px',
                                             // right: 1000 + 'px',
                                             // bottom: 10 + 'px'
                                            //this.state.area.split(',')[0]
                                        }}>
                                        <div
                                            style={{
                                            border: '1px solid blue',
                                            position: 'absolute',
                                            left: this.state.area.split(',')[1] > this.state.area.split(',')[3] ?
                                                this.state.area.split(',')[0] + 'px'
                                                : this.state.area.split(',')[2]-Math.sqrt(((this.state.area.split(',')[2]-this.state.area.split(',')[0])*(this.state.area.split(',')[2]-this.state.area.split(',')[0])+(this.state.area.split(',')[1]-this.state.area.split(',')[3])*(this.state.area.split(',')[1]-this.state.area.split(',')[3]))) + 'px',
                                            top: this.state.area.split(',')[1] > this.state.area.split(',')[3] ?
                                                685 - this.state.area.split(',')[1] + 'px'
                                                :685 - this.state.area.split(',')[3] + 'px',
                                            right: this.state.area.split(',')[1] > this.state.area.split(',')[3] ?
                                                1220 - Math.sqrt(((this.state.area.split(',')[2]-this.state.area.split(',')[0])*(this.state.area.split(',')[2]-this.state.area.split(',')[0])+(this.state.area.split(',')[1]-this.state.area.split(',')[3])*(this.state.area.split(',')[1]-this.state.area.split(',')[3]))) - this.state.area.split(',')[0] + 'px'
                                                : 1220-this.state.area.split(',')[2]+'px' ,
                                            bottom: this.state.area.split(',')[1] > this.state.area.split(',')[3] ?
                                                this.state.area.split(',')[1] + 'px'
                                                : this.state.area.split(',')[3] + 'px',
                                            transform: `rotate(${Math.atan((this.state.area.split(',')[1]-this.state.area.split(',')[3])/(this.state.area.split(',')[2]-this.state.area.split(',')[0]))}rad)`,
                                            transformOrigin: this.state.area.split(',')[1] > this.state.area.split(',')[3] ?
                                                             'top left' : 'bottom right'
                                        }}
                                        />

                                        <div
                                            style = {{
                                            border: '1px solid blue',
                                            position: 'absolute',
                                            left: this.state.area.split(',')[4] > this.state.area.split(',')[2] ?
                                                this.state.area.split(',')[4] + 'px'
                                                : this.state.area.split(',')[2] + 'px',
                                            top: this.state.area.split(',')[4] > this.state.area.split(',')[2] ?
                                                685 - Math.sqrt(((this.state.area.split(',')[4]-this.state.area.split(',')[2])*(this.state.area.split(',')[4]-this.state.area.split(',')[2])+(this.state.area.split(',')[3]-this.state.area.split(',')[5])*(this.state.area.split(',')[3]-this.state.area.split(',')[5]))) - this.state.area.split(',')[5] + 'px'
                                                : 685-this.state.area.split(',')[3]+'px' ,
                                            right: this.state.area.split(',')[4] > this.state.area.split(',')[2] ?
                                                1220 - this.state.area.split(',')[4] + 'px'
                                                : 1220 - this.state.area.split(',')[2] + 'px',
                                            bottom: this.state.area.split(',')[4] > this.state.area.split(',')[2] ?
                                                this.state.area.split(',')[5] + 'px'
                                                : this.state.area.split(',')[3]-Math.sqrt(((this.state.area.split(',')[4]-this.state.area.split(',')[2])*(this.state.area.split(',')[4]-this.state.area.split(',')[2])+(this.state.area.split(',')[3]-this.state.area.split(',')[5])*(this.state.area.split(',')[3]-this.state.area.split(',')[5]))) + 'px',
                                            transform: `rotate(${Math.atan((this.state.area.split(',')[4]-this.state.area.split(',')[2])/(this.state.area.split(',')[5]-this.state.area.split(',')[3]))}rad)`,
                                            transformOrigin: this.state.area.split(',')[4] > this.state.area.split(',')[2] ?
                                                             'bottom right' : 'top left'
                                        }}>
                                        </div>

                                        <div
                                            style = {{
                                            border: '1px solid blue',
                                            position: 'absolute',
                                            left: this.state.area.split(',')[7] > this.state.area.split(',')[5] ?
                                                this.state.area.split(',')[6] + 'px'
                                                : this.state.area.split(',')[4]-Math.sqrt(((this.state.area.split(',')[4]-this.state.area.split(',')[6])*(this.state.area.split(',')[4]-this.state.area.split(',')[6])+(this.state.area.split(',')[7]-this.state.area.split(',')[5])*(this.state.area.split(',')[7]-this.state.area.split(',')[5]))) + 'px',
                                            top: this.state.area.split(',')[7] > this.state.area.split(',')[5] ?
                                                685 - this.state.area.split(',')[7] + 'px'
                                                : 685 - this.state.area.split(',')[5] + 'px',
                                            right: this.state.area.split(',')[7] > this.state.area.split(',')[5] ?
                                                1220 - Math.sqrt(((this.state.area.split(',')[4]-this.state.area.split(',')[6])*(this.state.area.split(',')[4]-this.state.area.split(',')[6])+(this.state.area.split(',')[7]-this.state.area.split(',')[5])*(this.state.area.split(',')[7]-this.state.area.split(',')[5]))) - this.state.area.split(',')[6] + 'px'
                                                : 1220 - this.state.area.split(',')[4]+'px' ,
                                            bottom: this.state.area.split(',')[7] > this.state.area.split(',')[5] ?
                                                this.state.area.split(',')[7] + 'px'
                                                : this.state.area.split(',')[5] + 'px',
                                            transform: `rotate(${Math.atan((this.state.area.split(',')[7]-this.state.area.split(',')[5])/(this.state.area.split(',')[4]-this.state.area.split(',')[6]))}rad)`,
                                            transformOrigin: this.state.area.split(',')[7] > this.state.area.split(',')[5] ?
                                                             'top left' : 'bottom right'
                                        }}>
                                        </div>

                                        <div
                                            style = {{
                                            border: '1px solid blue',
                                            position: 'absolute',
                                            left: this.state.area.split(',')[6] > this.state.area.split(',')[0] ?
                                                this.state.area.split(',')[6] + 'px'
                                                : this.state.area.split(',')[0] + 'px',
                                            top: this.state.area.split(',')[6] > this.state.area.split(',')[0] ?
                                                685 - Math.sqrt(((this.state.area.split(',')[6]-this.state.area.split(',')[0])*(this.state.area.split(',')[6]-this.state.area.split(',')[0])+(this.state.area.split(',')[1]-this.state.area.split(',')[7])*(this.state.area.split(',')[1]-this.state.area.split(',')[7]))) - this.state.area.split(',')[7] + 'px'
                                                : 685-this.state.area.split(',')[1]+'px' ,
                                            right: this.state.area.split(',')[6] > this.state.area.split(',')[0] ?
                                                1220 - this.state.area.split(',')[6] + 'px'
                                                : 1220 - this.state.area.split(',')[0] + 'px',
                                            bottom: this.state.area.split(',')[6] > this.state.area.split(',')[0] ?
                                                this.state.area.split(',')[7] + 'px'
                                                : this.state.area.split(',')[1]-Math.sqrt(((this.state.area.split(',')[6]-this.state.area.split(',')[0])*(this.state.area.split(',')[6]-this.state.area.split(',')[0])+(this.state.area.split(',')[1]-this.state.area.split(',')[7])*(this.state.area.split(',')[1]-this.state.area.split(',')[7]))) + 'px',
                                            transform: `rotate(${Math.atan((this.state.area.split(',')[6]-this.state.area.split(',')[0])/(this.state.area.split(',')[7]-this.state.area.split(',')[1]))}rad)`,
                                            transformOrigin: this.state.area.split(',')[6] > this.state.area.split(',')[0] ?
                                                             'bottom right' : 'top left'
                                        }}>
                                        </div>
                                        
                                    </div>

                                </div>
                            </div>
                        </Col>
                    </Row>
                </div>
                {/*<showModal visible={visible} values={detailValues}/>*/}
                {/*{*/}
                {/*    <Modal title='详细信息@@@@@@@@@2' visible={visible} values={detailValues} footer={null} width={600} onCancel={this.onClose}>*/}

                {/*    <Form {...Layout}>*/}
                {/*        <Row>*/}

                {/*            <Col span={12}>*/}
                {/*                <Form.Item label='标签'>*/}
                {/*                    <Tag color='magenta'>{this.state.detailValues.label}</Tag>*/}
                {/*                </Form.Item>*/}
                {/*            </Col><Col span={24}>*/}
                {/*                <Form.Item label='入侵物照片' >*/}
                {/*                    <Zmage className='detail-img' src={this.state} />*/}
                {/*                </Form.Item>*/}
                {/*            </Col>*/}
                {/*            <Col span={12}>*/}
                {/*                <Form.Item label='创建时间'>*/}
                {/*                    <span className='form-span'>{this.state.detailValues.time}</span>*/}
                {/*                </Form.Item>*/}
                {/*            </Col>*/}

                {/*            <Col span={24}>*/}
                {/*                <Form.Item label='备注' >*/}
                {/*                    <span className='form-span'>*/}
                {/*                        撒旦发射点发，撒旦发射点射点发sdfsdf上射sdfsdfs是，点*/}
                {/*                    </span>*/}
                {/*                </Form.Item>*/}
                {/*            </Col>*/}
                {/*        </Row>*/}
                {/*    </Form>*/}
                {/*</Modal> }*/}
            </Layout>
        )
    }
}

export default Index
