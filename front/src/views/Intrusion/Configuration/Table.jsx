import React, { Component, useEffect, useState } from 'react'
import {
    Layout,
    Divider,
    Table,
    Button,
    Input,
    Modal,
    Form,
    Space,
    message,
    Popconfirm,
    InputNumber,
    Row,
    Col,
    Select,
    Image
} from 'antd'
import { getDevice } from '@/api/device'
import { screenshotIntrusion } from '@/api/intrusion'
import { PlusOutlined, EditOutlined, DeleteOutlined } from '@ant-design/icons'
import CustomBreadcrumb from '@/components/CustomBreadcrumb'
import '@/style/view-style/table.scss'

import { getIntrusion, addIntrusion, updateIntrusion, delIntrusion, muldelIntrusion, queryIntrusion } from '@/api/intrusion'

const { Search } = Input
const { Option } = Select

class Table4 extends Component {
    state = {
        data: [],
        storeData: [],
        deviceData: [],
        filteredInfo: null,
        sortedInfo: null,
        selectedRowKeys: [],
        loading: false,
        visible: false,
        initialValues: null,
        modal: {
            x1: null,
            y1: null,
            x2: null,
            y2: null,
            x3: null,
            y3: null,
            x4: null,
            y4: null,
        }
    }
    componentDidMount = () => {
        this.getIntrusion()
        this.getDevice()
    }
    getDevice = () => {
        getDevice().then(data => {
            this.setState({ deviceData: data.data })
        })
    }
    resetLoading = () => {
        const _this = this
        setTimeout(() => {
            if (_this.state.loading) {
                this.setState({ loading: false })
            }
        }, 3000)
    }
    handleResponse = (data, str = '提交') => {
        if (data.status === 'success') {
            message.success(`${str}成功`)
            this.setState({ visible: false })
            this.getIntrusion()
        } else {
            message.error(`${str}失败`)
        }
    }
    getIntrusion = () => {
        this.setState({ loading: true })
        getIntrusion().then(data => {
            this.setState({ data: data.data, loading: false })
        })
        this.resetLoading()
    }
    addIntrusion = values => {
        const optApi = this.state.initialValues ? updateIntrusion : addIntrusion
        optApi({ ...values }).then(data => {
            this.handleResponse(data, this.state.initialValues ? '修改' : '添加')
        })
    }
    delIntrusion = record => {
        const { key } = record
        delIntrusion({ key }).then(data => {
            this.handleResponse(data, '删除')
        })
    }
    muldelIntrusion = () => {
        muldelIntrusion({ key: this.state.selectedRowKeys }).then(data => {
            this.handleResponse(data, '删除')
        })
    }
    queryIntrusion = data => {
        this.setState({ loading: true })
        queryIntrusion({ name: data }).then(data => {
            this.setState({ data: data.data, loading: false })
        })
        this.resetLoading()
    }
    showModal = initialValues => {
        this.setState({ visible: true, initialValues })
    }
    closeModel = () => {
        this.setState({ visible: false })
    }
    onSelectChange = selectedRowKeys => {
        console.log('selectedRowKeys changed: ', selectedRowKeys)
        this.setState({ selectedRowKeys })
    }
    handleChange = (pagination, filters, sorter) => {
        console.log('Various parameters', pagination, filters, sorter)
        this.setState({ filteredInfo: filters, sortedInfo: sorter })
    }
    clearAll = () => {
        this.setState({ filteredInfo: null, sortedInfo: null })
        this.getIntrusion()
    }
    setValue = (key, value) => {
        console.log(key, value)
        const { modal } = this.state
        modal[key] = value
        this.setState({ modal })
    }
    render() {
        const { visible, loading, data, initialValues} = this.state
        let { sortedInfo, selectedRowKeys } = this.state
        sortedInfo = sortedInfo || {}
        const columns = [
            {
                title: '通道名',
                dataIndex: 'name',
                key: 'name',
                sorter: (a, b) => (a.name > b.name ? 1 : -1),
                sortOrder: sortedInfo.columnKey === 'name' && sortedInfo.order,
                ellipsis: true,
                align: 'center',
                render: (value, record) => <b className='table_title'>{value}</b>
            },
            {
                title: '检测区域',
                key: 'photo_amended',
                dataIndex: 'photo_amended',
                align: 'center',
                render: value => <Image width={60} src={value} />
            },
            {
                title: '备注',
                dataIndex: 'remark',
                key: 'remark',
                align: 'center'
            },
            {
                title: '时间',
                dataIndex: 'time',
                key: 'time',
                align: 'center'
            },
            {
                title: '操作',
                key: 'action',
                align: 'center',
                render: (text, record) => (
                    <Space size='middle'>
                        <span className='link' onClick={this.showModal.bind(this, record)}>
                            <EditOutlined />
                        </span>
                        <Popconfirm
                            placement='topRight'
                            title='是否确认删除该条信息？'
                            okText='确认'
                            cancelText='取消'
                            onConfirm={this.delIntrusion.bind(this, record)}>
                            <span className='link danger'>
                                <DeleteOutlined />
                            </span>
                        </Popconfirm>
                    </Space>
                )
            }
        ]
        const rowSelection = {
            selectedRowKeys,
            onChange: this.onSelectChange
        }

        const FormModal = props => {
            const [form] = Form.useForm()
            const { visible, initialValues } = props
            const layout = {
                labelCol: { span: 4 },
                wrapperCol: { span: 19 }
            }
            const tailFormItemLayout = {
                wrapperCol: { span: 19, offset: 4 }
            }
            const halfItemLayout = {
                labelCol: { span: 8 },
                wrapperCol: { span: 16 }
            }

            const [x1, setX1] = useState(false)
            const [y1, setY1] = useState(false)
            const [x2, setX2] = useState(false)
            const [y2, setY2] = useState(false)
            const [x3, setX3] = useState(false)
            const [y3, setY3] = useState(false)
            const [x4, setX4] = useState(false)
            const [y4, setY4] = useState(false)
            const [change, setChange] = useState(false)
            const [image, setImage] = useState('')

            useEffect(() => {
                if (initialValues && !change) {
                    setX1(initialValues.x1)
                    setY1(initialValues.y1)
                    setX2(initialValues.x2)
                    setY2(initialValues.y2)
                    setX3(initialValues.x3)
                    setY3(initialValues.y3)
                    setX4(initialValues.x4)
                    setY4(initialValues.y4)
                    setImage(initialValues.photo_original)
                }
            })
            const resetForm = () => {
                form.resetFields()
            }
            const setValue = (key, value) => {
                setChange(true)
                switch (key) {
                    case 'x1':
                        if (value < 0) value = 0
                        if (value > 400) value = 400
                        setX1(value)
                        break
                    case 'y1':
                        if (value < 0) value = 0
                        if (value > 225) value = 225
                        setY1(value)
                        break
                    case 'x2':
                        if (value < 0) value = 0
                        if (value > 400) value = 400
                        setX2(value)
                        break
                    case 'y2':
                        if (value < 0) value = 0
                        if (value > 225) value = 225
                        setY2(value)
                        break
                    case 'x3':
                        if (value < 0) value = 0
                        if (value > 400) value = 400
                        setX3(value)
                        break
                    case 'y3':
                        if (value < 0) value = 0
                        if (value > 225) value = 225
                        setY3(value)
                        break
                    case 'x4':
                        if (value < 0) value = 0
                        if (value > 400) value = 400
                        setX4(value)
                        break
                    case 'y4':
                        if (value < 0) value = 0
                        if (value > 225) value = 225
                        setY4(value)
                        break
                    default:
                        break
                }
                setTimeout(() => {
                    form.setFieldsValue({ [key]: value })
                }, 0)
            }
            const isValid = (x1, y1, x2, y2, x3, y3, x4, y4) => {
                if (
                    (typeof x1 === 'number' || typeof x1 === 'string') &&
                    (typeof y1 === 'number' || typeof y1 === 'string') &&
                    (typeof x2 === 'number' || typeof x2 === 'string') &&
                    (typeof y2 === 'number' || typeof y2 === 'string') &&
                    (typeof x3 === 'number' || typeof x3 === 'string') &&
                    (typeof y3 === 'number' || typeof y3 === 'string') &&
                    (typeof x4=== 'number' || typeof x4 === 'string') &&
                    (typeof y4 === 'number' || typeof y4 === 'string')
                ) {
                    return true
                }
                return false
            }

            return (
                <Modal
                    visible={visible}
                    title={initialValues ? '修改配置信息' : '添加配置信息'}
                    onCancel={this.closeModel}
                    footer={null}
                    width={700}
                    maskTransitionName=''>
                    <Form
                        {...layout}
                        form={form}
                        layout='horizontal'
                        initialValues={initialValues}
                        onFinish={this.addIntrusion}>
                        <Form.Item name='key' style={{ display: 'none' }} />
                        <Form.Item label='通道名' name='name'>
                            <Select showSearch allowClear>
                                {this.state.deviceData.map(item => (
                                    <Option value={item.name} key={item.key}>
                                        {item.name}
                                    </Option>
                                ))}
                            </Select>
                        </Form.Item>
                        <Row>
                            <Col span={12}>
                                <Form.Item
                                    {...halfItemLayout}
                                    label='左上角x轴坐标'
                                    extra='Range: 0 - 400'
                                    name='x1'
                                    rules={[{ required: true, message: '请输入左上角x轴坐标' }]}>
                                    <InputNumber min={0} max={400} onChange={e => setValue('x1', e)} />
                                </Form.Item>
                            </Col>
                            <Col span={12}>
                                <Form.Item
                                    {...halfItemLayout}
                                    label='左上角y轴坐标'
                                    extra='Range: 0 - 225'
                                    name='y1'
                                    rules={[{ required: true, message: '请输入左上角y轴坐标' }]}>
                                    <InputNumber min={0} max={225} onChange={e => setValue('y1', e)} />
                                </Form.Item>
                            </Col>
                            <Col span={12}>
                                <Form.Item
                                    {...halfItemLayout}
                                    label='右上角x轴坐标'
                                    extra='Range: 0 - 400'
                                    name='x2'
                                    rules={[{ required: true, message: '请输入右上角x轴坐标' }]}>
                                    <InputNumber min={0} max={400} onChange={e => setValue('x2', e)} />
                                </Form.Item>
                            </Col>
                            <Col span={12}>
                                <Form.Item
                                    {...halfItemLayout}
                                    label='右上角y轴坐标'
                                    extra='Range: 0 - 225'
                                    name='y2'
                                    rules={[{ required: true, message: '请输入右上角y轴坐标' }]}>
                                    <InputNumber min={0} max={225} onChange={e => setValue('y2', e)} />
                                </Form.Item>
                            </Col>
                            <Col span={12}>
                                <Form.Item
                                    {...halfItemLayout}
                                    label='右下角x轴坐标'
                                    extra='Range: 0 - 400'
                                    name='x3'
                                    rules={[{ required: true, message: '请输入右下角x轴坐标' }]}>
                                    <InputNumber min={0} max={400} onChange={e => setValue('x3', e)} />
                                </Form.Item>
                            </Col>
                            <Col span={12}>
                                <Form.Item
                                    {...halfItemLayout}
                                    label='右下角y轴坐标'
                                    extra='Range: 0 - 225'
                                    name='y3'
                                    rules={[{ required: true, message: '请输入右下角y轴坐标' }]}>
                                    <InputNumber min={0} max={225} onChange={e => setValue('y3', e)} />
                                </Form.Item>
                            </Col>
                            <Col span={12}>
                                <Form.Item
                                    {...halfItemLayout}
                                    label='左下角x轴坐标'
                                    extra='Range: 0 - 400'
                                    name='x4'
                                    rules={[{ required: true, message: '请输入左下角x轴坐标' }]}>
                                    <InputNumber min={0} max={400} onChange={e => setValue('x4', e)} />
                                </Form.Item>
                            </Col>
                            <Col span={12}>
                                <Form.Item
                                    {...halfItemLayout}
                                    label='左下角y轴坐标'
                                    extra='Range: 0 - 225'
                                    name='y4'
                                    rules={[{ required: true, message: '请输入左下角y轴坐标' }]}>
                                    <InputNumber min={0} max={225} onChange={e => setValue('y4', e)} />
                                </Form.Item>
                            </Col>
                        </Row>
                        <Form.Item label='备注' name='remark'>
                            <Input.TextArea allowClear placeholder='请输入备注信息' />
                        </Form.Item>
                        <Form.Item label='区域'>
                            <Image width={400} src={image} />
                            <div style={{ position: 'absolute', top: '0', left: '0', width: '400px', height: '225px' }}>
                                <div
                                    style={
                                        {
                                            visibility: isValid(x1, y1, x2, y2, x3, y3, x4, y4) ? 'visible' : 'hidden',

                                            border: '1px solid blue',
                                            position: 'absolute',
                                            left: y1 > y2 ? x1 + 'px' : x2-Math.sqrt(((x2-x1)*(x2-x1)+(y1-y2)*(y1-y2))) + 'px',
                                            top: y1 > y2 ? 225 - y1 + 'px' : 225 - y2 + 'px',
                                            right: y1 > y2 ?
                                                400 - Math.sqrt(((x2-x1)*(x2-x1)+(y1-y2)*(y1-y2))) - x1 + 'px'
                                                : 400-x2+'px' ,
                                            bottom: y1 > y2 ? y1 + 'px' : y2 + 'px',
                                            transform: `rotate(${Math.atan((y1-y2)/(x2-x1))}rad)`,
                                            transformOrigin: y1 > y2 ? 'top left' : 'bottom right'
                                        }

                                    }
                                />
                                <div
                                    style = {
                                    {
                                            visibility: isValid(x1, y1, x2, y2, x3, y3, x4, y4) ? 'visible' : 'hidden',
                                            border: '1px solid blue',
                                            position: 'absolute',
                                            left: x3 > x2 ? x3 + 'px' : x2 + 'px',
                                            top: x3 > x2 ?
                                                225 - Math.sqrt(((x3-x2)*(x3-x2)+(y2-y3)*(y2-y3))) - y3 + 'px'
                                                : 225-y2+'px' ,
                                            right: x3 > x2 ? 400 - x3 + 'px' : 400 - x2 + 'px',
                                            bottom: x3 > x2 ? y3 + 'px' : y2-Math.sqrt(((x3-x2)*(x3-x2)+(y2-y3)*(y2-y3))) + 'px',

                                            transform: `rotate(${Math.atan((x3-x2)/(y3-y2))}rad)`,
                                            transformOrigin: x3 > x2 ? 'bottom right' : 'top left'
                                        }
                                }>
                                </div>
                                <div
                                    style = {
                                        {
                                            visibility: isValid(x1, y1, x2, y2, x3, y3, x4, y4) ? 'visible' : 'hidden',

                                            border: '1px solid blue',
                                            position: 'absolute',
                                            left: y4 > y3 ? x4 + 'px' : x3-Math.sqrt(((x3-x4)*(x3-x4)+(y4-y3)*(y4-y3))) + 'px',
                                            top: y4 > y3 ? 225 - y4 + 'px' : 225 - y3 + 'px',
                                            right: y4 > y3 ?
                                                400 - Math.sqrt(((x3-x4)*(x3-x4)+(y4-y3)*(y4-y3))) - x4 + 'px'
                                                : 400-x3+'px' ,
                                            bottom: y4 > y3 ? y4 + 'px' : y3 + 'px',

                                            transform: `rotate(${Math.atan((y4-y3)/(x3-x4))}rad)`,
                                            transformOrigin: y4 > y3 ? 'top left' : 'bottom right'
                                        }
                                }>
                                </div>
                                <div
                                    style = {
                                    {
                                            visibility: isValid(x1, y1, x2, y2, x3, y3, x4, y4) ? 'visible' : 'hidden',
                                            border: '1px solid blue',
                                            position: 'absolute',
                                            left: x4 > x1 ? x4 + 'px' : x1 + 'px',
                                            top: x4 > x1 ?
                                                225 - Math.sqrt(((x4-x1)*(x4-x1)+(y1-y4)*(y1-y4))) - y4 + 'px'
                                                : 225-y1+'px' ,
                                            right: x4 > x1 ? 400 - x4 + 'px' : 400 - x1 + 'px',
                                            bottom: x4 > x1 ? y4 + 'px' : y1-Math.sqrt(((x4-x1)*(x4-x1)+(y1-y4)*(y1-y4))) + 'px',

                                            transform: `rotate(${Math.atan((x4-x1)/(y4-y1))}rad)`,
                                            transformOrigin: x4 > x1 ? 'bottom right' : 'top left'
                                        }
                                }>
                                </div>
                            </div>
                        </Form.Item>
                        <Form.Item {...tailFormItemLayout}>
                            <Button type='primary' loading={loading} style={{ marginRight: 10 }} htmlType='submit'>
                                提交
                            </Button>
                            {initialValues ? null : <Button onClick={resetForm}>清空</Button>}
                        </Form.Item>
                    </Form>
                </Modal>
            )
        }
        const TableOpt = () => {
            return (
                <div className='table-operations'>
                    <Search
                        style={{ marginBottom: 10, width: 300 }}
                        allowClear
                        placeholder='搜索'
                        onSearch={this.queryIntrusion}
                    />
                    <Divider type='vertical' />
                    <Button onClick={this.clearAll}>清除筛选或排序</Button>

                    <Space style={{ float: 'right' }}>
                        {this.state.selectedRowKeys.length ? (
                            <Popconfirm
                                title='是否确认删除选中条目？'
                                okText='确认'
                                cancelText='取消'
                                onConfirm={this.muldelIntrusion}>
                                <Button
                                    type='primary'
                                    icon={<DeleteOutlined />}
                                    danger
                                    disabled={!this.state.selectedRowKeys.length}>
                                    批量删除
                                </Button>
                            </Popconfirm>
                        ) : (
                            <Button type='primary' icon={<DeleteOutlined />} danger disabled>
                                批量删除
                            </Button>
                        )}
                        <Button type='primary' icon={<PlusOutlined />} onClick={this.showModal.bind(this, null)}>
                            添加
                        </Button>
                    </Space>
                </div>
            )
        }

        return (
            <div>
                <TableOpt />
                <Table
                    className='table'
                    size='middle'
                    columns={columns}
                    dataSource={data}
                    loading={loading}
                    rowSelection={rowSelection}
                    onChange={this.handleChange}
                />
                <FormModal visible={visible} initialValues={initialValues} />
            </div>
        )
    }
}

const TableView = () => (
    <Layout className='index animated fadeIn'>
        <div>
            <CustomBreadcrumb arr={['参数配置']} />
        </div>
        <div className='face_lib'>
            <div className='base-style'>
                <Table4 />
            </div>
        </div>
    </Layout>
)

export default TableView
