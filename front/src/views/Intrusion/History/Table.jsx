import React, { Component } from 'react'
import {
    Layout,
    Table,
    Button,
    Input,
    Row,
    Col,
    Select,
    Tag,
    Form,
    TreeSelect,
    Space,
    message,
    Popconfirm,
    DatePicker,
    Image
} from 'antd'
import { DeleteOutlined, SearchOutlined, ReloadOutlined } from '@ant-design/icons'
import Zmage from 'react-zmage'
import CustomBreadcrumb from '@/components/CustomBreadcrumb'
import '@/style/view-style/table.scss'

import { historyIntrusion, queryHistoryIntrusion, muldelHistoryIntrusion, delHistoryIntrusion } from '@/api/intrusion'
import { getDevice } from '@/api/device'

const { Option } = Select
const { TreeNode } = TreeSelect

class Table4 extends Component {
    state = {
        data: [],
        intrusionData: [],
        deviceData: [],
        filteredInfo: null,
        sortedInfo: null,
        selectedRowKeys: [],
        initialValues: null,
        loading: false
    }
    componentDidMount = () => {
        this.historyIntrusion()
        // this.organizationTree()
        // this.getPersonLabel()
        this.getDevice()
    }
    resetLoading = () => {
        const _this = this
        setTimeout(() => {
            if (_this.state.loading) {
                this.setState({ loading: false })
            }
        }, 3000)
    }
    handleResponse = (data, str = '提交') => {
        if (data.status === 'success') {
            message.success(`${str}成功`)
            this.setState({ visible: false })
            this.historyIntrusion()
        } else {
            message.error(`${str}失败`)
        }
    }
    historyIntrusion = () => {
        this.setState({ loading: true })
        historyIntrusion().then(data => {
            this.setState({ data: data.data, loading: false })
        })
        this.resetLoading()
    }
    // organizationTree = () => {
    //     organizationTree().then(data => {
    //         this.setState({ orgTree: data.data })
    //     })
    // }
    // getPersonLabel = () => {
    //     getPersonLabel().then(data => {
    //         this.setState({ personLabelData: data.data })
    //     })
    // }
    getDevice = () => {
        getDevice().then(data => {
            this.setState({ deviceData: data.data })
        })
    }
    delHistoryIntrusion = record => {
        const { key } = record
        delHistoryIntrusion({ key }).then(data => {
            this.handleResponse(data, '删除')
        })
    }
    muldelHistoryIntrusion = () => {
        muldelHistoryIntrusion({ key: this.state.selectedRowKeys }).then(data => {
            this.handleResponse(data, '删除')
        })
    }
    queryHistoryIntrusion = data => {
        this.setState({ loading: true })
        queryHistoryIntrusion(data).then(data => {
            this.setState({ data: data.data, loading: false })
        })
        this.resetLoading()
    }
    showModal = initialValues => {
        this.setState({ visible: true, initialValues })
    }
    closeModel = () => {
        this.setState({ visible: false })
    }
    onSelectChange = selectedRowKeys => {
        console.log('selectedRowKeys changed: ', selectedRowKeys)
        this.setState({ selectedRowKeys })
    }
    handleChange = (pagination, filters, sorter) => {
        console.log('Various parameters', pagination, filters, sorter)
        this.setState({ filteredInfo: filters, sortedInfo: sorter })
    }
    clearAll = () => {
        this.setState({ filteredInfo: null, sortedInfo: null })
        this.historyIntrusion()
    }
    renderTreeNodes = data =>
        data.map(item => {
            if (item.children) {
                return (
                    <TreeNode title={item.title} value={item.title} key={item.key} dataRef={item}>
                        {this.renderTreeNodes(item.children)}
                    </TreeNode>
                )
            }
            return <TreeNode value={item.title} key={item.key} {...item} />
        })
    render() {
        const { loading, data } = this.state
        let { sortedInfo, filteredInfo, selectedRowKeys } = this.state
        sortedInfo = sortedInfo || {}
        filteredInfo = filteredInfo || {}
        const columns = [
            {
                title: '编号',
                dataIndex: 'key',
                key: 'key',
                sorter: (a, b) => (a.key > b.key ? 1 : -1),
                sortOrder: sortedInfo.columnKey === 'key' && sortedInfo.order,
                ellipsis: true,
                align: 'center',
                render: (value, record) => <b className='table_title'>{value}</b>
            },
            {
                title: '照片',
                dataIndex: 'photo',
                key: 'photo',
                align: 'center',
                render: value => <Image width={50} src={value} />
            },
            {
                title: '标签',
                dataIndex: 'label',
                key: 'label',
                align: 'center'
            },
            {
                title: '通道',
                dataIndex: 'device',
                key: 'device',
                align: 'center'
            },
            {
                title: '备注',
                dataIndex: 'remark',
                key: 'remark',
                align: 'center'
            },
            {
                title: '创建时间',
                dataIndex: 'time',
                key: 'time',
                align: 'center'
            },
            {
                title: '操作',
                key: 'action',
                align: 'center',
                render: (text, record) => (
                    <Space size='middle'>
                        <Popconfirm
                            placement='topRight'
                            title='是否确认删除该条信息？'
                            okText='确认'
                            cancelText='取消'
                            onConfirm={this.delHistoryIntrusion.bind(this, record)}>
                            <span className='link danger'>
                                <DeleteOutlined />
                            </span>
                        </Popconfirm>
                    </Space>
                )
            }
        ]
        const rowSelection = {
            selectedRowKeys,
            onChange: this.onSelectChange
        }

        const TableOpt = () => {
            const [form] = Form.useForm()
            return (
                <div className='table-operations'>
                    <Form form={form} onFinish={this.queryHistoryIntrusion}>
                        <Row gutter={24}>
                            <Col xs={24} sm={12} md={12} lg={8} xl={5}>
                                <Form.Item label='编号' name='key'>
                                    <Input />
                                </Form.Item>
                            </Col>
                            <Col xs={24} sm={12} md={12} lg={8} xl={4}>
                                <Form.Item label='标签' name='label'>
                                    <Input />
                                    {/*<Select showSearch allowClear>*/}
                                    {/*    {this.state.intrusionData.map(item => (*/}
                                    {/*        <Option value={item.name} key={item.key}>*/}
                                    {/*            {item.name}*/}
                                    {/*        </Option>*/}
                                    {/*    ))}*/}
                                    {/*</Select>*/}
                                </Form.Item>
                            </Col>
                            <Col xs={24} sm={12} md={10} lg={8} xl={6}>
                                <Form.Item label='通道' name='device'>
                                    <Select showSearch allowClear>
                                        {this.state.deviceData.map(item => (
                                            <Option value={item.name} key={item.key}>
                                                {item.name}
                                            </Option>
                                        ))}
                                    </Select>
                                </Form.Item>
                            </Col>
                            <Col xs={24} sm={12} md={12} lg={8} xl={5}>
                                <Form.Item label='起始时间' name='mintime'>
                                    <DatePicker format='YYYY-MM-DD' style={{ width: '100%' }} placeholder='' />
                                </Form.Item>
                            </Col>
                            <Col xs={24} sm={12} md={12} lg={8} xl={5}>
                                <Form.Item label='终止时间' name='maxtime'>
                                    <DatePicker format='YYYY-MM-DD' style={{ width: '100%' }} placeholder='' />
                                </Form.Item>
                            </Col>
                            <Col xs={24} sm={12} md={12} lg={8} xl={2}>
                                <Space style={{ float: 'right' }}>
                                    <Button type='primary' htmlType='submit' icon={<SearchOutlined />}>
                                        搜索
                                    </Button>
                                </Space>
                            </Col>
                            <Col span={24} style={{ marginBottom: 10 }}>
                                <Space>
                                    <Button icon={<ReloadOutlined />} onClick={this.clearAll}>
                                        清除筛选或排序
                                    </Button>
                                </Space>
                                <Space style={{ float: 'right' }}>
                                    {this.state.selectedRowKeys.length ? (
                                        <Popconfirm
                                            title='是否确认删除选中条目？'
                                            okText='确认'
                                            cancelText='取消'
                                            onConfirm={this.muldelHistoryIntrusion}>
                                            <Button
                                                type='primary'
                                                icon={<DeleteOutlined />}
                                                danger
                                                disabled={!this.state.selectedRowKeys.length}>
                                                批量删除
                                            </Button>
                                        </Popconfirm>
                                    ) : (
                                        <Button type='primary' icon={<DeleteOutlined />} danger disabled>
                                            批量删除
                                        </Button>
                                    )}
                                </Space>
                            </Col>
                        </Row>
                    </Form>
                </div>
            )
        }

        return (
            <div>
                <TableOpt />
                <Table
                    className='table'
                    size='middle'
                    columns={columns}
                    dataSource={data}
                    loading={loading}
                    rowSelection={rowSelection}
                    onChange={this.handleChange}
                />
            </div>
        )
    }
}

const TableView = () => (
    <Layout className='index animated fadeIn'>
        <div>
            <CustomBreadcrumb arr={['历史记录']} />
        </div>
        <div className='history_intrusion'>
            <div className='base-style'>
                <Table4 />
            </div>
        </div>
    </Layout>
)

export default TableView
