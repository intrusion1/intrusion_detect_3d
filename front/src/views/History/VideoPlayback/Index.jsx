import React, { Component } from 'react'
import { Layout, Row, Col, Divider } from 'antd'
import { FullscreenOutlined } from '@ant-design/icons'
import screenfull from 'screenfull'
import '@/style/view-style/index.scss'
import CustomBreadcrumb from '@/components/CustomBreadcrumb'

import BarEcharts from './bar.jsx'

class Index extends Component {
    fullToggle = () => {
        if (screenfull.isEnabled) {
            screenfull.request(document.getElementById('bar'))
        }
    }
    render() {
        return (
            <Layout className='index animated fadeIn'>
                <div>
                    <CustomBreadcrumb arr={['历史记录', '视频回放']}></CustomBreadcrumb>
                </div>
                <Row>
                    <Col>
                        <div className='base-style'>
                            <div className='bar-header'>
                                <div>视频回放</div>
                                <FullscreenOutlined
                                    type='fullscreen'
                                    style={{ cursor: 'pointer' }}
                                    onClick={this.fullToggle}
                                />
                            </div>
                            <Divider />
                            <BarEcharts />
                        </div>
                    </Col>
                </Row>
            </Layout>
        )
    }
}

export default Index
