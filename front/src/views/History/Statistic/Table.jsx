import React, { Component } from 'react'
import { Layout, Form, Row, Col, DatePicker, Space, Button, Select } from 'antd'
import { SearchOutlined } from '@ant-design/icons'
import moment from 'moment'
import CustomBreadcrumb from '@/components/CustomBreadcrumb'
import PieEcharts from './components/Pie.jsx'
import '@/style/view-style/table.scss'

import { getDevice } from '@/api/device'
import { statisticHistory } from '@/api/history'

const { Option } = Select

class Table4 extends Component {
    state = {
        deviceList: [],
        pieGender: [],
        pieAge: [],
        piePart: [],
        pieLabel: [],
        mintime: moment(new Date()).subtract(7, 'days'),
        maxtime: moment(new Date()).subtract(1, 'days'),
        device: null
    }
    componentDidMount = () => {
        this.getDevice()
        this.statisticHistory({
            mintime: '2021-04-22 22:22:22',
            maxtime: '2021-08-31 00:00:00',
            device: '海康'
        })
        // this.queryPeriodFlow({
        //     mintime: this.state.mintime.format('YYYY-MM-DD'),
        //     maxtime: this.state.maxtime.format('YYYY-MM-DD')
        // })
    }
    getDevice = () => {
        getDevice().then(data => {
            this.setState({ deviceList: data.data })
        })
    }
    statisticHistory = params => {
        this.setState({
            mintime: moment(params.mintime),
            maxtime: moment(params.maxtime),
            device: params.device
        })
        statisticHistory({
            mintime: moment(params.mintime).format('YYYY-MM-DD HH:mm:ss'),
            maxtime: moment(params.maxtime).format('YYYY-MM-DD HH:mm:ss'),
            device: params.device ? params.device : undefined
        }).then(({ data }) => {
            const gender = data[0].gender
            const age = data[1].age
            const part = data[2].part
            const label = data[3].label
            const pieGender = gender.map(item => ({ name: item.type, value: item.value }))
            const pieAge = age.map(item => ({ name: item.type, value: item.value }))
            const piePart = part.map(item => ({ name: item.type, value: item.value }))
            const pieLabel = label.map(item => ({ name: item.type, value: item.value }))
            this.setState({
                pieGender,
                pieAge,
                piePart,
                pieLabel
            })
        })
    }
    render() {
        const TopOpt = () => {
            const [form] = Form.useForm()
            return (
                <div className='table-operations'>
                    <Form
                        form={form}
                        onFinish={this.statisticHistory}
                        initialValues={{
                            mintime: this.state.mintime,
                            maxtime: this.state.maxtime,
                            device: this.state.device
                        }}>
                        <Row>
                            <Space>
                                <Form.Item name='mintime'>
                                    <DatePicker showTime style={{ width: '200px' }} placeholder='请选择起始日期' />
                                </Form.Item>
                                <Form.Item>
                                    <span>--</span>
                                </Form.Item>
                                <Form.Item name='maxtime'>
                                    <DatePicker
                                        showTime
                                        style={{ width: '200px', marginRight: '10px' }}
                                        placeholder='请选择终止日期'
                                    />
                                </Form.Item>
                                <Form.Item name='device'>
                                    <Select
                                        showSearch
                                        placeholder='请选择设备通道'
                                        optionFilterProp='children'
                                        allowClear
                                        style={{ width: '200px', marginRight: '10px' }}
                                        filterOption={(input, option) =>
                                            option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                                        }>
                                        {this.state.deviceList.map(item => (
                                            <Option value={item.name} key={item.key}>
                                                {item.name}
                                            </Option>
                                        ))}
                                    </Select>
                                </Form.Item>
                                <Form.Item>
                                    <Button type='primary' htmlType='submit' icon={<SearchOutlined />}>
                                        搜索
                                    </Button>
                                </Form.Item>
                            </Space>
                        </Row>
                    </Form>
                </div>
            )
        }
        return (
            <div>
                <div
                    style={{
                        paddingLeft: '10px',
                        borderLeft: '4px solid #409EFF',
                        fontWeight: 'bold',
                        marginBottom: '20px'
                    }}>
                    统计分析
                </div>
                <div style={{ padding: '0 20px' }}>
                    <TopOpt />
                    <Row gutter={16}>
                        <Col span={24}>
                            <h3>
                                {this.state.device ? `【${this.state.device}】 ` : ''}
                                {`${this.state.mintime.format('YYYY-MM-DD HH:mm:ss')} ~ ${this.state.maxtime.format(
                                    'YYYY-MM-DD HH:mm:ss'
                                )}`}
                            </h3>
                        </Col>
                        <Col span={12}>
                            <PieEcharts pieId='gender' title='性别' data={this.state.pieGender} />
                        </Col>
                        <Col span={12}>
                            <PieEcharts pieId='age' title='年龄' data={this.state.pieAge} />
                        </Col>
                        <Col span={12}>
                            <PieEcharts pieId='part' title='机构' data={this.state.piePart} />
                        </Col>
                        <Col span={12}>
                            <PieEcharts pieId='label' title='标签' data={this.state.pieLabel} />
                        </Col>
                    </Row>
                </div>
            </div>
        )
    }
}

const TableView = () => (
    <Layout className='index animated fadeIn'>
        <div>
            <CustomBreadcrumb arr={['统计分析']} />
        </div>
        <div className='face_lib'>
            <div className='base-style'>
                <Table4 />
            </div>
        </div>
    </Layout>
)

export default TableView
