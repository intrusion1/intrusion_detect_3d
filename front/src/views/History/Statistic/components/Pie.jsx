import React, { Component } from 'react'
import echarts from 'echarts/lib/echarts'
import 'echarts/lib/chart/pie'
import 'echarts/lib/component/tooltip'
import 'echarts/lib/component/title'
import 'echarts/lib/component/legend'

class Pie extends Component {
    state = {
        myChart: null
    }
    componentDidMount() {
        let myChart = echarts.init(document.getElementById('pie' + this.props.pieId), 'light')
        myChart.setOption({
            title: {
                top: '3%',
                left: '1%',
                text: this.props.title
            },
            tooltip: {
                trigger: 'item'
            },
            legend: {
                top: '8%',
                left: 'center'
            },
            series: [
                {
                    name: '数量',
                    type: 'pie',
                    radius: ['40%', '70%'],
                    center: ['50%', '55%'],
                    avoidLabelOverlap: false,
                    itemStyle: {
                        borderRadius: 10,
                        borderColor: '#fff',
                        borderWidth: 2
                    },
                    label: {
                        show: false,
                        position: 'center'
                    },
                    emphasis: {
                        label: {
                            show: true,
                            fontSize: '24',
                            fontWeight: 'bold'
                        }
                    },
                    data: this.props.data
                }
            ]
        })
        window.addEventListener('resize', function() {
            myChart.resize()
        })
        this.setState({ myChart })
    }
    componentDidUpdate() {
        this.state.myChart.setOption({
            tooltip: {
                trigger: 'item'
            },
            legend: {
                top: '8%',
                left: 'center'
            },
            series: [
                {
                    name: '数量',
                    type: 'pie',
                    radius: ['40%', '70%'],
                    avoidLabelOverlap: false,
                    itemStyle: {
                        borderRadius: 10,
                        borderColor: '#fff',
                        borderWidth: 2
                    },
                    label: {
                        show: false,
                        position: 'center'
                    },
                    emphasis: {
                        label: {
                            show: true,
                            fontSize: '24',
                            fontWeight: 'bold'
                        }
                    },
                    data: this.props.data
                }
            ]
        })
    }
    render() {
        return (
            <div
                id={'pie' + this.props.pieId}
                style={{
                    height: 300,
                    marginBottom: '24px',
                    boxShadow: '0 0 10px rgba(0, 0, 0, .05)',
                    borderRadius: '4px'
                }}
            />
        )
    }
}

export default Pie
