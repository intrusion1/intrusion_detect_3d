import React, { Component, useEffect } from 'react'
import { Layout, Divider, Table, Button, Input, Modal, Form, Space, message, Tag, Popconfirm, Row, Col } from 'antd'
import { PlusOutlined, MinusOutlined } from '@ant-design/icons'
import CustomBreadcrumb from '@/components/CustomBreadcrumb'
import '@/style/view-style/table.scss'

import { getUser, addUser, updateUser, muldelUser, delUser, queryUser } from '@/api/user'

const { Search } = Input

class Table4 extends Component {
    state = {
        data: [],
        sortedInfo: null,
        selectedRowKeys: [],
        loading: false,
        visible: false,
        detailVisible: false,
        initialValues: null,
        detailValues: {}
    }
    componentDidMount = () => {
        this.getUser()
    }
    resetLoading = () => {
        const _this = this
        setTimeout(() => {
            if (_this.state.loading) {
                this.setState({ loading: false })
            }
        }, 3000)
    }
    handleResponse = (data, str = '提交') => {
        if (data.status === 'success') {
            message.success(`${str}成功`)
            this.setState({ visible: false })
            this.getUser()
        } else {
            message.error(`${str}失败`)
        }
    }
    getUser = () => {
        this.setState({ loading: true })
        getUser().then(data => {
            this.setState({ data: data.data, loading: false })
        })
        this.resetLoading()
    }
    addUser = values => {
        const optApi = this.state.initialValues ? updateUser : addUser
        optApi({ ...values }).then(data => {
            this.handleResponse(data, this.state.initialValues ? '修改' : '添加')
        })
    }
    delUser = record => {
        const { username } = record
        delUser({ username }).then(data => {
            this.handleResponse(data, '删除')
        })
    }
    muldelUser = () => {
        muldelUser({ key: this.state.selectedRowKeys }).then(data => {
            this.handleResponse(data, '删除')
        })
    }
    queryUser = data => {
        this.setState({ loading: true })
        queryUser({ username: data }).then(data => {
            this.setState({ data: data.data, loading: false })
        })
        this.resetLoading()
    }
    showModal = initialValues => {
        this.setState({ visible: true, initialValues })
    }
    closeModel = () => {
        this.setState({ visible: false })
    }
    showDetailModal = detailValues => {
        this.setState({ detailVisible: true, detailValues })
    }
    closeDetailModal = () => {
        this.setState({ detailVisible: false })
    }
    onSelectChange = selectedRowKeys => {
        console.log('selectedRowKeys changed: ', selectedRowKeys)
        this.setState({ selectedRowKeys })
    }
    handleChange = (pagination, filters, sorter) => {
        console.log('Various parameters', pagination, filters, sorter)
        this.setState({ sortedInfo: sorter })
    }
    clearAll = () => {
        this.setState({ sortedInfo: null })
        this.getUser()
    }
    render() {
        const { visible, detailVisible, loading, data, initialValues, detailValues } = this.state
        let { sortedInfo, selectedRowKeys } = this.state
        sortedInfo = sortedInfo || {}
        const columns = [
            {
                title: '用户名',
                dataIndex: 'username',
                key: 'username',
                sorter: (a, b) => (a.username > b.username ? 1 : -1),
                sortOrder: sortedInfo.columnKey === 'username' && sortedInfo.order,
                ellipsis: true,
                align: 'center',
                render: (value, record) => (
                    <b className='table_title' onClick={this.showDetailModal.bind(this, record)}>
                        {value}
                    </b>
                )
            },
            {
                title: '密码',
                dataIndex: 'password',
                key: 'password',
                align: 'center',
                render: value => <span>{value}</span>
            },
            {
                title: '角色',
                dataIndex: 'role',
                key: 'role',
                align: 'center',
                render: value => (
                    <Tag color='green' style={{ marginRight: 0 }}>
                        {value}
                    </Tag>
                )
            },
            {
                title: '备注',
                dataIndex: 'notes',
                key: 'notes',
                align: 'center'
            },
            {
                title: '创建时间',
                dataIndex: 'createTime',
                key: 'createTime',
                align: 'center',
                sorter: (a, b) => (a.createTime > b.createTime ? 1 : -1),
                sortOrder: sortedInfo.columnKey === 'createTime' && sortedInfo.order
            },
            {
                title: '操作',
                key: 'action',
                align: 'center',
                render: (text, record) => (
                    <Space size='middle'>
                        <span className='link' onClick={this.showModal.bind(this, record)}>
                            修改
                        </span>
                        <Popconfirm
                            placement='topRight'
                            title='是否确认删除该条信息？'
                            okText='确认'
                            cancelText='取消'
                            onConfirm={this.delUser.bind(this, record)}>
                            <span className='link danger'>删除</span>
                        </Popconfirm>
                    </Space>
                )
            }
        ]
        const rowSelection = {
            selectedRowKeys,
            onChange: this.onSelectChange
        }
        const tailFormItemLayout = {
            wrapperCol: {
                xs: {
                    span: 19,
                    offset: 4
                },
                sm: {
                    span: 19,
                    offset: 4
                }
            }
        }

        const FormModal = props => {
            const [form] = Form.useForm()
            const { visible, initialValues } = props

            useEffect(() => {
                form.setFieldsValue(initialValues)
            })
            const resetForm = () => {
                form.resetFields()
            }

            return (
                <Modal
                    visible={visible}
                    title={initialValues ? '修改用户' : '添加用户'}
                    onCancel={this.closeModel}
                    footer={null}>
                    <Form
                        form={form}
                        initialValues={initialValues}
                        labelCol={{ span: 4 }}
                        wrapperCol={{ span: 19 }}
                        layout='horizontal'
                        onFinish={this.addUser}>
                        <Form.Item name='key' style={{ display: 'none' }} />
                        <Form.Item label='用户名' name='username' rules={[{ required: true, message: '请输入用户名' }]}>
                            <Input allowClear placeholder='请输入用户名' />
                        </Form.Item>
                        <Form.Item label='密码' name='password' rules={[{ required: true, message: '请输入密码' }]}>
                            <Input allowClear placeholder='请输入密码' />
                        </Form.Item>
                        <Form.Item label='角色' name='role' rules={[{ required: true, message: '请输入角色' }]}>
                            <Input allowClear placeholder='请输入角色' />
                        </Form.Item>
                        <Form.Item label='备注' name='notes'>
                            <Input.TextArea allowClear placeholder='请输入备注信息' />
                        </Form.Item>
                        <Form.Item {...tailFormItemLayout}>
                            <Button type='primary' loading={loading} style={{ marginRight: 10 }} htmlType='submit'>
                                提交
                            </Button>
                            {initialValues ? null : <Button onClick={resetForm}>清空</Button>}
                        </Form.Item>
                    </Form>
                </Modal>
            )
        }
        const DetailModal = props => {
            const { visible, values } = props
            const layout = {
                labelCol: { span: 6 },
                wrapperCol: { span: 18 }
            }
            const layoutItem = {
                labelCol: { span: 3 },
                wrapperCol: { span: 21 }
            }

            return (
                <Modal title='详细信息' visible={visible} footer={null} width={600} onCancel={this.closeDetailModal}>
                    <Form {...layout}>
                        <Row>
                            <Col span={12}>
                                <Form.Item label='用户名'>
                                    <b className='form-span'>{values.username}</b>
                                </Form.Item>
                            </Col>
                            <Col span={12}>
                                <Form.Item label='密码'>
                                    <span className='form-span'>{values.password}</span>
                                </Form.Item>
                            </Col>
                            <Col span={12}>
                                <Form.Item label='角色'>
                                    <span className='form-span'>
                                        <Tag color='green' style={{ marginRight: 0 }}>
                                            {values.role}
                                        </Tag>
                                    </span>
                                </Form.Item>
                            </Col>
                            <Col span={12}>
                                <Form.Item label='创建时间'>
                                    <span className='form-span'>{values.createTime}</span>
                                </Form.Item>
                            </Col>
                            <Col span={24}>
                                <Form.Item label='备注' {...layoutItem}>
                                    <span className='form-span'>{values.notes}</span>
                                </Form.Item>
                            </Col>
                        </Row>
                    </Form>
                </Modal>
            )
        }
        const TableOpt = () => {
            return (
                <div className='table-operations'>
                    <Search
                        style={{ marginBottom: 10, width: 300 }}
                        allowClear
                        placeholder='Search'
                        onSearch={this.queryUser}
                    />
                    <Divider type='vertical' />
                    <Button onClick={this.clearAll}>清除筛选或排序</Button>

                    <Space style={{ float: 'right' }}>
                        {this.state.selectedRowKeys.length ? (
                            <Popconfirm
                                title='是否确认删除选中条目？'
                                okText='确认'
                                cancelText='取消'
                                onConfirm={this.muldelUser}>
                                <Button
                                    type='primary'
                                    icon={<MinusOutlined />}
                                    danger
                                    disabled={!this.state.selectedRowKeys.length}>
                                    批量删除
                                </Button>
                            </Popconfirm>
                        ) : (
                            <Button type='primary' icon={<MinusOutlined />} danger disabled>
                                批量删除
                            </Button>
                        )}
                        <Button type='primary' icon={<PlusOutlined />} onClick={this.showModal.bind(this, null)}>
                            添加
                        </Button>
                    </Space>
                </div>
            )
        }

        return (
            <div>
                <TableOpt />
                <Table
                    className='table'
                    size='middle'
                    columns={columns}
                    dataSource={data}
                    loading={loading}
                    rowSelection={rowSelection}
                    onChange={this.handleChange}
                />
                <FormModal visible={visible} initialValues={initialValues} />
                <DetailModal visible={detailVisible} values={detailValues} />
            </div>
        )
    }
}

const TableView = () => (
    <Layout className='index animated fadeIn'>
        <div>
            <CustomBreadcrumb arr={['用户管理']} />
        </div>
        <div className='face_lib'>
            <div className='base-style'>
                <Table4 />
            </div>
        </div>
    </Layout>
)

export default TableView
