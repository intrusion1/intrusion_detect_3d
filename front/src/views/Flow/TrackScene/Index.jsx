import React, { Component } from 'react'
import { Layout, Row, Col, Divider, Space, Menu, Dropdown, Progress, Button, Input } from 'antd'
import {
    FullscreenOutlined,
    MenuFoldOutlined,
    MenuUnfoldOutlined,
    SettingOutlined,
    DownOutlined,
    AppstoreOutlined,
    YoutubeOutlined
} from '@ant-design/icons'
import screenfull from 'screenfull'
import ReactPlayer from 'react-player'
import '@/style/view-style/index.scss'
import './index.scss'

import { getRealTimeFlow, getTraffic } from '@/api/flow'
import { getDevice, queryDevice } from '@/api/device'
import { switchChannel } from '@/api/video'

const { Search } = Input

class Index extends Component {
    state = {
        screenCount: '1',
        sidebarActive: '1',
        device: [],
        realTimeFlow: [],
        channel: ''
    }
    componentDidMount = () => {
        this.getDevice()
        this.getRealTimeFlow()
        this.timer = setInterval(() => {
            this.getRealTimeFlow()
        }, 5000)
    }
    componentWillUnmount() {
        this.timer && clearInterval(this.timer)
    }
    getDevice = () => {
        getDevice().then(data => {
            this.setState({ device: data.data })
        })
    }
    getRealTimeFlow = () => {
        getRealTimeFlow().then(data => {
            this.setState({ realTimeFlow: data.data })
        })
    }
    getTraffic = () => {
        getTraffic().then(data => {
            this.setState({ traffic: data.data })
        })
    }
    queryDevice = data => {
        queryDevice({ name: data }).then(data => {
            this.setState({ device: data.data })
        })
    }
    fullToggle = () => {
        if (screenfull.isEnabled) {
            screenfull.request(document.getElementById('flow'))
        }
    }
    changeChannel = name => {
        switchChannel({ name }).then(data => {
            const { channel } = data
            this.setState({ channel })
        })
    }
    changeScreenCount = ({ key }) => {
        this.setState({ screenCount: key })
    }
    changeSidebar = sidebarActive => {
        this.setState({ sidebarActive })
    }
    render() {
        const menu = (
            <Menu onClick={this.changeScreenCount}>
                <Menu.Item key='1'>1 屏</Menu.Item>
                <Menu.Item key='4'>4 屏</Menu.Item>
                <Menu.Item key='9'>9 屏</Menu.Item>
            </Menu>
        )
        const FlowItem = ({ data }) => {
            const { name, value } = data
            return (
                <div className='flowItem'>
                    <div>{name}</div>
                    <div style={{ width: '130px' }}>
                        {value <= 25 ? (
                            <Progress percent={value} size='small' showInfo={false} strokeColor='#52c41a' />
                        ) : null}
                        {value > 25 && value <= 50 ? (
                            <Progress percent={value} size='small' showInfo={false} strokeColor='#ffff12' />
                        ) : null}
                        {value > 50 && value <= 75 ? (
                            <Progress percent={value} size='small' showInfo={false} strokeColor='red' />
                        ) : null}
                        {value > 75 && value <= 100 ? (
                            <Progress percent={value} size='small' showInfo={false} strokeColor='#b00a0a' />
                        ) : null}
                    </div>
                </div>
            )
        }
        const DeviceItem = () => (
            <div className='deviceList'>
                {this.state.device.map((item, index) => (
                    <div key={index} style={{ marginBottom: '0' }} onClick={this.changeChannel.bind(this, item.name)}>
                        <Button type='link'>
                            <YoutubeOutlined />
                            {item.name}
                        </Button>
                    </div>
                ))}
            </div>
        )
        const Sidebar = () => (
            <div className='sidebar'>
                <div className='sidebar-item'>
                    <div className='sidebar-item-title'>人流量数据</div>
                    <div className='sidebar-item-content'>
                        {this.state.realTimeFlow.map(item => (
                            <FlowItem data={{ name: item.name, value: item.num }} />
                        ))}
                    </div>
                </div>
                <div className='sidebar-item'>
                    <div className='sidebar-item-title'>监控点</div>
                    <div className='sidebar-item-content'>
                        <Search style={{ marginBottom: 10 }} allowClear size='small' onSearch={this.queryDevice} />
                        <DeviceItem />
                    </div>
                </div>
            </div>
        )
        const ScreenContent = () => {
            if (this.state.screenCount === '1') {
                return (
                    <div className='screen'>
                        <ReactPlayer
                            className='content-cavans-live'
                            url={this.state.channel}
                            playing
                            width='100%'
                            height='100%'
                            controls
                            config={{
                                file: { forceHLS: true }
                            }}
                        />
                    </div>
                )
            } else if (this.state.screenCount === '4') {
                return (
                    <div className='screen'>
                        <div className='screen4' style={{ background: '#666' }}>
                            <ReactPlayer
                                className='content-cavans-live'
                                url={this.state.channel}
                                playing
                                width='100%'
                                height='100%'
                                controls
                                config={{
                                    file: { forceHLS: true }
                                }}
                            />
                        </div>
                        <div className='screen4'></div>
                        <div className='screen4'></div>
                        <div className='screen4' style={{ background: '#666' }}></div>
                    </div>
                )
            } else if (this.state.screenCount === '9') {
                return (
                    <div className='screen'>
                        <div className='screen9' style={{ background: '#666' }}>
                            <ReactPlayer
                                className='content-cavans-live'
                                url={this.state.channel}
                                playing
                                width='100%'
                                height='100%'
                                controls
                                config={{
                                    file: { forceHLS: true }
                                }}
                            />
                        </div>
                        <div className='screen9'></div>
                        <div className='screen9' style={{ background: '#666' }}></div>
                        <div className='screen9'></div>
                        <div className='screen9' style={{ background: '#666' }}></div>
                        <div className='screen9'></div>
                        <div className='screen9' style={{ background: '#666' }}></div>
                        <div className='screen9'></div>
                        <div className='screen9' style={{ background: '#666' }}></div>
                    </div>
                )
            }
        }
        const ScreenFooter = () => (
            <div className='container-content_content--footer'>
                <Space>
                    <MenuFoldOutlined className='cursor' />
                    <Divider type='vertical' className='divider' />
                    <MenuUnfoldOutlined className='cursor' />
                </Space>
                <Space>
                    <Dropdown overlay={menu} placement='topCenter'>
                        <span className='cursor'>
                            <AppstoreOutlined />
                            <DownOutlined className='down' />
                        </span>
                    </Dropdown>
                    <FullscreenOutlined className='cursor' onClick={this.fullToggle} />
                    <Divider type='vertical' className='divider' />
                    <SettingOutlined className='cursor' />
                </Space>
            </div>
        )

        return (
            <Layout className='index animated fadeIn'>
                <div className='container'>
                    <Row className='container-content'>
                        <Col flex='230px' className='height_100'>
                            <div className='container-content_sidebar'>
                                <div className='container-content_sidebar--content'>
                                    <Sidebar />
                                </div>
                                {/* <div className='container-content_sidebar--footer'>
                                    <div
                                        className='cursor'
                                        style={{
                                            fontWeight: this.state.sidebarActive === '1' ? 'bold' : 'normal'
                                        }}
                                        onClick={this.changeSidebar.bind(this, '1')}>
                                        云台
                                    </div>
                                    <div
                                        className='cursor'
                                        style={{
                                            fontWeight: this.state.sidebarActive === '2' ? 'bold' : 'normal'
                                        }}
                                        onClick={this.changeSidebar.bind(this, '2')}>
                                        视频参数
                                    </div>
                                </div> */}
                            </div>
                        </Col>
                        <Col flex='auto' className='height_100'>
                            <div className='container-content_content'>
                                <div id='flow' className='container-content_content--content'>
                                    <ScreenContent />
                                </div>
                                <ScreenFooter />
                            </div>
                        </Col>
                    </Row>
                </div>
            </Layout>
        )
    }
}

export default Index
