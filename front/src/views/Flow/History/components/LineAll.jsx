import React, { Component } from 'react'
import echarts from 'echarts/lib/echarts'
import 'echarts/lib/chart/line'
import 'echarts/lib/component/tooltip'
import 'echarts/lib/component/title'
import 'echarts/lib/component/legend'

class Line extends Component {
    state = {
        myChart: null
    }
    componentDidMount() {
        let myChart = echarts.init(document.getElementById('lineAll'))
        myChart.setOption({
            title: {
                text: this.props.title
            },
            tooltip: {
                trigger: 'axis'
            },
            legend: {
                top: '5%',
                data: ['通道1', '通道2', '通道3', '通道4', '通道5', '通道6', '通道7']
            },
            grid: {
                left: '3%',
                right: '4%',
                bottom: '3%',
                containLabel: true
            },
            toolbox: {
                feature: {
                    saveAsImage: {}
                }
            },
            xAxis: {
                type: 'category',
                boundaryGap: false,
                data: this.props.legend
            },
            yAxis: {
                type: 'value'
            },
            series: [
                {
                    name: '通道1',
                    type: 'line',
                    data: []
                },
                {
                    name: '通道2',
                    type: 'line',
                    data: []
                },
                {
                    name: '通道3',
                    type: 'line',
                    data: []
                },
                {
                    name: '通道4',
                    type: 'line',
                    data: []
                },
                {
                    name: '通道5',
                    type: 'line',
                    data: []
                },
                {
                    name: '通道6',
                    type: 'line',
                    data: []
                },
                {
                    name: '通道7',
                    type: 'line',
                    data: []
                }
            ]
        })
        window.addEventListener('resize', function() {
            myChart.resize()
        })
        this.setState({ myChart })
    }
    componentDidUpdate() {
        this.state.myChart.setOption({
            title: {
                text: this.props.title
            },
            tooltip: {
                trigger: 'axis'
            },
            legend: {
                data: ['通道1', '通道2', '通道3', '通道4', '通道5', '通道6', '通道7']
            },
            grid: {
                left: '3%',
                right: '4%',
                bottom: '3%',
                containLabel: true
            },
            toolbox: {
                feature: {
                    saveAsImage: {}
                }
            },
            xAxis: {
                type: 'category',
                boundaryGap: false,
                data: this.props.legend
            },
            yAxis: {
                type: 'value'
            },
            series: [
                {
                    name: '通道1',
                    type: 'line',
                    data: this.props.data[1]
                },
                {
                    name: '通道2',
                    type: 'line',
                    data: this.props.data[2]
                },
                {
                    name: '通道3',
                    type: 'line',
                    data: this.props.data[3]
                },
                {
                    name: '通道4',
                    type: 'line',
                    data: this.props.data[4]
                },
                {
                    name: '通道5',
                    type: 'line',
                    data: this.props.data[5]
                },
                {
                    name: '通道6',
                    type: 'line',
                    data: this.props.data[6]
                },
                {
                    name: '通道7',
                    type: 'line',
                    data: this.props.data[7]
                }
            ]
        })
    }
    render() {
        return <div id='lineAll' style={{ height: 400, marginBottom: '20px' }}></div>
    }
}

export default Line
