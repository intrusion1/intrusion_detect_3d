import React, { Component } from 'react'
import echarts from 'echarts/lib/echarts'
import 'echarts/lib/chart/line'
import 'echarts/lib/component/tooltip'
import 'echarts/lib/component/title'
import 'echarts/lib/component/legend'

const legendData = [
    '08:00-08:30',
    '08:30-09:00',
    '09:00-09:30',
    '09:30-10:00',
    '10:00-10:30',
    '10:30-11:00',
    '11:00-11:30',
    '11:30-12:00',
    '12:00-12:30',
    '12:30-13:00',
    '13:00-13:30',
    '13:30-14:00',
    '14:00-14:30',
    '14:30-15:00',
    '15:00-15:30',
    '15:30-16:00',
    '16:00-16:30',
    '16:30-17:00',
    '17:00-17:30',
    '17:30-18:00'
]

class Line extends Component {
    state = {
        myChart: null
    }
    componentDidMount() {
        let myChart = echarts.init(document.getElementById('line' + this.props.lineId))
        myChart.setOption({
            title: {
                text: this.props.date + ' 通道' + this.props.lineId
            },
            tooltip: {
                trigger: 'axis'
            },
            // legend: {
            //     data: ['人流量']
            // },
            grid: {
                left: '3%',
                right: '4%',
                bottom: '3%',
                containLabel: true
            },
            toolbox: {
                feature: {
                    saveAsImage: {}
                }
            },
            xAxis: {
                type: 'category',
                boundaryGap: false,
                data: legendData
            },
            yAxis: {
                type: 'value'
            },
            series: [
                {
                    name: '人流量',
                    type: 'line',
                    data: []
                }
            ]
        })
        window.addEventListener('resize', function() {
            myChart.resize()
        })
        this.setState({ myChart })
    }
    componentDidUpdate() {
        this.state.myChart.setOption({
            title: {
                text: this.props.date + ' 通道' + this.props.lineId
            },
            tooltip: {
                trigger: 'axis'
            },
            // legend: {
            //     data: ['人流量']
            // },
            grid: {
                left: '3%',
                right: '4%',
                bottom: '3%',
                containLabel: true
            },
            toolbox: {
                feature: {
                    saveAsImage: {}
                }
            },
            xAxis: {
                type: 'category',
                boundaryGap: false,
                data: legendData
            },
            yAxis: {
                type: 'value'
            },
            series: [
                {
                    name: '人流量',
                    type: 'line',
                    data: this.props.data
                }
            ]
        })
    }
    render() {
        return <div id={'line' + this.props.lineId} style={{ height: 300, marginBottom: '40px' }} />
    }
}

export default Line
