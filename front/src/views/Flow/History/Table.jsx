import React, { Component } from 'react'
import { Layout, Form, Row, Col, DatePicker, Space, Button, Divider } from 'antd'
import { SearchOutlined } from '@ant-design/icons'
import moment from 'moment'
import CustomBreadcrumb from '@/components/CustomBreadcrumb'
import LineEcharts from './components/Line.jsx'
import LineAllEcharts from './components/LineAll.jsx'
import '@/style/view-style/table.scss'

import { queryAnydayFlow, queryPeriodFlow } from '@/api/flow'

class Table4 extends Component {
    state = {
        line1: [],
        line2: [],
        line3: [],
        line4: [],
        line5: [],
        line6: [],
        line7: [],
        line: {},
        legend: [],
        mintime: moment(new Date()).subtract(7, 'days'),
        maxtime: moment(new Date()).subtract(1, 'days'),
        date: moment(new Date()).subtract(1, 'days')
    }
    componentDidMount = () => {
        this.queryAnydayFlow({ date: this.state.maxtime.format('YYYY-MM-DD') })
        this.queryPeriodFlow({
            mintime: this.state.mintime.format('YYYY-MM-DD'),
            maxtime: this.state.maxtime.format('YYYY-MM-DD')
        })
    }
    queryAnydayFlow = params => {
        this.setState({
            date: moment(params.date)
        })
        queryAnydayFlow({
            date: moment(params.date).format('YYYY-MM-DD')
        }).then(res => {
            const { data } = res
            const line1 = data.filter(item => item.identifier === 1).map(val => val.num)
            const line2 = data.filter(item => item.identifier === 2).map(val => val.num)
            const line3 = data.filter(item => item.identifier === 3).map(val => val.num)
            const line4 = data.filter(item => item.identifier === 4).map(val => val.num)
            const line5 = data.filter(item => item.identifier === 5).map(val => val.num)
            const line6 = data.filter(item => item.identifier === 6).map(val => val.num)
            const line7 = data.filter(item => item.identifier === 7).map(val => val.num)
            this.setState({ line1, line2, line3, line4, line5, line6, line7 })
        })
    }
    queryPeriodFlow = params => {
        this.setState({
            mintime: moment(params.mintime),
            maxtime: moment(params.maxtime)
        })
        queryPeriodFlow({
            mintime: moment(params.mintime).format('YYYY-MM-DD'),
            maxtime: moment(params.maxtime).format('YYYY-MM-DD')
        }).then(res => {
            const { data } = res
            const line1 = data
                .filter(item => item.identifier === 1)
                .sort((a, b) => a.date.localeCompare(b.date))
                .map(val => val.num)
            const line2 = data
                .filter(item => item.identifier === 2)
                .sort((a, b) => a.date.localeCompare(b.date))
                .map(val => val.num)
            const line3 = data
                .filter(item => item.identifier === 3)
                .sort((a, b) => a.date.localeCompare(b.date))
                .map(val => val.num)
            const line4 = data
                .filter(item => item.identifier === 4)
                .sort((a, b) => a.date.localeCompare(b.date))
                .map(val => val.num)
            const line5 = data
                .filter(item => item.identifier === 5)
                .sort((a, b) => a.date.localeCompare(b.date))
                .map(val => val.num)
            const line6 = data
                .filter(item => item.identifier === 6)
                .sort((a, b) => a.date.localeCompare(b.date))
                .map(val => val.num)
            const line7 = data
                .filter(item => item.identifier === 7)
                .sort((a, b) => a.date.localeCompare(b.date))
                .map(val => val.num)
            const legend = data
                .filter(item => item.identifier === 1)
                .map(item => item.date)
                .sort()
            const line = {
                1: line1,
                2: line2,
                3: line3,
                4: line4,
                5: line5,
                6: line6,
                7: line7
            }
            this.setState({ line, legend })
        })
    }
    render() {
        const TopOpt = () => {
            const [form] = Form.useForm()
            return (
                <div className='table-operations'>
                    <Form
                        form={form}
                        onFinish={this.queryPeriodFlow}
                        initialValues={{
                            mintime: this.state.mintime,
                            maxtime: this.state.maxtime
                        }}>
                        <Row>
                            <Space>
                                <Form.Item name='mintime'>
                                    <DatePicker style={{ width: '200px' }} placeholder='请选择起始日期' />
                                </Form.Item>
                                <Form.Item>
                                    <span>--</span>
                                </Form.Item>
                                <Form.Item name='maxtime'>
                                    <DatePicker
                                        style={{ width: '200px', marginRight: '10px' }}
                                        placeholder='请选择终止日期'
                                    />
                                </Form.Item>
                                <Form.Item>
                                    <Button type='primary' htmlType='submit' icon={<SearchOutlined />}>
                                        搜索
                                    </Button>
                                </Form.Item>
                            </Space>
                        </Row>
                    </Form>
                </div>
            )
        }
        const BottomOpt = () => {
            const [form] = Form.useForm()
            return (
                <div className='table-operations'>
                    <Form form={form} onFinish={this.queryAnydayFlow} initialValues={{ date: this.state.date }}>
                        <Row>
                            <Space>
                                <Form.Item name='date'>
                                    <DatePicker
                                        format='YYYY-MM-DD'
                                        style={{ width: '200px', marginRight: '10px' }}
                                        placeholder='请选择日期'
                                    />
                                </Form.Item>
                                <Form.Item>
                                    <Button type='primary' htmlType='submit' icon={<SearchOutlined />}>
                                        搜索
                                    </Button>
                                </Form.Item>
                            </Space>
                        </Row>
                    </Form>
                </div>
            )
        }
        return (
            <div>
                <div
                    style={{
                        paddingLeft: '10px',
                        borderLeft: '4px solid #409EFF',
                        fontWeight: 'bold',
                        marginBottom: '20px'
                    }}>
                    某一时期人流量
                </div>
                <div style={{ padding: '0 20px' }}>
                    <TopOpt />
                    <Row>
                        <Col span={24}>
                            <LineAllEcharts
                                data={this.state.line}
                                legend={this.state.legend}
                                title={`${this.state.mintime.format('YYYY-MM-DD')} ~ ${this.state.maxtime.format(
                                    'YYYY-MM-DD'
                                )}`}
                            />
                        </Col>
                    </Row>
                </div>
                <Divider />
                <div
                    style={{
                        paddingLeft: '10px',
                        borderLeft: '4px solid #409EFF',
                        fontWeight: 'bold',
                        marginBottom: '20px',
                        marginTop: '20px'
                    }}>
                    某天各个时间段人流量
                </div>
                <div style={{ padding: '0 20px' }}>
                    <BottomOpt />
                    <Row gutter={16}>
                        <Col span={12}>
                            <LineEcharts
                                lineId='1'
                                data={this.state.line1}
                                date={this.state.date.format('YYYY-MM-DD')}
                            />
                        </Col>
                        <Col span={12}>
                            <LineEcharts
                                lineId='2'
                                data={this.state.line2}
                                date={this.state.date.format('YYYY-MM-DD')}
                            />
                        </Col>
                        <Col span={12}>
                            <LineEcharts
                                lineId='3'
                                data={this.state.line3}
                                date={this.state.date.format('YYYY-MM-DD')}
                            />
                        </Col>
                        <Col span={12}>
                            <LineEcharts
                                lineId='4'
                                data={this.state.line4}
                                date={this.state.date.format('YYYY-MM-DD')}
                            />
                        </Col>
                        <Col span={12}>
                            <LineEcharts
                                lineId='5'
                                data={this.state.line5}
                                date={this.state.date.format('YYYY-MM-DD')}
                            />
                        </Col>
                        <Col span={12}>
                            <LineEcharts
                                lineId='6'
                                data={this.state.line6}
                                date={this.state.date.format('YYYY-MM-DD')}
                            />
                        </Col>
                        <Col span={12}>
                            <LineEcharts
                                lineId='7'
                                data={this.state.line7}
                                date={this.state.date.format('YYYY-MM-DD')}
                            />
                        </Col>
                    </Row>
                </div>
            </div>
        )
    }
}

const TableView = () => (
    <Layout className='index animated fadeIn'>
        <div>
            <CustomBreadcrumb arr={['历史记录']} />
        </div>
        <div className='face_lib'>
            <div className='base-style'>
                <Table4 />
            </div>
        </div>
    </Layout>
)

export default TableView
