import React, { Component, useEffect, useState } from 'react'
import {
    Layout,
    Divider,
    Table,
    Button,
    Input,
    Modal,
    Form,
    Space,
    message,
    Popconfirm,
    InputNumber,
    Row,
    Col,
    Select,
    Image
} from 'antd'
import { getDevice } from '@/api/device'
import { PlusOutlined, EditOutlined, DeleteOutlined, RedoOutlined } from '@ant-design/icons'
import CustomBreadcrumb from '@/components/CustomBreadcrumb'
import '@/style/view-style/table.scss'

import { getTraffic, addTraffic, updateTraffic, delTraffic, queryTraffic, screenshot } from '@/api/flow'

const { Search } = Input
const { Option } = Select

class Table4 extends Component {
    state = {
        data: [],
        storeData: [],
        deviceData: [],
        filteredInfo: null,
        sortedInfo: null,
        selectedRowKeys: [],
        loading: false,
        visible: false,
        initialValues: null,
        modal: {
            x1: null,
            y1: null,
            x2: null,
            y2: null
        }
    }
    componentDidMount = () => {
        this.getTraffic()
        this.getDevice()
    }
    getDevice = () => {
        getDevice().then(data => {
            this.setState({ deviceData: data.data })
        })
    }
    resetLoading = () => {
        const _this = this
        setTimeout(() => {
            if (_this.state.loading) {
                this.setState({ loading: false })
            }
        }, 3000)
    }
    handleResponse = (data, str = '提交') => {
        if (data.status === 'success') {
            message.success(`${str}成功`)
            this.setState({ visible: false })
            this.getTraffic()
        } else {
            message.error(`${str}失败`)
        }
    }
    handleResponseUpdate = (data, str = '提交') => {
        if (data.status === 'success') {
            message.success(`刷新成功`)
            this.setState({ visible: false })
            this.getTraffic()
        } else {
            message.error(`刷新失败`)
        }
    }
    getTraffic = () => {
        this.setState({ loading: true })
        getTraffic().then(data => {
            this.setState({ data: data.data, loading: false })
        })
        this.resetLoading()
    }
    addTraffic = values => {
        const optApi = this.state.initialValues ? updateTraffic : addTraffic
        optApi({ ...values }).then(data => {
            this.handleResponse(data, this.state.initialValues ? '修改' : '添加')
        })
    }
    screenshot = data => {
        this.setState({ loading: true })
        screenshot({ name: data.name }).then(data => {
            this.handleResponseUpdate(data, this.state.initialValues ? '修改' : '添加')
        })
        this.resetLoading()
    }
    delTraffic = record => {
        const { key } = record
        delTraffic({ key }).then(data => {
            this.handleResponse(data, '删除')
        })
    }
    muldelTraffic = () => {
        // muldelTraffic({ key: this.state.selectedRowKeys }).then(data => {
        //     this.handleResponse(data, '删除')
        // })
    }
    queryTraffic = data => {
        this.setState({ loading: true })
        queryTraffic({ name: data }).then(data => {
            this.setState({ data: data.data, loading: false })
        })
        this.resetLoading()
    }
    showModal = initialValues => {
        this.setState({ visible: true, initialValues })
    }
    closeModel = () => {
        this.setState({ visible: false })
    }
    onSelectChange = selectedRowKeys => {
        console.log('selectedRowKeys changed: ', selectedRowKeys)
        this.setState({ selectedRowKeys })
    }
    handleChange = (pagination, filters, sorter) => {
        console.log('Various parameters', pagination, filters, sorter)
        this.setState({ filteredInfo: filters, sortedInfo: sorter })
    }
    clearAll = () => {
        this.setState({ filteredInfo: null, sortedInfo: null })
        this.getTraffic()
    }
    setValue = (key, value) => {
        console.log(key, value)
        const { modal } = this.state
        modal[key] = value
        this.setState({ modal })
    }
    render() {
        const { visible, loading, data, initialValues } = this.state
        let { sortedInfo, selectedRowKeys } = this.state
        sortedInfo = sortedInfo || {}
        const columns = [
            {
                title: '通道名',
                dataIndex: 'name',
                key: 'name',
                sorter: (a, b) => (a.name > b.name ? 1 : -1),
                sortOrder: sortedInfo.columnKey === 'name' && sortedInfo.order,
                ellipsis: true,
                align: 'center',
                render: (value, record) => <b className='table_title'>{value}</b>
            },
            {
                title: '人员上限',
                dataIndex: 'limit',
                key: 'limit',
                align: 'center'
            },
            {
                title: '区域',
                key: 'photo_amended',
                dataIndex: 'photo_amended',
                align: 'center',
                render: value => <Image width={60} src={value} />
            },
            {
                title: '备注',
                dataIndex: 'remark',
                key: 'remark',
                align: 'center'
            },
            {
                title: '时间',
                dataIndex: 'time',
                key: 'time',
                align: 'center'
            },
            {
                title: '操作',
                key: 'action',
                align: 'center',
                render: (text, record) => (
                    <Space size='middle'>
                        <span className='link' onClick={this.screenshot.bind(this, record)}>
                            <RedoOutlined />
                        </span>
                        <span className='link' onClick={this.showModal.bind(this, record)}>
                            <EditOutlined />
                        </span>
                        <Popconfirm
                            placement='topRight'
                            title='是否确认删除该条信息？'
                            okText='确认'
                            cancelText='取消'
                            onConfirm={this.delTraffic.bind(this, record)}>
                            <span className='link danger'>
                                <DeleteOutlined />
                            </span>
                        </Popconfirm>
                    </Space>
                )
            }
        ]
        const rowSelection = {
            selectedRowKeys,
            onChange: this.onSelectChange
        }

        const FormModal = props => {
            const [form] = Form.useForm()
            const { visible, initialValues } = props
            const layout = {
                labelCol: { span: 4 },
                wrapperCol: { span: 19 }
            }
            const tailFormItemLayout = {
                wrapperCol: { span: 19, offset: 4 }
            }
            const halfItemLayout = {
                labelCol: { span: 8 },
                wrapperCol: { span: 16 }
            }

            const [x1, setX1] = useState(false)
            const [y1, setY1] = useState(false)
            const [x2, setX2] = useState(false)
            const [y2, setY2] = useState(false)
            const [change, setChange] = useState(false)
            const [image, setImage] = useState('')

            useEffect(() => {
                if (initialValues && !change) {
                    setX1(initialValues.x1)
                    setY1(initialValues.y1)
                    setX2(initialValues.x2)
                    setY2(initialValues.y2)
                    setImage(initialValues.photo_original)
                }
            })
            const resetForm = () => {
                form.resetFields()
            }
            const setValue = (key, value) => {
                setChange(true)
                switch (key) {
                    case 'x1':
                        if (value < 0) value = 0
                        if (value > 400) value = 400
                        setX1(value)
                        break
                    case 'y1':
                        if (value < 0) value = 0
                        if (value > 225) value = 225
                        setY1(value)
                        break
                    case 'x2':
                        if (value < 0) value = 0
                        if (value > 400) value = 400
                        setX2(value)
                        break
                    case 'y2':
                        if (value < 0) value = 0
                        if (value > 225) value = 225
                        setY2(value)
                        break
                    default:
                        break
                }
                setTimeout(() => {
                    form.setFieldsValue({ [key]: value })
                }, 0)
            }
            const isValid = (x1, y1, x2, y2) => {
                if (
                    (typeof x1 === 'number' || typeof x1 === 'string') &&
                    (typeof y1 === 'number' || typeof y1 === 'string') &&
                    (typeof x2 === 'number' || typeof x2 === 'string') &&
                    (typeof y2 === 'number' || typeof y2 === 'string')
                ) {
                    return true
                }
                return false
            }

            return (
                <Modal
                    visible={visible}
                    title={initialValues ? '修改配置信息' : '添加配置信息'}
                    onCancel={this.closeModel}
                    footer={null}
                    width={700}
                    maskTransitionName=''>
                    <Form
                        {...layout}
                        form={form}
                        layout='horizontal'
                        initialValues={initialValues}
                        onFinish={this.addTraffic}>
                        <Form.Item name='key' style={{ display: 'none' }} />
                        <Form.Item label='通道名' name='name'>
                            <Select showSearch allowClear>
                                {this.state.deviceData.map(item => (
                                    <Option value={item.name} key={item.key}>
                                        {item.name}
                                    </Option>
                                ))}
                            </Select>
                        </Form.Item>
                        <Form.Item
                            label='人员上限'
                            name='limit'
                            rules={[{ required: true, message: '请输入人员上限' }]}>
                            <InputNumber min={0} />
                        </Form.Item>
                        <Row>
                            <Col span={12}>
                                <Form.Item
                                    {...halfItemLayout}
                                    label='左上角x轴坐标'
                                    extra='Range: 0 - 400'
                                    name='x1'
                                    rules={[{ required: true, message: '请输入左上角x轴坐标' }]}>
                                    <InputNumber min={0} max={400} onChange={e => setValue('x1', e)} />
                                </Form.Item>
                            </Col>
                            <Col span={12}>
                                <Form.Item
                                    {...halfItemLayout}
                                    label='左上角y轴坐标'
                                    extra='Range: 0 - 225'
                                    name='y1'
                                    rules={[{ required: true, message: '请输入左上角y轴坐标' }]}>
                                    <InputNumber min={0} max={225} onChange={e => setValue('y1', e)} />
                                </Form.Item>
                            </Col>
                            <Col span={12}>
                                <Form.Item
                                    {...halfItemLayout}
                                    label='右下角x轴坐标'
                                    extra='Range: 0 - 400'
                                    name='x2'
                                    rules={[{ required: true, message: '请输入右下角x轴坐标' }]}>
                                    <InputNumber min={0} max={400} onChange={e => setValue('x2', e)} />
                                </Form.Item>
                            </Col>
                            <Col span={12}>
                                <Form.Item
                                    {...halfItemLayout}
                                    label='右下角y轴坐标'
                                    extra='Range: 0 - 225'
                                    name='y2'
                                    rules={[{ required: true, message: '请输入右下角y轴坐标' }]}>
                                    <InputNumber min={0} max={225} onChange={e => setValue('y2', e)} />
                                </Form.Item>
                            </Col>
                        </Row>
                        <Form.Item label='备注' name='remark'>
                            <Input.TextArea allowClear placeholder='请输入备注信息' />
                        </Form.Item>
                        <Form.Item label='区域'>
                            <Image width={400} src={image} />
                            <div style={{ position: 'absolute', top: '0', left: '0', width: '400px', height: '225px' }}>
                                <div
                                    style={{
                                        visibility: isValid(x1, y1, x2, y2) ? 'visible' : 'hidden',
                                        border: '1px solid red',
                                        position: 'absolute',
                                        left: x1 + 'px',
                                        top: 225 - y1 + 'px',
                                        right: 400 - x2 + 'px',
                                        bottom: y2 + 'px'
                                    }}
                                />
                            </div>
                        </Form.Item>
                        <Form.Item {...tailFormItemLayout}>
                            <Button type='primary' loading={loading} style={{ marginRight: 10 }} htmlType='submit'>
                                提交
                            </Button>
                            {initialValues ? null : <Button onClick={resetForm}>清空</Button>}
                        </Form.Item>
                    </Form>
                </Modal>
            )
        }
        const TableOpt = () => {
            return (
                <div className='table-operations'>
                    <Search
                        style={{ marginBottom: 10, width: 300 }}
                        allowClear
                        placeholder='搜索'
                        onSearch={this.queryTraffic}
                    />
                    <Divider type='vertical' />
                    <Button onClick={this.clearAll}>清除筛选或排序</Button>

                    <Space style={{ float: 'right' }}>
                        {this.state.selectedRowKeys.length ? (
                            <Popconfirm
                                title='是否确认删除选中条目？'
                                okText='确认'
                                cancelText='取消'
                                onConfirm={this.muldelTraffic}>
                                <Button
                                    type='primary'
                                    icon={<DeleteOutlined />}
                                    danger
                                    disabled={!this.state.selectedRowKeys.length}>
                                    批量删除
                                </Button>
                            </Popconfirm>
                        ) : (
                            <Button type='primary' icon={<DeleteOutlined />} danger disabled>
                                批量删除
                            </Button>
                        )}
                        <Button type='primary' icon={<PlusOutlined />} onClick={this.showModal.bind(this, null)}>
                            添加
                        </Button>
                    </Space>
                </div>
            )
        }

        return (
            <div>
                <TableOpt />
                <Table
                    className='table'
                    size='middle'
                    columns={columns}
                    dataSource={data}
                    loading={loading}
                    rowSelection={rowSelection}
                    onChange={this.handleChange}
                />
                <FormModal visible={visible} initialValues={initialValues} />
            </div>
        )
    }
}

const TableView = () => (
    <Layout className='index animated fadeIn'>
        <div>
            <CustomBreadcrumb arr={['参数配置']} />
        </div>
        <div className='face_lib'>
            <div className='base-style'>
                <Table4 />
            </div>
        </div>
    </Layout>
)

export default TableView
