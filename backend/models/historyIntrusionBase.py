# coding=utf-8
from flask.globals import request
from flask.json import jsonify
from backend import db, app
import numpy as np
from datetime import datetime
import json
from backend.settings import ip_port
history_ip = ip_port()

# def single_to_dict(self):
#         return {c.name: getattr(self, c.name) for c in self.__table__.columns}
class history(db.Model):
    """历史记录表"""
    __tablename__ = 'history_intrusion'
    key = db.Column(db.Integer, primary_key=True)
    photo = db.Column(db.String(1024))
    label = db.Column(db.String(64))
    device = db.Column(db.String(64))
    time = db.Column(db.DateTime,default=datetime.utcnow())
    remark = db.Column(db.String(32),index=64)

    def return_json(self):
        """返回特定数据"""
        dic = {}
        dic['key'] = self.key
        dic['photo'] = self.photo
        dic['label'] = self.label
        dic['device'] = self.device
        dic['time'] = self.time.strftime('%Y-%m-%d %X')
        dic['remark'] = self.remark
        return dic
    
    @classmethod
    def init_history(cls):
        base_infos = history.query.order_by(history.key.desc()).all()
        r_json = {}
        r_json['data'] = []
        for b_info in base_infos:
            r_json['data'].append(b_info.return_json())
        return r_json
    
    # @classmethod
    # def query_history(cls,data):
    #     data = json.loads(request.get_data())
    #     # print("我开始啦！！！！！！！！！！！！！")
    #     print(data)
    #     name = data['name']
    #     histories = cls.query.filter_by(name = name).all()
    #     r_json = {'data': []}
    #     for h in histories.items:
    #         r_json.append(h.return_json())
    #     print(r_json)
    #     return r_json
