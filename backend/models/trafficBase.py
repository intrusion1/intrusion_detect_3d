# coding=utf-8
from flask.globals import request
from flask.json import jsonify
from backend import db, app
import numpy as np
from datetime import datetime
import json
from backend.settings import ip_port
ip=ip_port()
class traffic(db.Model):
    """通道配置表"""
    __tablename__ = 'traffic'
    key = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), index=True)
    limit = db.Column(db.Integer, index=True)
    area = db.Column(db.String(1024))
    time=db.Column(db.DateTime,default=datetime.utcnow())
    remark=db.Column(db.String(32),index=64)
    photo_original = db.Column(db.String(1024))
    photo_amended = db.Column(db.String(1024))

    def return_json(self):
        """返回特定数据"""
        dic = {}
        area = self.area.split(',')
        dic['key'] = self.key
        dic['name'] = self.name
        dic['limit'] = self.limit
        dic['x1'] = area[0]
        dic['y1'] = area[1]
        dic['x2'] = area[2]
        dic['y2'] = area[3]
        #dic['area'] = ip+'/'+self.area
        dic['time'] = self.time.strftime('%Y-%m-%d %X')
        dic['remark'] = self.remark
        dic['photo_original'] = self.photo_original
        dic['photo_amended']  = self.photo_amended
        return dic
    
    @classmethod
    def init_traffic(cls):
        base_infos = traffic.query.order_by(traffic.key.desc()).all()
        r_json = {}
        r_json['data'] = []
        for b_info in base_infos:
            r_json['data'].append(b_info.return_json())
        return r_json
