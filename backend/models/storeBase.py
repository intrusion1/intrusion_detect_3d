from flask.globals import request
from flask.json import jsonify
from backend import db, app
import numpy as np
from datetime import datetime
import json

class store(db.Model):
    """人脸底裤表"""
    __tablename__ = 'storeroom'
    key = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), index=True)
    face = db.Column(db.String(128))
    time=db.Column(db.DateTime,default=datetime.utcnow())
    label=db.Column(db.String(32),index=64)
    note=db.Column(db.String(32),index=64)

    def return_json(self):
        """返回特定数据"""
        dic = {}
        dic['key'] = self.key
        dic['name'] = self.name
        dic['face'] = self.face
        dic['label'] = self.label
        dic['note'] = self.note
        dic['time'] = self.time.strftime('%Y-%m-%d %X')
        return dic

    @classmethod
    def init_storeroom(cls):
        base_infos = store.query.order_by(store.key.desc()).all()
        r_json = {}
        r_json['data'] = []
        for b_info in base_infos:
            r_json['data'].append(b_info.return_json())
        return r_json