# coding=utf-8
from flask.globals import request
from flask.json import jsonify
from backend import db, app
import numpy as np
from datetime import datetime
import json
from backend.settings import ip_port

ip = ip_port()


class realTimeFlow(db.Model):
    """实时人流量记录表"""
    __tablename__ = 'realTimeFlow'
    key = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), index=True)
    num = db.Column(db.Integer, index=True)
    up = db.Column(db.Integer, index=True)
    down = db.Column(db.Integer, index=True)
    last_up = db.Column(db.Integer, index=True)
    last_down = db.Column(db.Integer, index=True)


    def return_json(self):
        """返回特定数据"""
        dic = {}
        dic['key'] = self.key
        dic['name'] = self.name
        dic['num'] = self.num
        dic['up'] = self.up
        dic['down'] = self.down
        dic['last_up'] = self.last_up
        dic['last_down'] = self.last_down
        return dic

    @classmethod
    def init_realTimeFlow(cls):
        base_infos = realTimeFlow.query.order_by(realTimeFlow.key.desc()).all()
        r_json = {}
        r_json['data'] = []
        for b_info in base_infos:
            r_json['data'].append(b_info.return_json())
        return r_json

db.create_all()