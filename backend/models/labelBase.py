# coding=utf-8
from flask.globals import request
from flask.json import jsonify
from backend import db, app
import numpy as np
from datetime import datetime
import json

class labelR(db.Model):
    """人脸底裤表"""
    __tablename__ = 'label'
    key = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), index=True)
    type = db.Column(db.String(64), index=True)
    time=db.Column(db.DateTime,default=datetime.utcnow())

    def return_json(self):
        """返回特定数据"""
        dic = {}
        dic['key'] = self.key
        dic['name'] = self.name
        dic['type'] = self.type
        dic['time'] = self.time.strftime('%Y-%m-%d %X')
        return dic
    
    @classmethod
    def init_faceLib(cls):
        base_infos = labelR.query.order_by(labelR.key.desc()).all()
        r_json = {}
        r_json['data'] = []
        for b_info in base_infos:
            r_json['data'].append(b_info.return_json())
        return r_json