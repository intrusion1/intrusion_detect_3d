from backend import db,app
from datetime import datetime
import json
from flask import request

#数据库类
class User(db.Model):
    __tablename__='user'
    key=db.Column(db.Integer,primary_key=True)
    username=db.Column(db.String(64),index=True)
    password=db.Column(db.String(128),index=True)
    role=db.Column(db.String(32),index=True)
    time=db.Column(db.DateTime,default=datetime.utcnow())
    others=db.Column(db.String(32),index=True)

    def return_json(self):
        dic = {}
        dic['key'] = self.key
        dic['username'] = self.username
        dic['password'] = self.password
        dic['role'] = self.role
        dic['createTime'] = self.time.strftime('%Y-%m-%d %X')
        dic['notes'] = self.others
        return dic

    @classmethod
    def init_user(cls):
        """ 返回第一页历史信息 """
        user_infos = User.query.order_by(User.key.desc()).all()
        #user_infos = cls.query.order_by(cls.key.desc()).paginate(1, app.config['POSTS_PER_PAGE'], False)
        r_json ={}
        r_json['data'] = []
        for b_user in user_infos:
            r_json['data'].append(b_user.return_json())
        return r_json

    @classmethod
    def query_user(cls, data):
        data = json.loads(request.get_data())
        print(data)
        username = data['username']
        users = cls.query.filter_by(username=username)
        r_json = {}
        r_json['data'] = []
        for user in users.items:
            r_json.append(user.return_json())
        print(r_json)
        return r_json

