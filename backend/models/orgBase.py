from flask.json import jsonify
from zeep.xsd.elements.indicators import Group
from backend import db
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy_mptt import mptt_sessionmaker
from sqlalchemy import Column, Integer, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy_mptt.mixins import BaseNestedSets
import sqlalchemy_mptt

Base = declarative_base()

'''映射定义结构'''
class Tree(Base, BaseNestedSets):
    __tablename__ = "organization"
    id = db.Column(Integer, primary_key=True)
    name = db.Column(String(8))
    #parent_id = db.Column(Integer)

    def __repr__(self):
        return "<Node (%s)>" % self.id
    def return_json(self):
        dic = {}
        dic['key'] = self.id
        dic['title'] = self.name
        #dic['parent_id'] = self.parent_id
        return dic



    @classmethod
    def initdata(parent=1):
        group = db.session.query(Tree).filter_by().all()
        r_arr = []
        for item in group:
            if item.parent_id is None:
                r_arr.append(item.drilldown_tree(json=True,json_fields=cat_to_json)[0])
        r_json = {}
        r_json['data'] = r_arr
        return r_json

def cat_to_json(item):
    return {
        'key': item.id,
        'title': item.name
    }


'''
    @classmethod
    def initdata(tab=1):
        group = db.session.query(Tree).filter_by(parent_id=None).all()  # type: TreeGroup
        print(group)
        r_json = {}
        r_json['data'] = []
        if not group:
            return
        j = 0
        for i in group:
            r_json['data'].append(i.return_json())
            #print('- ' * tab + i.name)
            r_json['data'][j]['children'] = []
            z = 0
            for child_group in i.children:  # type: TreeGroup
                # new tabulation value for child record
                r_json['data'][j]['children'].append(child_group.return_json())
                r_json['data'][j]['children'][z]['children'] = []
                for child_group_2 in child_group.children:
                    r_json['data'][j]['children'][z]['children'].append(child_group_2.return_json())
                z += 1
            j += 1
        return r_json
'''