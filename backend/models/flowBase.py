# coding=utf-8
from flask.globals import request
from flask.json import jsonify
from backend import db, app
import numpy as np
from datetime import datetime
import json
from backend.settings import ip_port

ip = ip_port()


class flow(db.Model):
    """人流量分析历史记录表"""
    __tablename__ = 'flow'
    key = db.Column(db.Integer, primary_key=True)
    identifier = db.Column(db.Integer, index=True)
    date = db.Column(db.DateTime, default=datetime.utcnow())
    num = db.Column(db.Integer, index=True)
    interval = db.Column(db.Integer,index=True)
    # remark = db.Column(db.String(64))
    # channel_id = db.Column(db.Integer, index=True)
    up = db.Column(db.Integer, index=True)
    down = db.Column(db.Integer, index=True)
    def return_json(self):
        """返回特定数据"""
        dic = {}
        dic['key'] = self.key
        dic['identifier'] = self.identifier
        dic['num'] = self.num
        dic['date'] = self.date.strftime('%Y-%m-%d')
        dic['interval'] = self.interval
        dic['up'] = self.up
        dic['down'] = self.down
        # dic['remark'] = self.remark
        # dic['channel_id'] = self.channel_id
        return dic

    @classmethod
    def init_flow(cls):
        base_infos = flow.query.order_by(flow.key.desc()).all()
        r_json = {}
        r_json['data'] = []
        for b_info in base_infos:
            r_json['data'].append(b_info.return_json())
        return r_json