# coding=utf-8
from flask.globals import request
from flask.json import jsonify
from backend import db, app
import numpy as np
from datetime import datetime
import json
from backend.settings import ip_port
history_ip = ip_port()

def single_to_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}
class history(db.Model):
    """历史记录表"""
    __tablename__ = 'history'
    key = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), index=True)
    gender = db.Column(db.String(20))
    age = db.Column(db.Integer, index=True)
    photo = db.Column(db.String(1024))
    id = db.Column(db.String(64))
    part = db.Column(db.String(64))
    phone = db.Column(db.String(64))
    label = db.Column(db.String(64))
    device = db.Column(db.String(64))
    store = db.Column(db.String(64))
    time = db.Column(db.DateTime,default=datetime.utcnow())
    remark = db.Column(db.String(32),index=64)
    key_device = db.Column(db.Integer, index=True)
    channel_id = db.Column(db.Integer, index=True)

    def return_json(self):
        """返回特定数据"""
        dic = {}
        dic['key'] = self.key
        dic['name'] = self.name
        dic['gender'] = self.gender
        dic['age'] = self.age
        dic['photo'] = self.photo
        dic['id'] = self.id
        dic['part'] = self.part
        dic['phone'] = self.phone
        dic['label'] = self.label
        dic['device'] = self.device
        dic['store'] = self.store
        dic['time'] = self.time.strftime('%Y-%m-%d %X')
        dic['remark'] = self.remark
        dic['key_device'] = self.key_device
        dic['channel_id'] = self.channel_id
        return dic
    
    @classmethod
    def init_history(cls):
        base_infos = history.query.order_by(history.key.desc()).all()
        r_json = {}
        r_json['data'] = []
        for b_info in base_infos:
            r_json['data'].append(b_info.return_json())
        return r_json
    @classmethod
    def query_history(cls,data):
        data = json.loads(request.get_data())
        name = data['name']
        historys = cls.query.filter_by(name=name).all()
        r_json = {}
        r_json['data'] = []
        #face_dic = dict(faces)
        for history_face in historys.items:
            r_json.append(history_face.return_json())
        return r_json
