# coding=utf-8
from backend import db, app
from datetime import datetime
import json

class device(db.Model):
    """设备列表"""
    __tablename__ = 'device'
    key = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64),index=True)
    area = db.Column(db.String(64))
    protocol = db.Column(db.String(64))
    address = db.Column(db.String(64))
    port = db.Column(db.String(64))
    account = db.Column(db.String(64))
    password = db.Column(db.String(64))
    serial = db.Column(db.String(64))
    brand = db.Column(db.String(64))
    type = db.Column(db.String(64))
    version = db.Column(db.String(64))
    time=db.Column(db.DateTime,default=datetime.utcnow())
    note=db.Column(db.String(32),index=64)
    store = db.Column(db.String(64))
    recognition = db.Column(db.String(64))
    detection = db.Column(db.String(64))
    intrusion = db.Column(db.String(64))
    """对接"""
    mixed = db.Column(db.String(64))
    channel_id = db.Column(db.Integer)
    important= db.Column(db.String(64))
    stranger = db.Column(db.String(64))

    def return_json(self):
        """返回特定数据"""
        dic = {}
        dic['key'] = self.key
        dic['name'] = self.name
        dic['area'] = self.area
        dic['protocol'] = self.protocol
        dic['address'] = self.address
        dic['port'] = self.port
        dic['account'] = self.account
        dic['password'] = self.password
        dic['serial'] = self.serial
        dic['brand'] = self.brand
        dic['type'] = self.type
        dic['version'] = self.version
        dic['note'] = self.note
        dic['store'] = self.store
        dic['time'] = self.time.strftime('%Y-%m-%d %X')
        dic['recognition'] = self.recognition
        dic['detection'] = self.detection
        dic['intrusion'] = self.intrusion
        dic['mixed'] = self.mixed
        dic['channel_id'] = self.channel_id
        dic['important'] = self.important
        dic['stranger'] = self.stranger
        return dic
    @classmethod
    def init_device(self):
        base_infos = device.query.order_by(device.key.desc()).all()
        r_json = {}
        r_json['data'] = []
        for b_info in base_infos:
            r_json['data'].append(b_info.return_json())
        return r_json