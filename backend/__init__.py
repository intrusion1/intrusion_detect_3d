from flask import Flask,render_template,g,session
from flask import Flask,redirect,url_for,render_template,request,jsonify
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from config import Config
from flask_jwt_extended import JWTManager
from backend.utils.minio import Bucket
from backend.settings import minio_ip,minio_port

app = Flask(__name__,template_folder="templates",static_folder="static",static_url_path="/backend/static")
    #app = Flask(__name__,template_folder="/front_end/",static_folder="/front_end",static_url_path="/front_end")    #注册蓝图
app.config.from_object(Config)
db = SQLAlchemy(app)
migrate = Migrate(app,db)
jwt = JWTManager(app)
minio_obj = Bucket(service= '{}:{}'.format(minio_ip,minio_port), access_key="root", secret_key="bupt1234",secure=False)


@jwt.expired_token_loader
def my_expired_token_callback(jwt_header, jwt_payload):
    return jsonify({"code": 401, "message": "token expired"})

@jwt.invalid_token_loader
def my_invalid_token_callback(message):
    return jsonify({"code": 402, "message": message})

@jwt.token_verification_failed_loader
def my_token_verification_failed_loader_callback(jwt_header, jwt_payload):
    return jsonify({"code": 403, "message": "token_verification_failed"})

@jwt.unauthorized_loader
def my_unauthorized_loader_callback(message):
    return jsonify({"code": 404, "message": message})

from flask_apscheduler import APScheduler
from backend.views.realTimeFlow import schedule_task
scheduler = APScheduler()
scheduler.add_job(id="job1", func=schedule_task, trigger="cron", minute="0,30")
scheduler.start()

from backend import route



