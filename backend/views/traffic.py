# encoding:utf-8
import uuid
from PIL import Image

from flask import Blueprint, jsonify, request, Response, session
from backend import db
from backend.models.trafficBase import traffic
import json
import os
from sqlalchemy import func
import datetime
import shutil
from backend.utils.draw_area import pilImgDraw
from backend.models.storeBase import store
from backend.models.deviceBase import device
from backend.models.realTimeFlowBase import realTimeFlow
from backend import minio_obj
from backend.settings import ip_port, stream_name
import cv2
traffic_bp = Blueprint('traffic',__name__)

@traffic_bp.route('/api/traffic',methods=['GET'])
def getTraffic():
    r_json = traffic.init_traffic()
    for info in r_json['data']:
        info['photo_original'] = ip_port()+"/edge/" + str(info['photo_original'])
        info['photo_amended'] = ip_port()+"/edge/" + str(info['photo_amended'])
    return r_json



#添加（添加时需要新建图片以及绘框,并将图片路径插入数据库,并插入traffic表与realtimeflow表）
@traffic_bp.route('/api/add_traffic', methods=['POST'])
def add_traffic():
    data = json.loads(str(request.get_data(),'utf-8'))
    name = data['name']
    limit = data['limit']
    x1 = data['x1']
    y1 = data['y1']
    x2 = data['x2']
    y2 = data['y2']
    area = str(x1)+","+str(y1)+","+str(x2)+","+str(y2)
    remark = data['remark'] if 'ramark' in data else ''

    traffic_infos = db.session.query(traffic).filter(traffic.name == name).all()
    if len(traffic_infos) != 0:
        return jsonify({'status': 'fail'})
    # key = db.session.query(func.max(traffic.key)).first()[0]
    device_infos = db.session.query(device).filter(device.name == name).all()
    if len(device_infos) == 0:
        return jsonify({'status': 'fail'})
    device_info = device_infos[0]
    key = device_info.key
    uid = uuid.uuid1()
    file_name = "{}_{}.jpg".format(name, uid)


    # #这一步应该用截图替代
    # shutil.copyfile("./backend/data/area/origin.png",
    #                 "./backend/data/area/original/" + file_name)

    #截图
    account = "live"
    # password = device_info.password
    password = device_info.key
    # address = device_info.address
    port = device_info.port
    address = stream_name
    channel = "http://{}:{}/{}/{}.flv".format(address, port, account, password)
    capture = cv2.VideoCapture(channel)
    val, frame = capture.read()
    cv2.imwrite("./backend/data/area/original/{}.jpg".format(name), frame)
    src = "./backend/data/area/original/{}.jpg".format(name)
    dst = "./backend/data/area/original/" + file_name
    print(dst)
    os.rename(src, dst)
    minio_obj.upload_file("edge", 'area/original/' + file_name, dst, ".jpg")

    pilImgDraw(file_name, int(x1), int(y1), int(x2), int(y2))

    traffic_info = traffic(
        key=key,
        name=name,
        limit=limit,
        area=area,
        remark=remark,
        photo_amended="area/amendment/" + file_name,
        photo_original="area/original/" + file_name,
        time=datetime.datetime.now()
    )
    realTimeFlow_info = realTimeFlow(
        key=key,
        name=name,
        num=0,
        up=0,
        down=0,
        last_down=0,
        last_up=0
    )

    if traffic_info is None or realTimeFlow_info is None:
        return jsonify({'status':'fail'})
    else:
        db.session.add(traffic_info)
        db.session.add(realTimeFlow_info)
        db.session.commit()
        return jsonify({'status':'success'})

#查询
@traffic_bp.route('/api/query_traffic',methods=['POST'])
def query_traffic():
    data = json.loads(str(request.get_data(),'utf-8'))
    # print(request.headers)
    name = data['name'] if "name" in data else ""
    if name is "":
        query_name = db.session.query(traffic).filter().all()
    else:
        query_name = db.session.query(traffic).filter(traffic.name == name).all()
    r_json = {}
    r_json['data']=[]
    for name in query_name:
        r_json['data'].append(traffic.return_json(name))
    for info in r_json['data']:
        info['photo_original'] = "http://"+ip_port()+"/edge/" + str(info['photo_original'])
        info['photo_amended'] = "http://"+ip_port()+"/edge/" + str(info['photo_amended'])
    return jsonify(r_json)

#删除（需要将图片一并删除）
@traffic_bp.route('/api/del_traffic',methods=['POST'])
def del_traffic():
    data = json.loads(str(request.get_data(),'utf-8'))
    key = data['key']
    traffic_info = db.session.query(traffic).filter(traffic.key == key).first()
    image_name_amended = traffic_info.photo_amended if traffic_info.photo_amended is not None else 'none'
    image_name_original = traffic_info.photo_original if traffic_info.photo_original is not None else 'none'
    file_name = "./backend/data/" + image_name_amended
    if os.path.exists(file_name):
        os.remove(file_name)
        minio_obj.remove_file("edge", 'area/amendment/' + image_name_amended)
        print('成功删除文件:', file_name)
    else:
        print('未找到此文件:', file_name)

    file_name = "./backend/data/" + image_name_original
    if os.path.exists(file_name):
        os.remove(file_name)
        print('成功删除文件:', file_name)
        minio_obj.remove_file("edge", 'area/original/' + image_name_original)
    else:
        print('未找到此文件:', file_name)
    realTimeFlow_info = db.session.query(realTimeFlow).filter(realTimeFlow.key == key).first()
    if traffic_info is None or realTimeFlow_info is None:
        return jsonify({'status':'fail'})
    else:
        db.session.delete(traffic_info)
        db.session.delete(realTimeFlow_info)
        db.session.commit()
        return jsonify({'status':'success'})

#修改(修改时需要将框图删除，将原图名字更改，再按照坐标将生成框图）
@traffic_bp.route('/api/update_traffic',methods=['POST'])
def update_traffic():
    data = json.loads(str(request.get_data(),'utf-8'))
    key = data['key']
    name = data['name']
    limit = data['limit']
    x1 = data['x1']
    y1 = data['y1']
    x2 = data['x2']
    y2 = data['y2']
    area = str(x1)+","+str(y1)+","+str(x2)+","+str(y2)
    remark = data['remark']

    traffic_info = db.session.query(traffic).filter(traffic.key == key).first()
    image_name_amended = traffic_info.photo_amended
    image_name_original = traffic_info.photo_original

    #删除旧的框图
    file_name = "./backend/data/" + str(image_name_amended)
    if os.path.exists(file_name):
        os.remove(file_name)
        minio_obj.remove_file("edge", 'area/amendment/' + image_name_amended)
        print('成功删除文件:', file_name)
    else:
        print('未找到此文件:', file_name)

    #将原图改名
    uid = uuid.uuid1();
    src = "./backend/data/" + str(image_name_original)
    file_name = "{}_{}.jpg".format(name, uid)
    dst = "./backend/data/area/original/" + file_name
    print(dst)
    os.rename(src, dst)

    #暂时用删除与上传替代修改文件名
    minio_obj.remove_file("edge", 'area/original/' + str(image_name_original))
    minio_obj.upload_file("edge", 'area/original/' + file_name, dst, ".jpg")

    #创建新的框图
    pilImgDraw(file_name, int(x1), int(y1), int(x2), int(y2))

    # url = "http://10.38.30.170:8000//area//amendment//"+str(key)+".png"
    image_name_original = "area/original/"+file_name
    image_name_amended = "area/amendment/"+file_name
    minio_obj.upload_file("edge", 'area/amendment/' + file_name, "./backend/data/area/amendment/" + file_name, ".jpg")
    db.session.query(traffic).filter(traffic.key==key).update({"name":name,
        "limit":limit,"area":area,"remark":remark,"photo_original":image_name_original,
        "photo_amended":image_name_amended})
    db.session.query(realTimeFlow).filter(realTimeFlow.key==key).update({
        "name": name
    })
    db.session.commit()
    return jsonify({'status':'success'})

'''截图'''
@traffic_bp.route('/api/screenshot',methods=['POST'])
def screenshot():
    data = json.loads(str(request.get_data(),'utf-8'))
    name = data['name']
    print(name)
    device_info = db.session.query(device).filter(device.name==name).first()
    traffic_info = db.session.query(traffic).filter(traffic.name == name).first()
    area = traffic_info.area
    point = area.split(',')
    # account = device_info.account
    account = "live"
    # password = device_info.password
    password = device_info.key
    # address = device_info.address
    port = device_info.port
    address = stream_name
    channel = "http://{}:{}/{}/{}.flv".format(address,port,account,password)
    capture = cv2.VideoCapture(channel)
    val, frame = capture.read()
    cv2.imwrite("./backend/data/area/original/{}.jpg".format(name),frame)
    uid = uuid.uuid1();
    src = "./backend/data/area/original/{}.jpg".format(name)
    file_name = "{}_{}.jpg".format(name, uid)
    dst = "./backend/data/area/original/" + file_name
    print(dst)
    os.rename(src, dst)
    minio_obj.upload_file("edge", 'area/original/'+file_name, dst,".jpg")
    pilImgDraw(file_name, int(point[0]), int(point[1]), int(point[2]), int(point[3]))
    image_name_original = "area/original/"+file_name
    image_name_amended = "area/amendment/"+file_name


    image_original = traffic_info.photo_original
    image_amended = traffic_info.photo_amended
    # 删除旧图
    filename_original = "./backend/data/" + image_original
    if os.path.exists(filename_original):
        os.remove(filename_original)
        minio_obj.remove_file("edge", 'area/original/'+image_original)
        print('成功删除文件:', filename_original)
    else:
        print('未找到此文件:', filename_original)

    filename_amended = "./backend/data/" + image_amended
    if os.path.exists(filename_amended):
        os.remove(filename_amended)
        minio_obj.remove_file("edge", 'area/amendment/'+image_amended)
        print('成功删除文件:', filename_amended)
    else:
        print('未找到此文件:', filename_amended)
    db.session.query(traffic).filter(traffic.name==name).update({"photo_original":image_name_original, "photo_amended":image_name_amended})
    db.session.commit()
    return jsonify({'status':'success'})
