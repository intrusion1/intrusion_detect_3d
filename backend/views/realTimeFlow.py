from flask import Blueprint, jsonify, request, Response, session
from backend import db
from backend.models.trafficBase import traffic
import json
import os
from sqlalchemy import func
from datetime import datetime, date, timedelta
from backend.models.realTimeFlowBase import realTimeFlow
from backend.models.flowBase import flow
real_time_flow_bp = Blueprint('real_time_flow',__name__)

@real_time_flow_bp.route('/api/real_time_flow',methods=['GET'])
def real_time_flow_list():
    r_json = realTimeFlow.init_realTimeFlow()
    return r_json

#修改（给后端算法）
@real_time_flow_bp.route('/api/update_real_time_flow',methods=['POST'])
def update_real_time_flow():
    data = json.loads(str(request.get_data(),'utf-8'))
    key = data['key']
    num = data['num']

    db.session.query(realTimeFlow).filter(realTimeFlow.key==key).update({"num":num})
    db.session.commit()
    return jsonify({'status':'success'})

#添加(测试)
@real_time_flow_bp.route('/api/add_real_time_flow',methods=['POST'])
def add_real_time_flow():
    data = json.loads(str(request.get_data(),'utf-8'))
    name = data['name']
    num = data['num']
    key = db.session.query(func.max(realTimeFlow.key)).first()[0]
    if key is None:
        key = 0
    real_time_flow_info = realTimeFlow(
        key=key+1,
        name=name,
        num=num
    )
    if real_time_flow_info is None:
        return jsonify({'status':'fail'})
    else:
        db.session.add(real_time_flow_info)
        db.session.commit()
        return jsonify({'status':'success'})

#删除(用于测试)
@real_time_flow_bp.route('/api/del_real_time_flow',methods=['POST'])
def del_real_time_flow():
    data = json.loads(str(request.get_data(),'utf-8'))
    key = data['key']
    real_time_flow_info = realTimeFlow().query.filter_by(key=key).first()
    if real_time_flow_info is None:
        return jsonify({'status':'fail'})
    else:
        db.session.delete(real_time_flow_info)
        db.session.commit()
        return jsonify({'status':'success'})


#定时任务（暴露接口用于测试）
@real_time_flow_bp.route('/api/schedule_task',methods=['get'])
def schedule_task():
    with db.app.app_context():
        query_list = db.session.query(realTimeFlow).filter().all()
        for real_time_flow_info in query_list:
            up = real_time_flow_info.up
            down = real_time_flow_info.down
            name = real_time_flow_info.name
            key = real_time_flow_info.key
            num = real_time_flow_info.num
            last_up = real_time_flow_info.last_up
            last_down = real_time_flow_info.last_down
            db.session.query(realTimeFlow).filter(realTimeFlow.key == key).update(
                {"last_up": up, "last_down": down})
            db.session.commit()
            if(last_up>up):
                last_up = 0
            if(last_down>down):
                last_down = 0
            cur = datetime.now()
            date_now = cur.strftime('%Y-%m-%d')
            hour = cur.hour
            minute = cur.minute
            interval = hour*2
            if(minute >= 30):
                interval = interval+1
            if interval==0:
                date_now = (date.today() + timedelta(days=-1)).strftime("%Y-%m-%d")
                interval = 48
            num_flow = up+down-(last_up+last_down)
            flow_key = db.session.query(func.max(flow.key)).first()[0]
            flow_info = flow(
                key=flow_key + 1,
                identifier=key,
                date = date_now,
                num = num_flow,
                interval=interval,
                up = up-last_up,
                down = down-last_down
                )
            if flow_info is None:
                return jsonify({'status': 'fail'})
            else:
                db.session.add(flow_info)
                db.session.commit()
        return jsonify({'status':'success'})
    return jsonify({'status':'error'})