# coding=utf-8
from flask import Blueprint, jsonify, request, make_response, session
from flask_jwt_extended import jwt_required
from sqlalchemy.sql.expression import false
from backend import db
from backend.models.deviceBase import device
from backend.models.historyBase import history
from sqlalchemy import func
import cv2
import numpy as np
import json
import datetime
import time
from backend.utils.date_util import dateTOTime
from io import BytesIO
import xlwt,xlrd
from backend.settings import minio_ip,minio_port,ip_port
import uuid
from backend.face_recognition.test.embeddings import embeddings

history_bp = Blueprint('history', __name__)

'''获取数据'''
@history_bp.route('/api/history_face', methods=['GET'])
def history_face():
    r_json = history.init_history()
    for info in r_json['data']:
        info['photo'] = ip_port()+"/edge" + str(info['photo'])
    return jsonify(r_json)

'''黑名单数据'''
@history_bp.route('/api/history_blacklist',methods=['GET'])
def blacklist():
    query_label = db.session.query(history).order_by(history.key.desc()).filter(history.label=="黑名单").all()
    r_json = {}
    r_json['data']=[]
    for name in query_label:
        r_json['data'].append(history.return_json(name))
    for info in r_json['data']:
        info['photo'] = ip_port()+"/edge" + str(info['photo'])
    return jsonify(r_json)

'''其他人员数据'''
@history_bp.route('/api/history_whitelist',methods=['GET'])
def whitelist():
    query_label = db.session.query(history).order_by(history.key.desc()).filter(history.label!="黑名单").all()
    r_json = {}
    r_json['data']=[]
    for name in query_label:
        r_json['data'].append(history.return_json(name))
    for info in r_json['data']:
        info['photo'] = ip_port()+"/edge" + str(info['photo'])
    return jsonify(r_json)

'''删除'''
@history_bp.route('/api/del_history',methods=['POST'])
def del_history():
    print(request.get_data())
    data = json.loads(str(request.get_data(),'utf-8'))
    key = data['key']
    history_info = history().query.filter_by(key=key).first()
    if history_info is None:
        return jsonify({'status':'fail'})
    else: 
        db.session.delete(history_info)
        db.session.commit()
        return jsonify({'status':'success'})

'''历史记录查询（可以修改为动态查询）'''
@history_bp.route('/api/query_history', methods=['POST'])
def query_history():
    data = json.loads(str(request.get_data(), 'utf-8'))
    name = data['name'] if "name" in data else ""
    part = data['part'] if "part" in data else ""
    gender = data['gender'] if "gender" in data else ""
    label = data['label'] if "label" in data else ""
    minage = data['minage'] if "minage" in data else 1
    maxage = data['maxage'] if "maxage" in data else db.session.query(func.max(history.age)).first()[0]
    maxtime = data['maxtime'] + " 23:59:59" if "maxtime" in data else datetime.datetime.now().strftime('%Y-%m-%d %X')
    mintime = data['mintime'] + " 00:00:00" if "mintime" in data else "1970-01-01 00:00:00"
    device = data['device'] if "device" in data else ""
    if name is "":
        if part is "":
            if gender is "":
                if device is "":
                    if label is "":
                        query_name = db.session.query(history).filter(history.age >= minage, history.age <= maxage,
                                                                      history.time >= mintime, history.time <= maxtime).all()
                    else:
                        query_name = db.session.query(history).filter(history.age >= minage, history.age <= maxage,
                                                                      history.label == label, history.time >= mintime,
                                                                      history.time <= maxtime).all()
                else:
                    if label is "":
                        query_name = db.session.query(history).filter(history.age >= minage, history.age <= maxage,
                                                                      history.time >= mintime, history.time <= maxtime,
                                                                      history.device == device).all()
                    else:
                        query_name = db.session.query(history).filter(history.age >= minage, history.age <= maxage,
                                                                      history.time >= mintime, history.time <= maxtime,
                                                                      history.device == device, history.label == label).all()
            else:
                if device is "":
                    if label is "":
                        query_name = db.session.query(history).filter(history.age >= minage, history.age <= maxage,
                                                                      history.time >= mintime, history.time <= maxtime,
                                                                      history.gender == gender).all()
                    else:
                        query_name = db.session.query(history).filter(history.age >= minage, history.age <= maxage,
                                                                      history.time >= mintime, history.time <= maxtime,
                                                                      history.gender == gender, history.label == label).all()
                else:
                    if label is "":
                        query_name = db.session.query(history).filter(history.age >= minage, history.age <= maxage,
                                                                      history.time >= mintime, history.time <= maxtime,
                                                                      history.gender == gender, history.device == device).all()
                    else:
                        query_name = db.session.query(history).filter(history.age >= minage, history.age <= maxage,
                                                                      history.time >= mintime, history.time <= maxtime,
                                                                      history.device == device, history.label == label,
                                                                      history.gender == gender).all()
        else:
            if gender is "":
                if device is "":
                    if label is "":
                        query_name = db.session.query(history).filter(history.age >= minage, history.age <= maxage,
                                                                      history.time >= mintime, history.time <= maxtime,
                                                                      history.part == part).all()
                    else:
                        query_name = db.session.query(history).filter(history.age >= minage, history.age <= maxage,
                                                                      history.time >= mintime, history.time <= maxtime,
                                                                      history.label == label, history.part == part).all()
                else:
                    if label is "":
                        query_name = db.session.query(history).filter(history.age >= minage, history.age <= maxage,
                                                                      history.time >= mintime, history.time <= maxtime,
                                                                      history.device == device, history.part == part).all()
                    else:
                        query_name = db.session.query(history).filter(history.age >= minage, history.age <= maxage,
                                                                      history.time >= mintime, history.time <= maxtime,
                                                                      history.device == device, history.label == label,
                                                                      history.part == part).all()
            else:
                if device is "":
                    if label is "":
                        query_name = db.session.query(history).filter(history.age >= minage, history.age <= maxage,
                                                                      history.time >= mintime, history.time <= maxtime,
                                                                      history.gender == gender, history.part == part).all()
                    else:
                        query_name = db.session.query(history).filter(history.age >= minage, history.age <= maxage,
                                                                      history.time >= mintime, history.time <= maxtime,
                                                                      history.gender == gender, history.label == label,
                                                                      history.part == part).all()
                else:
                    if label is "":
                        query_name = db.session.query(history).filter(history.age >= minage, history.age <= maxage,
                                                                      history.time >= mintime, history.time <= maxtime,
                                                                      history.device == device, history.part == part,
                                                                      history.gender == gender).all()
                    else:
                        query_name = db.session.query(history).filter(history.age >= minage, history.age <= maxage,
                                                                      history.time >= mintime, history.time <= maxtime,
                                                                      history.device == device, history.label == label,
                                                                      history.part == part,history.gender == gender).all()
    else:
        if part is "":
            if gender is "":
                if device is "":
                    if label is "":
                        query_name = db.session.query(history).filter(history.age >= minage, history.age <= maxage,
                                                                      history.time >= mintime, history.time <= maxtime,
                                                                      history.name == name).all()
                    else:
                        query_name = db.session.query(history).filter(history.age >= minage, history.age <= maxage,
                                                                      history.time >= mintime, history.time <= maxtime,
                                                                      history.label == label, history.name == name).all()
                else:
                    if label is "":
                        query_name = db.session.query(history).filter(history.age >= minage, history.age <= maxage,
                                                                      history.time >= mintime, history.time <= maxtime,
                                                                      history.device == device, history.name == name).all()
                    else:
                        query_name = db.session.query(history).filter(history.age >= minage, history.age <= maxage,
                                                                      history.device == device, history.label == label,
                                                                      history.time >= mintime, history.time <= maxtime,
                                                                      history.name == name).all()
            else:
                if device is "":
                    if label is "":
                        query_name = db.session.query(history).filter(history.age >= minage, history.age <= maxage,
                                                                      history.time >= mintime, history.time <= maxtime,
                                                                      history.gender == gender, history.name == name).all()
                    else:
                        query_name = db.session.query(history).filter(history.age >= minage, history.age <= maxage,
                                                                      history.time >= mintime, history.time <= maxtime,
                                                                      history.gender == gender, history.label == label,
                                                                      history.name == name).all()
                else:
                    if label is "":
                        query_name = db.session.query(history).filter(history.age >= minage, history.age <= maxage,
                                                                      history.time >= mintime, history.time <= maxtime,
                                                                      history.device == device, history.name == name,
                                                                      history.gender == gender).all()
                    else:
                        query_name = db.session.query(history).filter(history.age >= minage, history.age <= maxage,
                                                                      history.time >= mintime, history.time <= maxtime,
                                                                      history.device == device, history.label == label,
                                                                      history.name == name, history.gender == gender).all()
        else:
            if gender is "":
                if device is "":
                    if label is "":
                        query_name = db.session.query(history).filter(history.age >= minage, history.age <= maxage,
                                                                      history.time >= mintime, history.time <= maxtime,
                                                                      history.part == part, history.name == name).all()
                    else:
                        query_name = db.session.query(history).filter(history.age >= minage, history.age <= maxage,
                                                                      history.time >= mintime, history.time <= maxtime,
                                                                      history.label == label, history.part == part,
                                                                      history.name == name).all()
                else:
                    if label is "":
                        query_name = db.session.query(history).filter(history.age >= minage, history.age <= maxage,
                                                                      history.time >= mintime, history.time <= maxtime,
                                                                      history.device == device, history.part == part,
                                                                      history.name == name).all()
                    else:
                        query_name = db.session.query(history).filter(history.age >= minage, history.age <= maxage,
                                                                      history.time >= mintime, history.time <= maxtime,
                                                                      history.device == device, history.label == label,
                                                                      history.part == part, history.name == name).all()
            else:
                if device is "":
                    if label is "":
                        query_name = db.session.query(history).filter(history.age >= minage, history.age <= maxage,
                                                                      history.time >= mintime, history.time <= maxtime,
                                                                      history.gender == gender, history.part == part,
                                                                      history.name == name).all()
                    else:
                        query_name = db.session.query(history).filter(history.age >= minage, history.age <= maxage,
                                                                      history.time >= mintime, history.time <= maxtime,
                                                                      history.gender == gender, history.label == label,
                                                                      history.part == part, history.name == name).all()
                else:
                    if label is "":
                        query_name = db.session.query(history).filter(history.age >= minage, history.age <= maxage,
                                                                      history.time >= mintime, history.time <= maxtime,
                                                                      history.device == device, history.part == part,
                                                                      history.name == name, history.gender == gender).all()
                    else:
                        query_name = db.session.query(history).filter(history.age >= minage, history.age <= maxage,
                                                                      history.time >= mintime, history.time <= maxtime,
                                                                      history.device == device, history.label == label,
                                                                      history.part == part, history.name == name,
                                                                      history.gender == gender).all()
    r_json = {}
    r_json['data'] = []
    for name in query_name:
        r_json['data'].append(history.return_json(name))
    for info in r_json['data']:
        info['photo'] = ip_port()+"/edge" + str(info['photo'])
    return jsonify(r_json)


'''批量删除'''
@history_bp.route('/api/muldel_history',methods=['POST'])
def muldel():
    data = json.loads(str(request.get_data(),'utf-8'))
    print(data)
    for key in data['key']:
        history_info = history().query.filter_by(key=key).first()
        if history_info is None:
            return jsonify({'status':'fail'})
        else:
            db.session.delete(history_info)
            db.session.commit()
    return jsonify({'status':'success'})

'''统计数据'''
@history_bp.route('/api/statistic_history',methods=['POST'])
def statistic_history():
    data = json.loads(str(request.get_data(),'utf-8'))
    maxtime = data['maxtime'] if "maxtime" in data else datetime.datetime.now().strftime('%Y-%m-%d %X')
    mintime = data['mintime'] if "mintime" in data else "1970-01-01 00:00:00"
    devicename = data['device']
    query_name = db.session.query(history).filter(history.time>=mintime,history.time<=maxtime,history.device==devicename).all()
    r_json = {}
    r_json['data'] = []
    for name in query_name:
        r_json['data'].append(history.return_json(name))
    # print(r_json)
    male = 0
    female = 0
    age1 = 0
    age2 = 0
    age3 = 0
    age4 = 0
    part_set = {}
    label_set = {}
    for record in r_json['data']:
        if record['gender']=='男':
            male=male+1
        elif record['gender']=='女':
            female=female+1
        if record['age']<18:
            age1=age1+1
        elif record['age']>17 and record['age']<36:
            age2=age2+1
        elif record['age']>35 and record['age']<61:
            age3=age3+1
        else:
            age4=age4+1
        if record['part'] in part_set:
            part_set[record['part']] = part_set[record['part']]+1
        else:
            part_set[record['part']] = 1
        if record['label'] in label_set:
            label_set[record['label']] = label_set[record['label']]+1
        else:
            label_set[record['label']] = 1
    r_json['data'] = []

    g_json={}
    g_json['gender']=[]
    gender_num1 = {}
    gender_num1['type'] = 'male'
    gender_num1['value'] = male
    g_json['gender'].append(gender_num1)
    gender_num2 = {}
    gender_num2['type'] = 'female'
    gender_num2['value'] = female
    g_json['gender'].append(gender_num2)

    a_json={}
    a_json['age']=[]
    age_num1 = {}
    age_num1['type'] = '0-17岁'
    age_num1['value'] = age1
    a_json['age'].append(age_num1)
    age_num2 = {}
    age_num2['type'] = '14-35岁'
    age_num2['value'] = age2
    a_json['age'].append(age_num2)
    age_num3 = {}
    age_num3['type'] = '36-60岁'
    age_num3['value'] = age3
    a_json['age'].append(age_num3)
    age_num4 = {}
    age_num4['type'] = '61岁及以上'
    age_num4['value'] = age4
    a_json['age'].append(age_num4)

    p_json={}
    p_json['part']=[]
    for key in part_set:
        part_name={}
        part_name['type']=key
        part_name['value']=part_set[key]
        p_json['part'].append(part_name)

    l_json = {}
    l_json['label'] = []
    for key in label_set:
        label_name = {}
        label_name['type'] = key
        label_name['value'] = label_set[key]
        l_json['label'].append(label_name)

    r_json['data'].append(g_json)
    r_json['data'].append(a_json)
    r_json['data'].append(p_json)
    r_json['data'].append(l_json)
    return jsonify(r_json)


'''导出为excel'''
@history_bp.route('/api/export_history',methods=['GET'])
def export_history():
    datas = db.session.query(history.name,history.age,history.gender,history.photo,history.id,history.part,history.phone,history.remark,history.time).all()
    #datastyle = xlwt.XFStyle()
    #datastyle.num_format_str = 'yyyy-mm-dd hh:mm:ss'
    fields = ['姓名','年龄','性别','照片','证件号','单位','手机号','备注']
    book = xlwt.Workbook(encoding='utf-8')
    sheet = book.add_sheet('用户信息')
    for col,field in enumerate(fields):
        sheet.write(0,col,field)
    row = 1
    for data in datas:
        for col,field in enumerate(data[0:-1]):
            sheet.write(row,col,field)
        row += 1
    sio = BytesIO()
    book.save(sio)
    sio.seek(0)
    response = make_response(sio.getvalue())
    response.headers['Content-type'] = 'application/vnd.ms-excel;charset=UTF-8'
    response.headers['Content-Disposition'] = 'attachment; filename=persons.xls'
    response.headers['Access-Control-Allow-Origin'] = 'Content-Disposition'
    response.headers['Access-Control-Allow-Method'] = '*'
    response.headers['Access-Control-Allow-Headers'] = '*'
    return response

#------------------------------------对接------------------------------#
@history_bp.route('/api/ipc/recognize/passerby/get', methods=['GET'])
@jwt_required(optional=False)
def query_passberby():
    page_index= int(request.args.get("page_index")) if "page_index" in request.args else 1
    page_size=int(request.args.get("page_size")) if "page_size" in request.args else 20
    end_time = int(request.args.get("end_time")) if "end_time" in request.args else time.mktime(datetime.datetime.now().timetuple())
    start_time = int(request.args.get("start_time")) if "start_time" in request.args else 0
    channel_id = int(request.args.get("channel_id")) if request.args.get("channel_id")!='' else 0
    facelib_name = request.args.get("facelib_name") if "facelib_name" in request.args else ""
    sex = request.args.get("sex") if "sex" in request.args else ""
    maxtime = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(end_time))
    mintime = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(start_time))

    filters = [
        history.time >= mintime,
        history.time <= maxtime,
        history.remark == "nvr",
    ]
    if channel_id is not 0:
        filters.append(history.channel_id == channel_id)
    if facelib_name is not "":
        filters.append(history.store == facelib_name)
    if sex is not "":
        filters.append(history.gender == sex)
    query_list = db.session.query(history).filter(*filters).all()
    total_count = len(query_list)
    r_json = {}
    r_json['code'] = 0
    r_json['message'] = "ok"
    r_json['total_count'] = total_count
    r_json['data'] = []
    for history_info in query_list[(page_index-1)*page_size:min(page_index*page_size,total_count)]:
        info = {}
        ip = minio_ip
        port = minio_port
        info['face_url'] = '{}/{}/{}'.format(ip_port(),"edge", history_info.photo)
        info['create_time'] = dateTOTime(str(history_info.time))
        info['ip'] = ip
        info['sex'] = ''
        if (history_info.gender == '男'):
            info['sex'] = 'male'
        elif (history_info.gender == '女'):
            info['sex'] = 'female'
        info['channel_id'] =  history_info.channel_id
        r_json['data'].append(info)
    return jsonify(r_json)


@history_bp.route('/api/ipc/recognize/important/get', methods=['GET'])
@jwt_required(optional=False)
def query_important():
    page_index= int(request.args.get("page_index")) if "page_index" in request.args else 1
    page_size=int(request.args.get("page_size")) if "page_size" in request.args else 20
    end_time = int(request.args.get("end_time")) if "end_time" in request.args else time.mktime(datetime.datetime.now().timetuple())
    start_time = int(request.args.get("start_time")) if "start_time" in request.args else 0
    channel_id = int(request.args.get("channel_id")) if request.args.get("channel_id")!='' else 0
    facelib_name = request.args.get("facelib_name") if "facelib_name" in request.args else ""
    maxtime = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(end_time))
    mintime = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(start_time))
    filters = [
        history.time >= mintime,
        history.time <= maxtime,
        history.remark == "nvr",
        history.name != "未知",
    ]
    if channel_id is not 0:
        filters.append(history.channel_id == channel_id)
    if facelib_name is not "":
        filters.append(history.store == facelib_name)
    query_list = db.session.query(history).filter(*filters).all()
    total_count = len(query_list)
    r_json = {}
    r_json['code'] = 0
    r_json['message'] = "ok"
    r_json['total_count'] = total_count
    r_json['data'] = []
    for history_info in query_list[(page_index-1)*page_size:min(page_index*page_size,total_count)]:
        info = {}
        ip = minio_ip
        port = minio_port
        info['face_url'] = '{}/{}/{}'.format(ip_port(),"edge", history_info.photo)
        info['create_time'] = dateTOTime(str(history_info.time))
        info['ip'] = ip
        info['person_name'] = history_info.name
        info['sex'] = ''
        if (history_info.gender == '男'):
            info['sex'] = 'male'
        elif (history_info.gender == '女'):
            info['sex'] = 'female'
        info['ages'] = ''  # history_info.age
        info['person_id'] = history_info.id
        info['phone'] = history_info.phone
        info['facelib_name'] = history_info.store
        info['channel_id'] =  history_info.channel_id
        r_json['data'].append(info)
    return jsonify(r_json)

@history_bp.route('/api/ipc/recognize/stranger/get', methods=['GET'])
@jwt_required(optional=False)
def query_stranger():
    page_index = int(request.args.get("page_index")) if "page_index" in request.args else 1
    page_size = int(request.args.get("page_size")) if "page_size" in request.args else 20
    end_time = int(request.args.get("end_time")) if "end_time" in request.args else time.mktime(
        datetime.datetime.now().timetuple())
    start_time = int(request.args.get("start_time")) if "start_time" in request.args else 0
    channel_id = int(request.args.get("channel_id")) if request.args.get("channel_id")!='' else 0
    maxtime = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(end_time))
    mintime = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(start_time))

    #ages
    #sex

    filters = [
        history.time >= mintime,
        history.time <= maxtime,
        history.remark == "nvr",
        history.name == "未知",
    ]
    if channel_id is not 0:
        filters.append(history.channel_id == channel_id)
    query_list = db.session.query(history).filter(*filters).all()
    total_count = len(query_list)
    r_json = {}
    r_json['code'] = 0
    r_json['message'] = "ok"
    r_json['total_count'] = total_count
    r_json['data'] = []
    for history_info in query_list[(page_index-1)*page_size:min(page_index*page_size,total_count)]:
        info = {}
        ip = minio_ip
        port = minio_port
        info['face_url'] = '{}/{}/{}'.format(ip_port(),"edge", history_info.photo)
        info['create_time'] = dateTOTime(str(history_info.time))
        info['ip'] = ip
        info['sex'] = ''
        info['ages'] = '未知'             #history_info.age
        info['channel_id'] =  history_info.channel_id
        r_json['data'].append(info)
    return jsonify(r_json)