from array import array
from flask import Blueprint, jsonify, request, Response, session
from flask_jwt_extended import jwt_required
from sqlalchemy.sql.operators import exists, notendswith_op
from backend import db
import backend
from backend.models.storeBase import store
from backend.models.faceBase import face
import json
from sqlalchemy import func
import datetime
from backend.face_recognition.test.embeddings import embeddings
import os,uuid,shutil

store_bp = Blueprint('store',__name__)

'''返回所有的人脸库'''
@store_bp.route('/api/storeroom', methods=['GET'])
def storeroom():
    r_json = store.init_storeroom()
    return jsonify(r_json)

'''返回人脸库中所有人脸信息'''
@store_bp.route('/api/storeFace',methods=['POST'])
def storeFace():
    data = json.loads(str(request.get_data(),'utf-8')) # {"key":1}
    id = data['key']
    storeFace = store().query.filter_by(key=id)
    sto_json = {}
    sto_json['data'] = []
    for i in storeFace:
        sto_json['data'].append(store.return_json(i))
    str_face = sto_json['data'][0]['face']
    face_json = {}
    face_json['data'] = []
    if str_face is None:
        return face_json
    array_face = str_face.split(',')
    face_id_array=[]
    for i in array_face:
        face_id = db.session.query(face).filter(face.key==i).first()
        if face_id:
            face_id_array.append(str(face_id.key))
        else:
            pass
    face_id_str = ','+','.join(face_id_array)+',' #新的face_id
    for i in face_id_array:
        name = db.session.query(face).filter(face.key==i).first()
        if name:
            face_json['data'].append(face.return_json(name))
    db.session.query(store).filter(store.key==id).update({"face":face_id_str})
    db.session.commit()
    return face_json

'''
在人脸库中添加/减少人员信息
@store_bp.route('/api/storeUpdateFace',methods=['POST']) 
def storeUpdateFace():
    data = json.loads(str(request.get_data(),'utf-8')) # {"key":1,"face":"1,2,3,4,5,6,7,8,9,10,46,47,48,50"}
    key = data['key']
    face_id = data['face']
    face_id_arr = face_id.split(',')
    face_id_arr_update = ','.join(face_id_arr)
    db.session.query(store).filter(store.key==key).update({"face":face_id_arr_update})
    db.session.commit()
    return jsonify({'status':'success'})
'''

'''创建新的人脸库'''
@store_bp.route('/api/add_store',methods=['POST'])
def add_store():
    data = json.loads(str(request.get_data(),'utf-8')) # {"name":"B库","label":"白名单"}
    key = db.session.query(func.max(store.key)).first()[0]; #查询`key`最大值
    if key is None:
        key=0
    name = data['name']
    label = data['label']
    os.makedirs("backend/data/store/{}_{}".format(key+1,uuid.uuid1()))
    if "note" in data:
        note = data['note']
    else:
        note = ''
    store_info = store(key=key+1,name=name,label=label,face=',',note=note,time=datetime.datetime.now())
    if store_info is None:
        return jsonify({'status':'fail'})
    else:
        db.session.add(store_info)
        db.session.commit()
        return jsonify({'status':'success'})

'''修改人脸库'''
@store_bp.route('/api/update_store', methods=['POST'])
def update_store():
    data = json.loads(str(request.get_data(),'utf-8'))
    key = data['key']
    name = data['name']
    label = data['label']
    if "note" in data:
        note = data['note']
    else:
        note = ''
    db.session.query(store).filter(store.key==key).update({
        "name":name,
        "label":label,
        "note":note
    })
    db.session.commit()
    return jsonify({'status':'success'})

'''删除人脸库'''
@store_bp.route('/api/del_store',methods=['POST'])
def del_store():
    data = json.loads(str(request.get_data(),'utf-8'))
    key = data['key']
    people = db.session.query(store).filter(store.key==key).first()
    name = people.name
    print(name)
    path = "backend/data/store/"
    for i,f in enumerate(os.listdir(path)):
        if f.find('{}_'.format(key))>=0:
            shutil.rmtree(path+f)
        #os.removedirs(path)
    store_info = store().query.filter_by(key=key).first()
    db.session.delete(store_info)
    db.session.commit()
    return jsonify({"status":"success"})

'''查询人脸库'''
@store_bp.route('/api/query_store',methods=['POST'])
def query_store():
    data = json.loads(str(request.get_data(),'utf-8'))
    name = data['name'] if "name" in data else ""
    label = data['label'] if "label" in data else ""
    if name is "":
        if label is "":
            query_name = db.session.query(store).filter().all()
        else:
            query_name = db.session.query(store).filter(store.label==label).all()
    else:
        if label is "":
            query_name = db.session.query(store).filter(store.name==name).all()
        else:
            query_name = db.session.query(store).filter(store.name==name,store.label==label)
    r_json = {}
    r_json['data']=[]
    for name in query_name:
        r_json['data'].append(store.return_json(name))
    return jsonify(r_json)

'''删除人脸库中的人员'''
@store_bp.route('/api/from_store_del_face', methods=['POST'])
def from_store_del_face():
    data = json.loads(str(request.get_data(),'utf-8'))
    key_store = data['key_store']
    key_face = str(data['key_face'])
    face_id = db.session.query(store.face).filter(store.key==key_store).all()
    face_id_str = ''.join(face_id[0])
    face_id_str = face_id_str.replace(','+str(key_face+','),',')
    #删除路径中的图片
    store_ = db.session.query(store).filter(store.key==key_store).first()
    face_  = db.session.query(face).filter(face.key==key_face).first()
    path = "backend/data/store/{}_{}".format(store_.key,store_.name)
    if os.path.exists(path):
        for k,f in enumerate(os.listdir(path)):
            if f.find('{}_'.format(face_.key))>=0:
                os.remove(path+'/'+f)
    #更新人员库中的人员
    db.session.query(store).filter(store.key==key_store).update({"face":face_id_str})
    db.session.commit()
    return jsonify({"status":"success"})

'''批量删除人脸库中的人员'''
@store_bp.route('/api/from_store_muldel_face',methods=['POST'])
def from_store_muldel_face():
    data = json.loads(str(request.get_data(),'utf-8'))
    key_store = data['key_store']
    key_face = str(data['key_face']).split(',')
    face_id = db.session.query(store.face).filter(store.key==key_store).all()
    face_id_str = ''.join(face_id[0])
    for i in key_face:
        face_id_str = face_id_str.replace(','+i+',',',')
    db.session.query(store).filter(store.key==key_store).update({"face":face_id_str})
    db.session.commit()
    return jsonify({"status":"success"})

'''生成embeddings 删除/添加/导入/修改/批量删除'''
@store_bp.route('/api/embeddings',methods=['POST'])
def faceEmb():
    data = json.loads(str(request.get_data(),'utf-8'))
    key = data['key']
    store_path = "backend/data/store"
    for i,f in enumerate(os.listdir(store_path)):
        if f.find('{}_'.format(key))>=0:
            store_name = f
    emb_path = "backend/data/store/{}/embedding.h5".format(store_name)
    image_path = "backend/data/store/{}".format(store_name)
    print(image_path,emb_path)
    embeddings(emb_path,image_path)
    return jsonify({"status":"success"})

#------------------------------------对接------------------------------#

'''创建新的人脸库'''
@store_bp.route('/api/ipc/facelib/create',methods=['POST'])
@jwt_required(optional=False)
def create_facelib():
    data = json.loads(str(request.get_data(),'utf-8')) # {"name":"B库","label":"白名单"}
    key = db.session.query(func.max(store.key)).first()[0]; #查询`key`最大值
    if key is None:
        key=0
    name = data['name']
    #label = data['nvr_ip']

    label='白名单'

    os.makedirs("backend/data/store/{}_{}".format(key+1,uuid.uuid1()))
    if "note" in data:
        note = data['note']
    else:
        note = 'nvr'
    store_info = store(key=key+1,name=name,label=label,face=',',note=note,time=datetime.datetime.now())
    if store_info is None:
        return jsonify({"code":1, "message": "error"})
    else:
        db.session.add(store_info)
        db.session.commit()
        return jsonify({"code":0,"message":"ok"})

'''修改人脸库'''
@store_bp.route('/api/ipc/facelib/modify', methods=['POST'])
@jwt_required(optional=False)
def modify_facelib():
    data = json.loads(str(request.get_data(),'utf-8'))
    new_name = data['new_name']
    name = data['name']
    # label = data['nvr_ip']

    db.session.query(store).filter(store.name==name).update({
        "name":new_name,
    })
    db.session.commit()
    return jsonify({"code":0,"message":"ok"})

'''删除人脸库'''
@store_bp.route('/api/ipc/facelib/delete',methods=['POST'])
@jwt_required(optional=False)
def delete_facelib():
    data = json.loads(str(request.get_data(),'utf-8'))
    name = data['name']
    facelib = db.session.query(store).filter(store.name==name).first()
    name = facelib.name
    key = facelib.key
    print(name)
    print(key)
    path = "backend/data/store/"
    for i,f in enumerate(os.listdir(path)):
        if f.find('{}_'.format(key))>=0:
            shutil.rmtree(path+f)
        #os.removedirs(path)
    store_info = store().query.filter_by(key=key).first()
    db.session.delete(store_info)
    db.session.commit()
    return jsonify({"code":0,"message":"ok"})

'''返回平台创建的所有的人脸库'''
@store_bp.route('/api/ipc/facelib/get', methods=['GET'])
@jwt_required(optional=False)
def get_facelib():
    query_name = db.session.query(store).filter(store.note=='nvr').all()
    r_json = {}
    r_json['code'] = 0
    r_json['message'] = "ok"
    r_json['data'] = []
    name = ""
    for record in query_name:
        r_json['data'].append({"name":record.name})

    print(r_json)
    return jsonify(r_json)