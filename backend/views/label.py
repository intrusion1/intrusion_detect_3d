from flask import Blueprint, jsonify, request
from flask.wrappers import Request
from sqlalchemy.sql.elements import Label
from backend import db
from sqlalchemy import func
from backend.models.labelBase import labelR
import json
from sqlalchemy import func
import datetime

label_bp = Blueprint('label',__name__)

'''获取数据'''
@label_bp.route('/api/label', methods=['GET'])
def labelRoom():
    '''初始页面'''
    r_json = labelR.init_faceLib()
    return jsonify(r_json)

'''人员标签'''
@label_bp.route('/api/personLabel',methods=['GET'])
def personLabel():
    query_label = db.session.query(labelR).filter(labelR.type=="人员标签").all()
    r_json = {}
    r_json['data']=[]
    for name in query_label:
        r_json['data'].append(labelR.return_json(name))
    return jsonify(r_json)

'''人脸库标签'''
@label_bp.route('/api/storeLabel',methods=['GET'])
def storeLabel():
    query_label = db.session.query(labelR).filter(labelR.type=="人脸库标签").all()
    r_json = {}
    r_json['data']=[]
    for name in query_label:
        r_json['data'].append(labelR.return_json(name))
    return jsonify(r_json)

'''添加标签'''
@label_bp.route('/api/add_label', methods=['POST'])
def add_label():
    data = json.loads(str(request.get_data(),'utf-8'))
    name = data['name']
    type = data['type']
    key = db.session.query(func.max(labelR.key)).first()[0]
    if key is None:
        key = 0
    label_info = labelR(
        key=key+1,
        name=name,
        type=type,
        time=datetime.datetime.now()
    )
    if label_info is None:
        return jsonify({'status':'fail'})
    else:
        db.session.add(label_info)
        db.session.commit()
        return jsonify({'status':'success'})

'''修改标签'''
@label_bp.route('/api/update_label',methods=['POST'])
def update_label():
    data = json.loads(str(request.get_data(),'utf-8'))
    key = data['key']
    name = data['name']
    type = data['type']
    db.session.query(labelR).filter(labelR.key==key).update({"name":name,"type":type})
    db.session.commit()
    return jsonify({'status':'success'})

'''删除标签'''
@label_bp.route('/api/del_label',methods=['POST'])
def del_label():
    data = json.loads(str(request.get_data(),'utf-8'))
    key = data['key']
    label_info = labelR().query.filter_by(key=key).first()
    if label_info is None:
        return jsonify({'status':'fail'})
    else:
        db.session.delete(label_info)
        db.session.commit()
        return jsonify({'status':'success'})
    
'''批量删除'''
@label_bp.route('/api/muldel_label',methods=['POST'])
def muldel_label():
    data = json.loads(str(request.get_data(),'utf-8'))
    for key in data['key']:
        label_info = labelR().query.filter_by(key=key).first()
        if label_info is None:
            return jsonify({'status':'fail'})
        else:
            db.session.delete(label_info)
            db.session.commit()
    return jsonify({'status':'success'})

'''查询标签'''
@label_bp.route('/api/query_label',methods=['POST'])
def query_label():
    data = json.loads(str(request.get_data(),'utf-8'))
    name = data['name'] if "name" in data else ""
    type = data['type'] if "type" in data else ""
    if name is "":
        if type is "":
            label_name = db.session.query(labelR).filter().all()
        else:
            label_name = db.session.query(labelR).filter(labelR.type==type).all()
    else:
        if type is "":
            label_name = db.session.query(labelR).filter(labelR.name==name).all()
        else:
            label_name = db.session.query(labelR).filter(labelR.type==type,labelR.name==name).all()
    r_json = {}
    r_json['data'] = []
    for name in label_name:
        r_json['data'].append(labelR.return_json(name))
    return jsonify(r_json)