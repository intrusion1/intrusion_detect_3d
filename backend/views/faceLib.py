# coding=utf-8
from operator import and_
from flask import Blueprint, jsonify, request, make_response, session
from flask_jwt_extended import jwt_required
from sqlalchemy.sql.expression import label, null
from backend import db
from backend import minio_obj
from backend.models.faceBase import face
from backend.models.storeBase import store
from sqlalchemy import func
import cv2
import numpy as np
import json
import datetime
from io import BytesIO
import xlwt,xlrd
import uuid
import base64
# from qcloud_cos import CosS3Client
# from backend.pictureStorage.config import config
import os,uuid,shutil
import urllib.request
import cv2
from backend.settings import ip_port
#from ftplib import FTP
import uuid


face_bp = Blueprint('face', __name__)

image_ip = ip_port()

path = 'backend/data/images'

''' ftp登录、上传、下载
def ftpconnect(host,port,username,password):
    ftp = FTP()
    ftp.set_pasv(False)
    ftp.connect(host,port)
    ftp.login(username,password)
    return ftp

 
def downloadfile(ftp, remotepath, localpath):  #remotepath：上传服务器路径；localpath：本地路径；
    bufsize = 1024                #设置缓冲块大小
    fp = open(localpath,'wb')     #以写模式在本地打开文件
    ftp.retrbinary('RETR ' + remotepath, fp.write, bufsize) #接收服务器上文件并写入本地文件
    ftp.set_debuglevel(0)         #关闭调试
    fp.close()                    #关闭文件

def uploadfile(ftp, remotepath, localpath):
    bufsize = 1024
    fp = open(localpath, 'rb')
    ftp.storbinary('STOR ' + remotepath, fp, bufsize)    #上传文件
    ftp.set_debuglevel(0)
    fp.close() 
'''
"""截取保存人脸图片，根据frame_id生成url并返回"""
def face_box(image):
    data = np.fromstring(image, dtype=np.uint8)
    img = cv2.imdecode(data, 1)
    client = CosS3Client(config)
    face_photo = img
    uid = uuid.uuid4()
    image_url = "https://fish-1305575458.cos.ap-beijing.myqcloud.com/" + str(uid) + ".jpg"
    try:
        f = cv2.imencode('.jpg', face_photo)[1].tobytes()
    except Exception as e:
        print(e)
    response = client.put_object(
        Bucket='fish-1305575458',
        Body=f,
        Key=str(uid) + '.jpg',
        EnableMD5=False
    )
    return image_url

'''获取数据'''
@face_bp.route('/api/faceLib', methods=['GET'])
def faceLib():
    '''初始页面'''
    r_json = face.init_faceLib()
    for info in r_json['data']:
        info['photo'] = ip_port()+"/edge/" + str(info['photo'])
    return jsonify(r_json)

'''添加'''
@face_bp.route('/api/add_face',methods=['POST'])
def add_face():
    data_dict = json.loads(str(request.get_data(),'utf-8'))
    strs=data_dict['photo']
    imgdata=base64.b64decode(strs[22:])
    key = db.session.query(func.max(face.key)).first()[0];#查询`key`最大值
    if key is None:
        key=1
    else:
        key=key+1
    name = data_dict["name"]


    file=open('backend/data/images/{}_{}.jpg'.format(key,name),'wb')
    file.write(imgdata)
    file.close()
    minio_obj.upload_file("edge", 'images/{}_{}.jpg'.format(key,name), 'backend/data/images/{}_{}.jpg'.format(key,name), ".jpg")

    image_url = 'edge/images/{}_{}.jpg'.format(key,name)
    age = data_dict['age']
    gender = data_dict['gender']
    photo = image_url
    id = data_dict['id']
    part = data_dict['part']
    phone = data_dict['phone']
    label = data_dict['label'] if "label" in data_dict else ""
    remark = data_dict['remark'] if "remark" in data_dict else ""
    face_info = face(
                     key=key,
                     name=name,
                     age=age,
                     gender=gender,
                     photo=photo,
                     id=id,
                     part=part,
                     phone=phone,
                     label = label,
                     time=datetime.datetime.now(),
                     remark=remark)
    if face_info is None:
        return jsonify({'status':'fail'})
    else:
        db.session.add(face_info)
        db.session.commit()
        return jsonify({'status':'success'})

'''修改'''
@face_bp.route('/api/update_face',methods=['POST'])
def update_face():
    data_dict = json.loads(str(request.get_data(),'utf-8'))
    strs=data_dict['photo']
    key = data_dict['key']
    name = data_dict["name"]
    #这啥呀？？
    if strs[-3:] == "jpg":
        image_url=db.session.query(face).filter(face.key==key).first().photo
        print(image_url)
    else:
        imgdata=base64.b64decode(strs[22:])
        # 解决前端缓存问题
        image_path = 'backend/data/images'
        files = os.listdir(image_path)
        for i, f in enumerate(files):
            if f.find('{}_'.format(key))>=0:
                os.remove(image_path+'/'+f)
                minio_obj.remove_file("edge", 'images/'+f)
        uid = uuid.uuid1()
        filename = '{}_{}.jpg'.format(key,uid)
        file=open('{}/{}'.format(path,filename),'wb')
        file.write(imgdata)
        file.close()
        image_url = 'edge/images/{}'.format(filename)
        minio_obj.upload_file("edge", 'images/'+filename, 'backend/data/images/{}_{}.jpg'.format(key, uid),
                              ".jpg")
        #修改人脸库中图片,还需要所有的特征文件重新生成
        store_path = 'backend/data/store'
        for j,f_path in enumerate(os.listdir(store_path)):
            for k,f in enumerate(os.listdir(store_path+'/'+f_path)): 
                if f.find('{}_'.format(key))>=0:
                    os.remove(store_path+'/'+f_path+'/'+f)
                    shutil.copyfile(path+'/'+filename,store_path + '/' + f_path+'/'+filename)
    age = data_dict['age']
    gender = data_dict['gender']
    photo = image_url
    id = data_dict['id']
    part = data_dict['part']
    phone = data_dict['phone']
    label = data_dict['label'] if "label" in data_dict else ""
    remark = data_dict['remark'] if "remark" in data_dict else ""
    face_info = face(key=key)

    if face_info is None:
        return jsonify({'status':'fail'})
    else:
        db.session.query(face).filter(face.key==key).update({"name":name,
                                            "age":age,
                                            "gender":gender,
                                            "photo":photo,
                                            "id":id,
                                            "part":part,
                                            "phone":phone,
                                            "label":label,
                                            "remark":remark})
        db.session.commit()
        return jsonify({'status':'success'})

'''删除'''
#是不是忘了修改库里的key串了
@face_bp.route('/api/del_face',methods=['POST'])
def del_face():
    data = json.loads(str(request.get_data(),'utf-8'))
    key = data['key']
    face_info = db.session.query(face).filter(face.key==key).first()
    image_name = face_info.photo[7:]
    print(image_name)
    store_path = 'backend/data/store'
    images_path = 'backend/data/images'
    print(images_path + '/' + image_name)
    if os.path.exists(images_path+'/'+image_name):
        print(images_path + '/'  + image_name)
        os.remove(images_path+'/'+image_name)
        minio_obj.remove_file("edge", 'images/'+image_name)
    for j,f_path in enumerate(os.listdir(store_path)):
        print(store_path+'/'+f_path+'/'+image_name)
        if os.path.exists(store_path+'/'+f_path+'/'+image_name):
            os.remove(store_path+'/'+f_path+'/'+image_name)
    if face_info is None:
        return jsonify({'status':'fail'})
    else: 
        db.session.delete(face_info)
        db.session.commit()
        return jsonify({'status':'success'})

'''批量删除'''
@face_bp.route('/api/muldel_face',methods=['POST'])
def muldel():
    data = json.loads(str(request.get_data(),'utf-8'))
    for key in data['key']:
        face_info = db.session.query(face).filter(face.key==key).first()
        image_name = face_info.photo[7:]
        print(image_name)
        store_path = 'backend/data/store'
        images_path = 'backend/data/images'
        if os.path.exists(images_path + '/' + image_name):
            os.remove(images_path + '/' + image_name)
            minio_obj.remove_file("edge", 'images/'+image_name)
        for j,f_path in enumerate(os.listdir(store_path)):
            print(store_path+'/'+f_path+'/'+image_name)
            if os.path.exists(store_path+'/'+f_path+'/'+image_name):
                os.remove(store_path+'/'+f_path+'/'+image_name)
        #face_info = face().query.filter_by(key=key).first()
        if face_info is None:
            return jsonify({'status':'fail'})
        else:
            db.session.delete(face_info)
            db.session.commit()
    return jsonify({'status':'success'})

'''过滤条件查询'''
@face_bp.route('/api/query_face',methods=['POST'])
def query_face():
    data = json.loads(str(request.get_data(),'utf-8'))
    name = data['name'] if "name" in data else ""
    part = data['part'] if "part" in data else ""
    gender = data['gender'] if "gender" in data else ""
    label = data['label'] if "label" in data else ""
    minage = data['minage'] if "minage" in data else 1
    maxage = data['maxage'] if "maxage" in data else db.session.query(func.max(face.age)).first()[0]
    if name is "":
        if part is "":
            if gender is "":
                if label is "":
                    query_name = db.session.query(face).filter(face.age>=minage,face.age<=maxage).all()
                else:
                    query_name = db.session.query(face).filter(face.age>=minage,face.age<=maxage,face.label==label).all()
            else:
                if label is "":
                    query_name = db.session.query(face).filter(face.age>=minage,face.age<=maxage,face.gender==gender).all()
                else:
                    query_name = db.session.query(face).filter(face.age>=minage,face.age<=maxage,face.gender==gender,face.label==label).all()
        #query_name = face().query.filter_by(name=name,part=part).all()
        else:
            if gender is "":
                if label is "":
                    query_name = db.session.query(face).filter(face.age>=minage,face.age<=maxage,face.part==part).all()
                else:
                    query_name = db.session.query(face).filter(face.age>=minage,face.age<=maxage,face.label==label,face.part==part).all()
            else:
                if label is "":
                    query_name = db.session.query(face).filter(face.age>=minage,face.age<=maxage,face.gender==gender,face.part==part).all()
                else:
                    query_name = db.session.query(face).filter(face.age>=minage,face.age<=maxage,face.gender==gender,face.label==label,face.part==part).all()
    else:
        if part is "":
            if gender is "":
                if label is "":
                    query_name = db.session.query(face).filter(face.age>=minage,face.age<=maxage,face.name==name).all()
                else:
                    query_name = db.session.query(face).filter(face.age>=minage,face.age<=maxage,face.label==label,face.name==name).all()
            else:
                if label is "":
                    query_name = db.session.query(face).filter(face.age>=minage,face.age<=maxage,face.gender==gender,face.name==name).all()
                else:
                    query_name = db.session.query(face).filter(face.age>=minage,face.age<=maxage,face.gender==gender,face.label==label,face.name==name).all()
        #query_name = face().query.filter_by(name=name,part=part).all()
        else:
            if gender is "":
                if label is "":
                    query_name = db.session.query(face).filter(face.age>=minage,face.age<=maxage,face.part==part,face.name==name).all()
                else:
                    query_name = db.session.query(face).filter(face.age>=minage,face.age<=maxage,face.label==label,face.part==part,face.name==name).all()
            else:
                if label is "":
                    query_name = db.session.query(face).filter(face.age>=minage,face.age<=maxage,face.gender==gender,face.part==part,face.name==name).all()
                else:
                    query_name = db.session.query(face).filter(face.age>=minage,face.age<=maxage,face.gender==gender,face.label==label,face.part==part,face.name==name).all()
    r_json = {}
    r_json['data']=[]
    for name in query_name:
        r_json['data'].append(face.return_json(name))
    return jsonify(r_json)

'''导出为excel'''
@face_bp.route('/api/export_face',methods=['GET'])
def export_face():
    datas = db.session.query(face.name,face.age,face.gender,face.photo,face.id,face.part,face.phone,face.remark,face.time).all()
    #datastyle = xlwt.XFStyle()
    #datastyle.num_format_str = 'yyyy-mm-dd hh:mm:ss'
    fields = ['姓名','年龄','性别','照片','证件号','单位','手机号','备注']
    book = xlwt.Workbook(encoding='utf-8')
    sheet = book.add_sheet('用户信息')
    for col,field in enumerate(fields):
        sheet.write(0,col,field)
    row = 1
    for data in datas:
        for col,field in enumerate(data[0:-1]):
            sheet.write(row,col,field)
        row += 1
    sio = BytesIO()
    book.save(sio)
    sio.seek(0)
    response = make_response(sio.getvalue())
    response.headers['Content-type'] = 'application/vnd.ms-excel;charset=UTF-8'
    response.headers['Content-Disposition'] = 'attachment; filename=persons.xls'
    response.headers['Access-Control-Allow-Origin'] = 'Content-Disposition'
    response.headers['Access-Control-Allow-Method'] = '*'
    response.headers['Access-Control-Allow-Headers'] = '*'
    return response

'''导入 图片数据呢？'''
@face_bp.route('/api/import_face',methods=['POST'])
def import_face():
    file = request.files.get('file')
    f = file.read()
    data = xlrd.open_workbook(file_contents=f)
    table = data.sheets()[0]
    nrows = table.nrows
    key = db.session.query(func.max(face.key)).first()[0];#查询`key`最大值
    for i in range(1,nrows):
        row_date = table.row_values(i)
        n = i -1
        face_info = face(
                     key=key+i,
                     name=row_date[0],
                     age=row_date[1],
                     gender=row_date[2],
                     photo=row_date[3],
                     id=row_date[4],
                     part=row_date[5],
                     phone=row_date[6],
                     time=datetime.datetime.now(),
                     remark=row_date[7])
        if face_info is None:
            return jsonify({'status':'fail'})
        else:
            db.session.add(face_info)
            db.session.commit()
    return jsonify({'status':'success'})
    #bk = xlrd.open_workbook(excelName,encoding_override='utf-8')
    #sh = bk.sheets()[0]
    #print(sh)

'''从人员管理中选择人员添加到人脸库中'''
@face_bp.route('/api/append_to_store', methods=['POST'])
def append_to_store():
    data = json.loads(str(request.get_data(),'utf-8'))
    key_face = data['key_face'].split(',')
    key_store = data['key_store'].split(',')
    for i in key_store:
        #face_id = store.query.filter
        key_face_copy = key_face
        key_face_update = ""
        people = db.session.query(store.face).filter(store.key==i).first()
        for f in people:
            flag = f
        if flag is None:
            key_face_update = ",".join(key_face_copy)
        else:
            for j in people:
                j_arr = j.split(',')
            for k in j_arr:
                if k not in key_face_copy:
                    key_face_copy.append(k)
            key_face_update = ","+",".join(key_face_copy)

        store_ = db.session.query(store).filter(store.key==i).first()

        #将图片复制到相应的人脸库中
        for face_id in key_face_copy:
            if face_id:
                people = db.session.query(face).filter(face.key==int(face_id)).first()
                image_name = people.photo[7:]
                for c, f in enumerate(os.listdir("backend/data/store")):
                    if f.find('{}_'.format(store_.key))>=0:
                        store_name = f
                filename = "backend/data/store/{}/{}".format(store_name,image_name)
                print(filename)
                shutil.copyfile('backend/data/images/{}'.format(image_name),filename)

        #人员的id写到人脸库中
        db.session.query(store).filter(store.key==i).update({"face":key_face_update})
        db.session.commit()
    return jsonify({'status':'success'})


#------------------------------------对接------------------------------#

'''添加'''
@face_bp.route('/api/ipc/face/add',methods=['POST'])
@jwt_required(optional=False)
def add_face1():
    data_dict = json.loads(str(request.get_data(),'utf-8'))
    strs=data_dict['face_img']
    imgdata=base64.b64decode(strs)
    key = db.session.query(func.max(face.key)).first()[0];#查询`key`最大值
    if key is None:
        key=1
    else:
        key=key+1
    # uid = uuid.uuid1()
    pid = data_dict['pid']
    file=open('backend/data/images/{}.jpg'.format(pid),'wb')
    file.write(imgdata)
    file.close()
    minio_obj.upload_file("edge", 'images/{}.jpg'.format(pid), 'backend/data/images/{}.jpg'.format(pid),".jpg")

    image_url = 'images/{}.jpg'.format(pid)
    # age = data_dict['age']
    name = data_dict["person_name"]
    age = data_dict['age'] if 'age' in data_dict else 0
    gender = data_dict['sex'] if 'sex' in data_dict else '未知'
    photo = image_url
    id = data_dict['certificate_number']

    # part = data_dict['part']
    part=''
    phone = data_dict['phone'] if 'phone' in data_dict else '未知'
    remark = data_dict['certificate_type']
    label = data_dict['remark'] if "remark" in data_dict else 'nvr'
    face_info = face(
                     key=key,
                     name=name,
                     age=age,
                     gender=gender,
                     photo=photo,
                     id=id,
                     part=part,
                     phone=phone,
                     label = label,
                     time=datetime.datetime.now(),
                     remark=remark)



    #用facelib_name查询人脸库中的人脸id串
    #复制图片
    #将新的key加入人脸id串中

    facelib_name = data_dict['facelib_name']
    facelib = db.session.query(store).filter(store.name == facelib_name).first()
    str_key = facelib.face
    str_key = "{}{},".format(str_key,key)
    # people = db.session.query(face).filter(face.key == key).first()
    print(face_info.photo)
    image_name = face_info.photo[7:]
    for c, f in enumerate(os.listdir("backend/data/store")):
        if f.find('{}_'.format(facelib.key)) >= 0:
            store_name = f
    filename = "backend/data/store/{}/{}".format(store_name, image_name)
    print(filename)
    shutil.copyfile('backend/data/images/{}'.format(image_name), filename)

    #确定操作都正确后，再加入人脸和人脸库中去
    if face_info is None:
        return jsonify({"code":1,"message":"error"})
    else:
        db.session.add(face_info)
        db.session.commit()

    db.session.query(store).filter(store.name == facelib_name).update({"face": str_key})
    db.session.commit()
    # pid = image_name[0:image_name.rfind('.')]
    return jsonify({"code":0,"message":"ok","pid":pid})


'''删除'''
@face_bp.route('/api/ipc/face/delete',methods=['POST'])
@jwt_required(optional=False)
def delete_face():
    data = json.loads(str(request.get_data(),'utf-8'))
    pid = data['pid']
    facelib_name = data['facelib_name']
    photo = "images/" + pid + ".jpg"
    face_info = db.session.query(face).filter(face.photo==photo).first()
    key = face_info.key
    image_name = face_info.photo[7:]
    print(image_name)
    store_path = 'backend/data/store'
    if os.path.exists(path+'/'+image_name):
        os.remove(path+'/'+image_name)
        minio_obj.remove_file("edge", 'images/'+image_name)
    for j,f_path in enumerate(os.listdir(store_path)):
        print(store_path+'/'+f_path+'/'+image_name)
        if os.path.exists(store_path+'/'+f_path+'/'+image_name):
            os.remove(store_path+'/'+f_path+'/'+image_name)
    if face_info is None:
        return jsonify({"code": 1, "message": "error"})
    else:
        db.session.delete(face_info)
        db.session.commit()
        # return jsonify({'status':'success'})

    #修改key串
    facelib = db.session.query(store).filter(store.name == facelib_name).first()
    str_key = str(facelib.face)
    key_store = str_key.split(',')
    key_store.remove(str(key))
    key_face_update = ",".join(key_store)
    db.session.query(store).filter(store.name == facelib_name).update({"face": key_face_update})
    db.session.commit()
    return jsonify({"code": 0, "message": "ok"})

'''修改'''
@face_bp.route('/api/ipc/face/modify',methods=['POST'])
@jwt_required(optional=False)
def modify_face():
    data_dict = json.loads(str(request.get_data(),'utf-8'))
    # strs=data_dict['face_img']
    # key = data_dict['key']
    person_name = data_dict['person_name']
    facelib_name = data_dict['facelib_name']
    pid = data_dict['pid']
    photo = 'images/{}.jpg'.format(pid)
    face_info = db.session.query(face).filter(face.photo == photo).first()
    key = face_info.key
    # if strs[-3:] == "jpg":
    #     image_url=db.session.query(face).filter(face.key==key).first().photo
    #     print(image_url)
    # else:
    #     imgdata=base64.b64decode(strs[22:])
    #     # 解决前端缓存问题
    #     files = os.listdir(path)
    #     for i, f in enumerate(files):
    #         if f.find('{}_'.format(key))>=0:
    #             os.remove(path+'/'+f)
    #     uid = uuid.uuid1()
    #     filename = '{}_{}.jpg'.format(key,uid)
    #     file=open('{}/{}'.format(path,filename),'wb')
    #     file.write(imgdata)
    #     file.close()
    #     image_url = 'images/{}'.format(filename)
    #     #修改人脸库中图片
    #     store_path = 'backend/data/store'
    #     for j,f_path in enumerate(os.listdir(store_path)):
    #         for k,f in enumerate(os.listdir(store_path+'/'+f_path)):
    #             if f.find('{}_'.format(key))>=0:
    #                 os.remove(store_path+'/'+f_path+'/'+f)
    #                 shutil.copyfile(path+'/'+filename,store_path + '/' + f_path+'/'+filename)
    age = 0
    gender = data_dict['sex']
    # photo = image_url
    id = data_dict['certificate_number']
    # part = data_dict['part']
    part = ''
    phone = data_dict['phone']
    remark = data_dict['certificate_type']
    label = data_dict['remark'] if "remark" in data_dict else 'nvr'
    face_info = face(key=key)

    if face_info is None:
        return jsonify({"code": 1, "message": "error"})
    else:
        db.session.query(face).filter(face.key==key).update({
                                            "name":person_name,
                                            "age":age,
                                            "gender":gender,
                                            "photo":photo,
                                            "id":id,
                                            "part":part,
                                            "phone":phone,
                                            "label":label,
                                            "remark":remark})
        db.session.commit()
        return jsonify({"code": 0, "message": "ok"})

@face_bp.route('/api/ipc/face/get', methods=['GET'])
@jwt_required(optional=False)
def get_face1():
    name = request.args.get('name')
    facelib = db.session.query(store).filter(store.name == name).first()
    str_key = str(facelib.face)
    key_store = str_key.split(',')
    r_json = {}
    r_json['code'] = 0
    r_json['message'] = "ok"
    r_json['data'] = []
    n = len(key_store)
    for key in key_store[1:n-1]:
        face_info = db.session.query(face).filter(face.key == key).first()
        info = {}
        pid = face_info.photo[7:face_info.photo.rfind('.')]
        info['pid'] = pid
        info['person_name'] = face_info.name
        info['sex'] = face_info.gender
        info['certificate_type'] = face_info.remark
        info['certificate_number"'] = face_info.id
        info['phone'] = face_info.photo
        # ip = '10.112.197.219'
        # port = 9000
        info['face_url'] = '{}/{}/{}'.format(image_ip,"edge",face_info.photo)
        r_json['data'].append(info)
    return jsonify(r_json)