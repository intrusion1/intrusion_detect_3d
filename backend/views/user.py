# coding=utf-8
from flask import Blueprint, jsonify, request, Response, session
from sqlalchemy.sql.operators import notendswith_op
from backend import db,jwt
from backend.models.userBase import User
import json
from sqlalchemy import func
import datetime
from flask_jwt_extended import (
    JWTManager, jwt_required, create_access_token,
    get_jwt_identity
)

user_bp = Blueprint('User', __name__)


@user_bp.route('/api/user', methods=['GET'])
def user():
    '''初始页面'''
    r_json = User.init_user()
    return jsonify(r_json)



@user_bp.route('/api/login', methods=['POST'])
def login():
    data = json.loads(request.get_data())
    username = data['name'] if "name" in data else ""
    password = data['password'] if "password" in data else ""
    if username == "" or password == "":
        return jsonify({"code": 1,"messsage": "username or password is null", "token": ""})
    user_infos = User().query.filter_by(username=username, password=password).all()
    if len(user_infos)==0:
        return jsonify({"code": 1, "messsage": "Wrong user name or password", "token": ""})
    access_token = create_access_token(identity=username)
    return jsonify({"code": 1, "messsage": "Wrong user name or password", "token": access_token})

@user_bp.route('/api/user', methods=['POST'])
def get_user():
    """接受查询条件，对条件进行筛选，返回json格式信息"""
    data = json.loads(str(request.get_data(), 'utf-8'))
    print(data,'\n\n')
    r_json = User.query_user(data)
    return jsonify(r_json)

'''添加用户'''
@user_bp.route('/api/add_user', methods=['POST'])
def add_user():
    data = json.loads(str(request.get_data(),'utf-8'))
    key = db.session.query(func.max(User.key)).first()[0];#查询`key`最大值
    username = data['username']
    password = data['password']
    role = data['role']
    if "notes" in data:
        notes = data['notes']
    else:
        notes=''
    user_info = User(
                     key=key+1,
                     username=username,
                     password=password,
                     role=role,
                     time=datetime.datetime.now(),
                     others=notes)
    if user_info is None:
        return jsonify({'status':'fail'})
    else:
        db.session.add(user_info)
        db.session.commit()
        return jsonify({'status':'success'})
    
'''删除'''
@user_bp.route('/api/del_user', methods=['POST'])
def del_user():
    """删除特定用户"""
    data = json.loads(str(request.get_data(),'utf-8'))
    print(data)
    username = data['username']
    user_info = User().query.filter_by(username=username).first()
    print(user_info)
    if user_info is None or user_info.username == 'admin':
        return jsonify({'status': 'fail'})
    else:
        db.session.delete(user_info)
        db.session.commit()
        return jsonify({'status': 'success'})

'''查询'''

@user_bp.route('/api/query_user',methods=['POST'])
def query_user():
    data = json.loads(str(request.get_data(),'utf-8'))
    username = data['username']
    if username is None:
        return jsonify({'status':'fail'})
    elif username is "":
        r_json = User.init_user()
        return jsonify(r_json)
    else:
        query_username = User().query.filter_by(username=username).all()
        r_json = {}
        r_json['data']=[]
        for username in query_username:
            r_json['data'].append(User.return_json(username))
        return jsonify(r_json)
# before_request作用的路由
my_api = ['/api/user_login', '/api/user_logout', '/api/add_user', '/api/change_pwd',
          '/api/edit_user', '/api/del_user']

'''修改'''
@user_bp.route('/api/update_user',methods=['POST'])
def update_user():
    data = json.loads(str(request.get_data(),'utf-8'))
    key = data['key']
    username = data['username']
    password = data['password']
    role = data['role']
    if "notes" in data:
        notes = data['notes']
    else:
        notes=''
    face_info = User(key=key)
    if face_info is None:
        return jsonify({'status':'fail'})
    else:
        db.session.query(User).filter(User.key==key).update({"username":username,
                                            "password":password,
                                            "role":role,
                                            "others":notes})
        db.session.commit()
        return jsonify({'status':'success'})
        
'''批量删除'''
@user_bp.route('/api/muldel_user',methods=['POST'])
def muldel_user():
    data = json.loads(str(request.get_data(),'utf-8'))
    for key in data['key']:
        user_info = User().query.filter_by(key=key).first()
        if user_info is None:
            return jsonify({'status':'fail'})
        else:
            db.session.delete(user_info)
            db.session.commit()
    return jsonify({'status':'success'})

'''简易 登陆/权限验证'''
'''
@user_bp.before_request
def is_login():
    if request.path in my_api:
        res = session.get('role')
        request_path = request.path
        print(request_path)
        if request_path in ['/api/user_login', '/api/user_logout']:  # login,logout直接放行
            return None
        elif not res:
            return '未登陆'
        elif res == '0':
            return '无权限'
    return None
'''

'''设置cookie，登陆用'''
@user_bp.route('/api/user_login', methods=['POST'])
def user_login():
    data = json.loads(str(request.get_data(), 'utf-8'))
    username = data['username']
    password = data['password']
    this_user = User().query.filter_by(username=username).first()
    if this_user is None:
        result = json.dumps({'status': 'usererror'})
        resp = Response(result, content_type='application/json')
    elif this_user and this_user.role and this_user.password == password:
        result = json.dumps({'status': 'success', 'username': username, 'role': this_user.role})  # 返回登录信息
        resp = Response(result, content_type='application/json')
        resp.set_cookie('role', str(this_user.role))
        resp.set_cookie('username', str(username))
        session['role'] = this_user.role
    else:
        result = json.dumps({'status': 'fail'})
        resp = Response(result, content_type='application/json')
    return resp


'''删除cookie，注销用'''
@user_bp.route('/api/user_logout',methods=['POST'])
def user_logout():
    resp = Response(json.dumps({"status": "success"}), content_type='application/json')
    resp.delete_cookie('role')
    resp.delete_cookie('username')
    session.pop('role')
    return resp

