# coding=utf-8
from flask import Blueprint, jsonify, request, make_response, session
from flask_jwt_extended import jwt_required
from sqlalchemy.sql.expression import false
from backend import db
from backend.models.deviceBase import device
from backend.models.historyIntrusionBase import history
from sqlalchemy import func
import cv2
import numpy as np
import json
import datetime
import time
from backend.utils.date_util import dateTOTime
from io import BytesIO
import xlwt,xlrd
from backend.settings import ip_port

history_intrusion_bp = Blueprint('history_intrusion', __name__)

'''直接获取数据'''
@history_intrusion_bp.route('/api/history_intrusion', methods=['GET'])
def history_intrusion():
    r_json = history.init_history()
    for info in r_json['data']:
        info['photo'] = ip_port()+"/edge" + str(info['photo'])
    return jsonify(r_json)


'''历史记录查询'''
@history_intrusion_bp.route('/api/query_history_intrusion', methods=['POST'])
def query_history_intrusion():
    data = json.loads(str(request.get_data(), 'utf-8'))
    key = data['key'] if "key" in data else ""
    label = data['label'] if "label" in data else ""
    device = data['device'] if "device" in data else ""
    maxtime = data['maxtime'] + " 23:59:59" if "maxtime" in data else datetime.datetime.now().strftime('%Y-%m-%d %X')
    mintime = data['mintime'] + " 00:00:00" if "mintime" in data else "1970-01-01 00:00:00"
    # remark = data['remark'] if "remark" in data else ""
    if key is "":
        if device is "":
            if label is "":
                query_name = db.session.query(history).filter(history.time >= mintime, history.time <= maxtime).all()
            else:
                query_name = db.session.query(history).filter(history.label == label, history.time >= mintime,
                                                                      history.time <= maxtime).all()
        else:
            if label is "":
                query_name = db.session.query(history).filter(history.device == device, history.time >= mintime,
                                                          history.time <= maxtime).all()
            else:
                query_name = db.session.query(history).filter(history.device == device, history.label == label,
                                                              history.time >= mintime, history.time <= maxtime).all()
    else:
        if device is "":
            if label is "":
                query_name = db.session.query(history).filter(history.key == key,
                                                              history.time >= mintime, history.time <= maxtime).all()
            else:
                query_name = db.session.query(history).filter(history.key == key, history.label == label,
                                                              history.time >= mintime, history.time <= maxtime).all()
        else:
            if label is "":
                query_name = db.session.query(history).filter(history.key == key, history.device == device,
                                                              history.time >= mintime, history.time <= maxtime).all()
            else:
                query_name = db.session.query(history).filter(history.key == key,
                                                              history.device == device,
                                                              history.label == label,
                                                              history.time >= mintime, history.time <= maxtime).all()

    r_json = {}
    r_json['data'] = []
    # print(query_name)
    for name in query_name:
        r_json['data'].append(history.return_json(name))
    for info in r_json['data']:
        info['photo'] = ip_port()+"/edge" + str(info['photo'])
    return jsonify(r_json)


@history_intrusion_bp.route('/api/del_intrusion',methods=['POST'])
def del_intrusion():
    print(request.get_data())
    data = json.loads(str(request.get_data(),'utf-8'))
    key = data['key']
    history_info = history().query.filter_by(key=key).first()
    if history_info is None:
        return jsonify({'status':'fail'})
    else:
        db.session.delete(history_info)
        db.session.commit()
        return jsonify({'status':'success'})


@history_intrusion_bp.route('/api/muldel_intrusion',methods=['POST'])
def muldel_intrusion():
    data = json.loads(str(request.get_data(),'utf-8'))
    print(data)
    for key in data['key']:
        history_intrusion_info = history().query.filter_by(key=key).first()
        if history_intrusion_info is None:
            return jsonify({'status':'fail'})
        else:
            db.session.delete(history_intrusion_info)
            db.session.commit()
    return jsonify({'status':'success'})