# encoding:utf-8

import uuid

from flask import Blueprint, jsonify, request, Response, session
from backend import db
from backend.models.intrusionBase import intrusion
import json
import os
from sqlalchemy import func
import datetime
import shutil
from backend.utils.draw_area import pilImgDraw_intrusion
from backend.models.deviceBase import device
from backend.models.realTimeFlowBase import realTimeFlow
from backend import minio_obj
from backend.settings import ip_port, stream_name
import cv2

intrusion_bp = Blueprint('intrusion', __name__)

def area_rebuild(data):
    x1 = data['x1']
    y1 = data['y1']
    x2 = data['x2']
    y2 = data['y2']
    x3 = data['x3']
    y3 = data['y3']
    x4 = data['x4']
    y4 = data['y4']
    area = str(x1) + "," + str(y1) + "," + str(x2) + "," + str(y2) + "," + str(x3) + "," + str(y3) + "," + str(x4) + "," + str(y4)
    return area

@intrusion_bp.route('/api/intrusion', methods=['GET'])
def getIntrusion():
    r_json = intrusion.init_intrusion()
    for info in r_json['data']:
        info['photo_original'] = ip_port() + "/edge/" + str(info['photo_original'])
        info['photo_amended'] = ip_port() + "/edge/" + str(info['photo_amended'])
    print(r_json['data'])
    return r_json


# 添加（添加时需要新建图片以及绘框,并将图片路径插入数据库,并插入traffic表与realtimeflow表）
@intrusion_bp.route('/api/add_intrusion', methods=['POST'])
def add_intrusion():
    print("开始添加参数！")
    data = json.loads(str(request.get_data(), 'utf-8'))
    name = data['name']
    x1 = data['x1']
    y1 = data['y1']
    x2 = data['x2']
    y2 = data['y2']
    x3 = data['x3']
    y3 = data['y3']
    x4 = data['x4']
    y4 = data['y4']
    area = str(x1) + "," + str(y1) + "," + str(x2) + "," + str(y2) + "," + str(x3)+","+str(y3)+","+str(x4)+","+str(y4)
    remark = data['remark'] if 'remark' in data else ''
    intrusion_infos = db.session.query(intrusion).filter(intrusion.name == name).all()
    if len(intrusion_infos) != 0:
        return jsonify({'status': 'fail'})
    device_infos = db.session.query(device).filter(device.name == name).all()
    if len(device_infos) == 0:
        return jsonify({'status': 'fail'})
    device_info = device_infos[0]
    key = device_info.key
    uid = uuid.uuid1()
    file_name = "{}_{}.jpg".format(name, uid)

    # 截图
    account = "live"
    # password = device_info.password
    password = device_info.key
    # address = device_info.address
    port = device_info.port
    address = stream_name
    channel = "http://{}:{}/{}/{}.flv".format(address, port, account, password)
    capture = cv2.VideoCapture(channel)
    val, frame = capture.read()
    cv2.imwrite("./backend/data/intrusion/original/{}.jpg".format(name), frame)
    src = "./backend/data/intrusion/original/{}.jpg".format(name)
    dst = "./backend/data/intrusion/original/" + file_name
    print(dst)
    os.rename(src, dst)
    minio_obj.upload_file("edge", 'intrusion/original/' + file_name, dst, ".jpg")

    pilImgDraw_intrusion(file_name, int(x1), int(y1), int(x2), int(y2), int(x3), int(y3), int(x4), int(y4))

    intrusion_info = intrusion(
        key=key,
        name=name,
        area=area,
        remark=remark,
        photo_amended="intrusion/amendment/" + file_name,
        photo_original="intrusion/original/" + file_name,
        time=datetime.datetime.now()
    )
    if intrusion_info is None:
        return jsonify({'status': 'fail'})
    else:
        db.session.add(intrusion_info)
        db.session.commit()
        return jsonify({'status': 'success'})


# 查询
@intrusion_bp.route('/api/query_intrusion', methods=['POST'])
def query_intrusion():
    data = json.loads(str(request.get_data(), 'utf-8'))
    name = data['name'] if "name" in data else ""
    if name is "":
        query_name = db.session.query(intrusion).filter().all()
    else:
        query_name = db.session.query(intrusion).filter(intrusion.name == name).all()
    r_json = {'data': []}
    for name in query_name:
        r_json['data'].append(intrusion.return_json(name))
    for info in r_json['data']:
        info['photo_original'] = "http://" + ip_port() + "/edge/" + str(info['photo_original'])
        info['photo_amended'] = "http://" + ip_port() + "/edge/" + str(info['photo_amended'])
    return jsonify(r_json)


# 删除（需要将图片一并删除）
@intrusion_bp.route('/api/del_intrusion', methods=['POST'])
def del_intrusion():
    data = json.loads(str(request.get_data(), 'utf-8'))
    key = data['key']
    intrusion_info = db.session.query(intrusion).filter(intrusion.key == key).first()
    image_name_amended = intrusion_info.photo_amended if intrusion_info.photo_amended is not None else 'none'
    image_name_original = intrusion_info.photo_original if intrusion_info.photo_original is not None else 'none'
    file_name = "./backend/data/" + image_name_amended
    if os.path.exists(file_name):
        os.remove(file_name)
        minio_obj.remove_file("edge", 'intrusion/amendment/' + image_name_amended)
        print('成功删除文件:', file_name)
    else:
        print('未找到此文件:', file_name)

    file_name = "./backend/data/" + image_name_original
    if os.path.exists(file_name):
        os.remove(file_name)
        print('成功删除文件:', file_name)
        minio_obj.remove_file("edge", 'intrusion/original/' + image_name_original)
    else:
        print('未找到此文件:', file_name)
    if intrusion_info is None:
        return jsonify({'status': 'fail'})
    else:
        db.session.delete(intrusion_info)
        db.session.commit()
        return jsonify({'status': 'success'})


#修改(修改时需要将框图删除，将原图名字更改，再按照坐标将生成框图）
@intrusion_bp.route('/api/update_intrusion',methods=['POST'])
def update_intrusion():
    data = json.loads(str(request.get_data(),'utf-8'))
    key = data['key']
    name = data['name']
    x1 = data['x1']
    y1 = data['y1']
    x2 = data['x2']
    y2 = data['y2']
    x3 = data['x3']
    y3 = data['y3']
    x4 = data['x4']
    y4 = data['y4']
    area = str(x1)+","+str(y1)+","+str(x2)+","+str(y2) + "," + str(x3)+","+str(y3)+","+str(x4)+","+str(y4)
    remark = data['remark']

    intrusion_info = db.session.query(intrusion).filter(intrusion.key == key).first()
    image_name_amended = intrusion_info.photo_amended
    image_name_original = intrusion_info.photo_original

    #删除旧的框图
    file_name = "./backend/data/" + str(image_name_amended)
    if os.path.exists(file_name):
        os.remove(file_name)
        minio_obj.remove_file("edge", 'intrusion/amendment/' + image_name_amended)
        print('成功删除文件:', file_name)
    else:
        print('未找到此文件:', file_name)

    #将原图改名
    uid = uuid.uuid1();
    src = "./backend/data/" + str(image_name_original)
    file_name = "{}_{}.jpg".format(name, uid)
    dst = "./backend/data/intrusion/original/" + file_name
    print(dst)
    os.rename(src, dst)

    #暂时用删除与上传替代修改文件名
    minio_obj.remove_file("edge", 'intrusion/original/' + str(image_name_original))
    minio_obj.upload_file("edge", 'intrusion/original/' + file_name, dst, "image/jpeg")

    #创建新的框图
    pilImgDraw_intrusion(file_name, int(x1), int(y1), int(x2), int(y2), int(x3), int(y3), int(x4), int(y4))

    image_name_original = "intrusion/original/"+file_name
    image_name_amended = "intrusion/amendment/"+file_name
    minio_obj.upload_file("edge", 'intrusion/amendment/' + file_name, "./backend/data/intrusion/amendment/" + file_name, "image/jpeg")
    db.session.query(intrusion).filter(intrusion.key==key).update({"name":name,
        "area":area,"remark":remark,"photo_original":image_name_original,
        "photo_amended":image_name_amended})
    db.session.query(realTimeFlow).filter(realTimeFlow.key==key).update({
        "name": name
    })
    db.session.commit()
    return jsonify({'status':'success'})


@intrusion_bp.route('/api/muldel_intrusion',methods=['POST'])
def muldel_intrusion():
    data = json.loads(str(request.get_data(),'utf-8'))
    for key in data['key']:
        intrusion_info = db.session.query(intrusion).filter(intrusion.key==key).first()
        image_name = intrusion_info.photo[7:]
        print(image_name)
        store_path = 'backend/data/store'
        images_path = 'backend/data/images'
        if os.path.exists(images_path + '/' + image_name):
            os.remove(images_path + '/' + image_name)
            minio_obj.remove_file("edge", 'images/'+image_name)
        for j, f_path in enumerate(os.listdir(store_path)):
            print(store_path+'/'+f_path+'/'+image_name)
            if os.path.exists(store_path+'/'+f_path+'/'+image_name):
                os.remove(store_path+'/'+f_path+'/'+image_name)
        #face_info = face().query.filter_by(key=key).first()
        if intrusion_info is None:
            return jsonify({'status':'fail'})
        else:
            db.session.delete(intrusion_info)
            db.session.commit()
    return jsonify({'status':'success'})


@intrusion_bp.route('/api/screenshot_intrusion',methods=['POST'])
def screenshot_intrusion():
    data = json.loads(str(request.get_data(),'utf-8'))
    name = data['name']
    print(name)
    device_info = db.session.query(device).filter(device.name==name).first()
    intrusion_info = db.session.query(intrusion).filter(intrusion.name == name).first()
    area = intrusion_info.area
    point = area.split(',')
    # account = device_info.account
    account = "live"
    # password = device_info.password
    password = device_info.key
    # address = device_info.address
    port = device_info.port
    address = stream_name
    channel = "http://{}:{}/{}/{}.flv".format(address,port,account,password)
    capture = cv2.VideoCapture(channel)
    val, frame = capture.read()
    cv2.imwrite("./backend/data/intrusion/original/{}.jpg".format(name),frame)
    uid = uuid.uuid1()
    src = "./backend/data/intrusion/original/{}.jpg".format(name)
    file_name = "{}_{}.jpg".format(name, uid)
    dst = "./backend/data/intrusion/original/" + file_name
    print(dst)
    os.rename(src, dst)
    minio_obj.upload_file("edge", 'intrusion/original/'+file_name, dst,".jpg")
    pilImgDraw_intrusion(file_name, int(point[0]), int(point[1]), int(point[2]), int(point[3]))
    image_name_original = "intrusion/original/"+file_name
    image_name_amended = "intrusion/amendment/"+file_name


    image_original = intrusion_info.photo_original
    image_amended = intrusion_info.photo_amended
    # 删除旧图
    filename_original = "./backend/data/" + image_original
    if os.path.exists(filename_original):
        os.remove(filename_original)
        minio_obj.remove_file("edge", 'intrusion/original/'+image_original)
        print('成功删除文件:', filename_original)
    else:
        print('未找到此文件:', filename_original)

    filename_amended = "./backend/data/" + image_amended
    if os.path.exists(filename_amended):
        os.remove(filename_amended)
        minio_obj.remove_file("edge", 'intrusion/amendment/'+image_amended)
        print('成功删除文件:', filename_amended)
    else:
        print('未找到此文件:', filename_amended)
    db.session.query(intrusion).filter(intrusion.name==name).update({"photo_original":image_name_original, "photo_amended":image_name_amended})
    db.session.commit()
    return jsonify({'status':'success'})