from flask import Blueprint, jsonify, request, Response, session
from sqlalchemy.sql.expression import null
from sqlalchemy.sql.operators import notendswith_op
from backend import db
from backend.models.orgBase import Tree
import json
from sqlalchemy import func

org_bp = Blueprint('Tree',__name__)

@org_bp.route('/api/orgTree',methods=['GET'])
def orgnization():
    r_json = Tree.initdata()
    return jsonify(r_json)

'''添加节点'''
@org_bp.route('/api/add_node',methods=['POST'])
def add_node():
    data = json.loads(str(request.get_data(),'utf-8'))
    if "id" not in data:
        parent_id = None
    else:
        parent_id = data['id']
    name = data['name']
    id_children = db.session.query(func.max(Tree.id)).first()[0]
    if id_children is None:
        id_children = 0
    print(id_children)
    org_info = Tree(id=id_children+1,name=name,parent_id=parent_id)
    db.session.add(org_info)
    db.session.commit()
    return jsonify({"status":"success"})

'''添加子节点'''
'''
@org_bp.route('/api/add_children_node',methods=['POST'])
def add_chidren_node():
    data = json.loads(str(request.get_data(),'utf-8'))
    parent_id = data['id']
    name = data['name']
    id_children = db.session.query(func.max(Tree.id)).first()[0]
    org_info = Tree(id=id_children+1,name=name,parent_id=parent_id)
    db.session.add(org_info)
    db.session.commit()
    return jsonify({"status":"success"})
'''
'''删除节点'''
@org_bp.route('/api/del_node',methods=['POST'])
def del_node():
    data = json.loads(str(request.get_data(),'utf-8'))
    id = data['id']
    org_info = db.session.query(Tree).filter(Tree.id==id).first()
    db.session.delete(org_info)
    db.session.commit()
    return({"status":"success"})

'''修改节点'''
@org_bp.route('/api/update_node',methods=['POST'])
def update_node():
    data = json.loads(str(request.get_data(),'utf-8'))
    id = data['id']
    name = data['name']
    db.session.query(Tree).filter(Tree.id==id).update({"name":name})
    db.session.commit()
    return jsonify({"status":"success"})

'''获取子节点数据'''
@org_bp.route('/api/children_node', methods=['POST'])
def children_node():
    data = json.loads(str(request.get_data(),'utf-8'))
    id = data['id']
    children = db.session.query(Tree).filter(Tree.parent_id==id).all()
    r_json = {}
    r_json['data'] = []
    for i in children:
        r_json['data'].append(Tree.return_json(i))
    return jsonify(r_json)