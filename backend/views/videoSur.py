# coding=utf-8
import requests
from numpy.lib.utils import source
from backend.models.faceBase import face
from backend import db
from cv2 import data
import tensorflow as tf
from flask import Blueprint, jsonify, request, Response, session
from backend.scripts.tensorflow_infer import inference
import h5py
from datetime import datetime
import cv2,json
import os

from backend.models.deviceBase import device
from backend.models.intrusionBase import intrusion
from camera_discovery.CameraDiscovery import CameraONVIF
from backend.settings import channel_ip_port, stream_ip, stream_name

video_source = 0
unkown_encodings=[]
face_detect_bp = Blueprint('faceDetect', __name__)
intrusion_channel_bp = Blueprint('IntrusionChannel', __name__)
state=False
state_recognition = False
channel=-1
# intrusionchannel = -1

# '''人脸检测开关'''
# @face_detect_bp.route('/api/switch_faceDection', methods=['POST'])
# def switchDection():
#     data = json.loads(request.get_data())
#     global state
#     if data['value'] == 'open':
#         state = True
#     else:
#         state = False
#     return jsonify({'status':'success'})

'''选择通道'''
@face_detect_bp.route('/api/switch_channel', methods=['POST'])
def switchChannel():
    data = json.loads(request.get_data())
    print("选择通道")
    print(data)
    name = data['name']
    global channel
    if name == '0':
        channel = 0
    else:
        device_info = device().query.filter_by(name=name).first()
        account = device_info.account
        password = device_info.password
        address = device_info.address
        port = device_info.port
        # channel = "http://{}:{}/{}/{}.m3u8".format(address,port,account,password)
        # from_stream = "{}://{}:{}@{}".format("rtsp", account, password, address)
        from_stream = "/home/td/detection.flv"
        # "rtsp://admin:bupt123456@192.168.3.64"
        to_stream = "{}://{}/{}/{}".format("rtmp", stream_ip, "live", device_info.key)
        log_name = "log_ffmpeg{}.out".format(device_info.key)

        url = "http://" + stream_name + ":6666/api/push_stream"
        # url = "http://10.112.197.219:6666/api/push_stream"
        payload = {
            "type": 1,
            "fromStream": from_stream,
            "toStream": to_stream,
            "logName": log_name
        }
        headers = {
            "Content-Type": "application/json; charset=UTF-8",
            "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJmcmVzaCI6ZmFsc2UsImlhdCI6MTY2MTc0MzQxMCwianRpIjoiZGNjMjg0ZTgtODhiNC00MzM4LTlkOTUtZWI3ZjU5NWQzMThhIiwidHlwZSI6ImFjY2VzcyIsInN1YiI6ImFkbWluIiwibmJmIjoxNjYxNzQzNDEwLCJleHAiOjQ4MTUzNDM0MTB9.VUOQwVxzHZXKgBu9aHz0D7NWXf19dcMmMDeIck57Yf8"
        }
        response = requests.post(url, data=json.dumps(payload), headers=headers).text
        print(response)
        channel = "{}/{}/{}/index.m3u8".format(channel_ip_port(), "live", device_info.key)
        print(channel)
    return jsonify({"channel":channel})
        #channel = "rtsp://{}/{}/{}".format(address,account,password)
    #print(channel)
    #cap = cv2.VideoCapture(channel)
    #if cap.isOpened():
    #print(channel)
    #return Response(gen(channel,False), mimetype='multipart/x-mixed-replace; boundary=font_frame')
    #else:
    #    return jsonify({'status':'fail'})

'''视频流画面（按每一帧上传）'''
@face_detect_bp.route('/api/video_feed', methods=['GET'])
def video_feed():
    # print("hhhhhhahahahahahahahahahahh")
    print(channel)
    return ' '
    #return Response(gen(channel,False), mimetype='multipart/x-mixed-replace; boundary=font_frame')
def gen(source,FLAG):
    cap = cv2.VideoCapture(source)
    while True:
        if source != channel:
            source = channel
            cap = cv2.VideoCapture(source)
        return_value, frame = cap.read()
        #print(return_value)
        '''
        if FLAG is True:
            frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
            inference(frame,0.8,0.6,(260,260),True,False)
            img_out = frame[:, :, ::-1]
            font_frame = cv2.imencode('.jpg', img_out)[1].tobytes()
            yield (b'--font_frame\r\n'
                    b'Content-Type: image/jpeg\r\n\r\n' + font_frame +  b'\r\n')
        else:
            font_frame = cv2.imencode('.jpg', frame)[1].tobytes()
            yield(b'--font_frame\r\n'
                                b'Content-Type: image/jpeg\r\n\r\n' + font_frame +  b'\r\n')
                                '''
        font_frame = cv2.imencode('.jpg', frame)[1].tobytes()
        print(font_frame)
        if font_frame:
            yield(b'--font_frame\r\n'
                                b'Content-Type: image/jpeg\r\n\r\n' + font_frame +  b'\r\n')


@intrusion_channel_bp.route('/api/switch_intrusion_channel', methods=['POST'])
def switchIntrusionChannel():
    data = json.loads(request.get_data())
    # print("选择通道")
    # print(data)
    name = data['name']
    global channel
    if name == '0':
        channel = 0
    else:
        device_info = device().query.filter_by(name=name).first()
        account = device_info.account
        password = device_info.password
        address = device_info.address
        port = device_info.port
        # channel = "http://{}:{}/{}/{}.m3u8".format(address,port,account,password)
        # from_stream = "{}://{}:{}@{}".format("rtsp", account, password, address)
        from_stream = "/home/td/detection.flv"
        # "rtsp://admin:bupt123456@192.168.3.64"
        to_stream = "{}://{}/{}/{}".format("rtmp", stream_ip, "live", device_info.key)
        log_name = "log_ffmpeg{}.out".format(device_info.key)

        url = "http://" + stream_name + ":6666/api/push_stream"
        # url = "http://10.112.197.219:6666/api/push_stream"
        payload = {
            "type": 1,
            "fromStream": from_stream,
            "toStream": to_stream,
            "logName": log_name
        }
        headers = {
            "Content-Type": "application/json; charset=UTF-8",
            "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJmcmVzaCI6ZmFsc2UsImlhdCI6MTY2MTc0MzQxMCwianRpIjoiZGNjMjg0ZTgtODhiNC00MzM4LTlkOTUtZWI3ZjU5NWQzMThhIiwidHlwZSI6ImFjY2VzcyIsInN1YiI6ImFkbWluIiwibmJmIjoxNjYxNzQzNDEwLCJleHAiOjQ4MTUzNDM0MTB9.VUOQwVxzHZXKgBu9aHz0D7NWXf19dcMmMDeIck57Yf8"
        }
        response = requests.post(url, data=json.dumps(payload), headers=headers).text
        # print(response)
        channel = "{}/{}/{}/index.m3u8".format(channel_ip_port(), "live", device_info.key)
        print(channel)
    return jsonify({"channel":channel})


@intrusion_channel_bp.route('/api/get_intrusion_area', methods=['POST'])
def getIntrusionArea():
    area_str = ""
    data = json.loads(request.get_data())
    print(data)
    name = data['name']
    intrusion_info= intrusion().query.filter_by(name=name).first()
    area = intrusion_info.area
    areas = area.split(",")
    for i in range(8):
        value = int(areas[i])
        index = value * ((685/225) if (i % 2 == 1) else (1220/400))
        area_str += str(index) + ','
    print("区域是什么！？")
    area = area_str[:-1]
    print(area)
    return jsonify({"area":area})