from enum import Flag
from flask import Blueprint, jsonify, request, Response, session
from flask_jwt_extended import jwt_required

from backend import db,jwt
from backend.models.deviceBase import device
import json
import os
from sqlalchemy import func
import datetime
from backend.settings import stream_ip,stream_name,ws_stream_name
from backend.utils.discernVedio import discernVedio
from backend.models.storeBase import store
from backend.models.realTimeFlowBase import realTimeFlow
from backend.models.trafficBase import traffic
from backend.models.intrusionBase import intrusion
from backend.face_recognition.test.test import face_recognition,load_align
from backend.face_recognition.test.embeddings import load_model
from backend.yolov5_deepsort.count_person import PersonNum
from backend.yolov5_intrusion.temp import intrusion_detect
import requests
from concurrent.futures import ThreadPoolExecutor
from multiprocessing import Process

executor = ThreadPoolExecutor(10)
device_bp = Blueprint('device',__name__)

'''返回设备列表详细信息'''
@device_bp.route('/api/device_list',methods=['GET'])
def device_list():
    r_json = device.init_device()
    return r_json

'''添加设备信息'''
@device_bp.route('/api/add_device',methods=['POST'])
def add_device():
    data = json.loads(str(request.get_data(),'utf-8'))
    key = db.session.query(func.max(device.key)).first()[0];#查询`key`最大值
    name = data['name'] #必填
    area = data['area'] if "area" in data else ""
    #protocol = data['protocol'] #必填
    address = data['address'] #必填
    port = data['port'] #必填
    account = data['account'] #必填
    password = data['password'] #必填
    recognition = "开启" if data['recognition'] is 1 or "开启" else "关闭" #必填
    detection = "开启" if data['detection'] is 1 or "开启" else "关闭" #必填
    store = data['store'] if "store" in data and recognition is 1 else "" #若人脸识别功能开启则必填
    serial = data['serial'] if "serial" in data else "" 
    brand = data['brand'] if "brand" in data else ""
    type = data['type'] if "type" in data else ""
    version = data['version'] if "version" in data else ""
    note=data['note'] if "note" in data else ""
    # plan_name = data['plan_name'] if "plan_name" in data else ""
    device_info = device(
                     key=key+1,
                     name=name,
                     area=area,
                     protocol="rtsp",
                     address=address,
                     port=port,
                     account=account,
                     password=password,
                     serial=serial,
                     brand=brand,
                     type=type,
                     version=version,
                     time=datetime.datetime.now(),
                     note=note,
                     recognition=recognition,
                     detection=detection,
                     store=store,
                     channel_id=0
    )
    # '''多余的判断'''
    # if device_info is None:
    #     return jsonify({'status':'fail'})
    # else:
    db.session.add(device_info)
    db.session.commit()
    # '''推视频'''
    # cmd_str = "nohup ffmpeg -re -stream_loop -1 -i /home/td/detection.flv -vcodec copy -acodec copy -f flv rtmp://10.112.197.219/live/" + str(
    #     device_info.key) + " >log_ffmpeg" + str(device_info.key) + ".out 2>&1 &"
    #
    # '''推摄像头流'''
    # # protocol_from = "rtsp"
    # # protocol_to = "rtmp"
    # # channel_from = "{ }://{}:{}@{}".format(protocol_from, account, password, address)
    # # channel_to = "{}://{}/{}/{}".format(protocol_to, address, "live", device_info.key)
    # # log_name = "log_ffmpeg{}.out".format(device.key)
    # # cmd_str = "nohup ffmpeg -i {} -vcodec copy -acodec copy -f flv {} >{} 2>&1 &".format(channel_from, channel_to,
    # #                                                                                      log_name)
    # print(cmd_str)
    # '''跳转到指定目录执行命令，后续需要改成接口执行'''
    # message_cmd = os.popen("cd /home/td/log_ffmpeg; " + cmd_str)
    # print(message_cmd)
    from_stream = "/home/td/detection.flv"
    # from_stream = "{}://{}:{}@{}".format("rtsp", account, password, address)
    # "rtsp://admin:bupt123456@192.168.3.64"
    to_stream =  "{}://{}/{}/{}".format("rtmp", stream_ip, "live", device_info.key)
    log_name = "log_ffmpeg{}.out".format(device_info.key)

    url = "http://"+stream_name+":6666/api/push_stream"
    #url = "http://10.112.197.219:6666/api/push_stream"
    payload = {
                "type": 1,
                "fromStream": from_stream,
                "toStream": to_stream,
                "logName": log_name
            }
    headers = {
        "Content-Type": "application/json; charset=UTF-8",
        "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJmcmVzaCI6ZmFsc2UsImlhdCI6MTY2MTc0MzQxMCwianRpIjoiZGNjMjg0ZTgtODhiNC00MzM4LTlkOTUtZWI3ZjU5NWQzMThhIiwidHlwZSI6ImFjY2VzcyIsInN1YiI6ImFkbWluIiwibmJmIjoxNjYxNzQzNDEwLCJleHAiOjQ4MTUzNDM0MTB9.VUOQwVxzHZXKgBu9aHz0D7NWXf19dcMmMDeIck57Yf8"
    }
    response = requests.post(url, data=json.dumps(payload), headers=headers).text
    print(response)
    return jsonify({'status': 'success'})
        
'''修改设备信息'''
@device_bp.route('/api/update_device',methods=['POST'])
def update_device():
    data = json.loads(str(request.get_data(),'utf-8'))
    key = data['key']
    name = data['name']
    area = data['area'] if "area" in data else ""
    #protocol = data['protocol']
    address = data['address']
    port = data['port']
    account = data['account']
    password = data['password']
    # print("xxxxxxt")
    print(data['intrusion'])
    recognition = "开启" if data['recognition'] == (1 or True) else "关闭" #必填
    detection = "开启" if data['detection'] == (1 or True) else "关闭" #必填
    intrusion = "开启" if data['intrusion'] == (1 or True) else "关闭"
    store = data['store'] if "store" in data else ""
    serial = data['serial'] if "serial" in data else "" 
    brand = data['brand'] if "brand" in data else ""
    type = data['type'] if "type" in data else ""
    version = data['version'] if "version" in data else ""
    note=data['note'] if "note" in data else ""
    # plan_name = data['plan_name'] if "plan_name" in data else ""
    device_info = device(key=key)
    traffic_info = traffic(key=key)
    realTimeFlow_info = realTimeFlow(key=key)
    if device_info is None:
        return jsonify({'status':'fail'})
    else:
        db.session.query(device).filter(device.key==key).update({"name":name,"address":address,"port":port,
                                                                 "account":account,"password":password,"recognition":recognition,
                                                                 "detection":detection,"intrusion":intrusion,"store":store,"serial":serial,
                                                                 "brand":brand,"type":type,"version":version,
                                                                 "note":note})
        # 同步修改人流量设备信息
        if traffic_info is not None:
            db.session.query(traffic).filter(traffic.key == key).update({
                "name": name
            })
        #同步修改人流量设备统计信息
        if realTimeFlow_info is not None:
            db.session.query(realTimeFlow).filter(realTimeFlow.key == key).update({
                "name": name
            })
        db.session.commit()
        return jsonify({'status':'success'})

'''删除设备信息'''
@device_bp.route('/api/del_device',methods=['POST'])
def del_device():
    data = json.loads(str(request.get_data(),'utf-8'))
    key = data['key']
    device_info = device().query.filter_by(key=key).first()
    traffic_info = traffic().query.filter_by(key=key).first()
    realTimeFlow_info = realTimeFlow().query.filter_by(key=key).first()
    if device_info is None:
        return jsonify({'status':'fail'})
    else:
        if traffic_info is not None:
            db.session.delete(traffic_info)
        if realTimeFlow_info is not None:
            db.session.delete(realTimeFlow_info)
        db.session.delete(device_info)
        db.session.commit()
        to_stream = "{}://{}/{}/{}".format("rtmp", stream_ip, "live", device_info.key)
        url = "http://"+stream_name+":6666/api/kill_stream"
        payload = {
            "toStream": to_stream
        }
        headers = {
            "Content-Type": "application/json; charset=UTF-8",
            "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJmcmVzaCI6ZmFsc2UsImlhdCI6MTY2MTc0MzQxMCwianRpIjoiZGNjMjg0ZTgtODhiNC00MzM4LTlkOTUtZWI3ZjU5NWQzMThhIiwidHlwZSI6ImFjY2VzcyIsInN1YiI6ImFkbWluIiwibmJmIjoxNjYxNzQzNDEwLCJleHAiOjQ4MTUzNDM0MTB9.VUOQwVxzHZXKgBu9aHz0D7NWXf19dcMmMDeIck57Yf8"
        }
        response = requests.post(url, data=json.dumps(payload), headers=headers).text
        print(response)
        return jsonify({'status':'success'})

'''批量删除'''
@device_bp.route('/api/muldel_device',methods=['POST'])
def muldel_device():
    data = json.loads(str(request.get_data(),'utf-8'))
    #循环删除
    for key in data['key']:
        device_info = device().query.filter_by(key=key).first()
        traffic_info = traffic().query.filter_by(key=key).first()
        realTimeFlow_info = realTimeFlow().query.filter_by(key=key).first()
        if device_info is None:
            return jsonify({"status":"fail"})
        else:
            if traffic_info is not None:
                db.session.delete(traffic_info)
            if realTimeFlow_info is not None:
                db.session.delete(realTimeFlow_info)
            db.session.delete(device_info)
            db.session.commit()
            to_stream = "{}://{}/{}/{}".format("rtmp", stream_ip, "live", device_info.key)
            url = "http://"+stream_name+":6666/api/kill_stream"
            payload = {
                "toStream": to_stream
            }
            headers = {
                "Content-Type": "application/json; charset=UTF-8",
                "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJmcmVzaCI6ZmFsc2UsImlhdCI6MTY2MTc0MzQxMCwianRpIjoiZGNjMjg0ZTgtODhiNC00MzM4LTlkOTUtZWI3ZjU5NWQzMThhIiwidHlwZSI6ImFjY2VzcyIsInN1YiI6ImFkbWluIiwibmJmIjoxNjYxNzQzNDEwLCJleHAiOjQ4MTUzNDM0MTB9.VUOQwVxzHZXKgBu9aHz0D7NWXf19dcMmMDeIck57Yf8"
            }
            response = requests.post(url, data=json.dumps(payload), headers=headers).text
            print(response)
    return jsonify({"status":"success"})

'''查询设备信息'''
@device_bp.route('/api/query_device',methods=['POST'])
def query_device():
    data = json.loads(str(request.get_data(),'utf-8'))
    name = data['name']
    if name is None:
        return jsonify({'status':'fail'})
    elif name is "":
        r_json = device.init_device()
        faceRecognition()
        return jsonify(r_json)
    else:
        query_name = device().query.filter(
            device.name.like("%" + name + "%") if name is not None else ""
        ).all()
        r_json = {}
        r_json['data']=[]
        for name in query_name:
            r_json['data'].append(device.return_json(name))
        # faceRecognition()
        return jsonify(r_json)

'''人脸识别功能'''
@device_bp.route('/api/face_recognition', methods=['POST'])
def faceRecognition():
    data = json.loads(request.get_data())
    name = data['name']
    device_info = db.session.query(device).filter(device.name==name).first()
    account = device_info.account
    key = device_info.key
    password = device_info.password
    address = device_info.address
    port = device_info.port
    store_name = device_info.store
    state = device_info.recognition
    note = device_info.note
    channel_id = device_info.channel_id
    protocol = 'http'
    format = 'flv'
    # "http://10.112.197.219:8979/live/{key_num}.flv"
    channel = "{}://{}:{}/{}/{}.{}".format(protocol, stream_name, port, "live", key, format)

    # channel = "{}://{}:{}/{}/{}.{}".format(protocol, address, port, account, password, format)
    path = "backend/data/store/"
    store_key = db.session.query(store).filter(store.name==store_name).first()
    if store_key is None:
        pass
    else:
        for i, f in enumerate(os.listdir(path)):
            if f.find('{}_'.format(store_key.key))>=0:
                store_path = path+f+'/embedding.h5'
        print(store_path)
        if state == "开启":
            executor.submit(face_recognition(channel,store_path,name,store_name,note, key,channel_id))
    return jsonify({"status":"success"})


'''人流量分析功能'''
@device_bp.route('/api/update_num',methods=['POST'])
def update_num():
    data = json.loads(request.get_data())
    name = data['name']
    Flag = True
    device_info = db.session.query(device).filter(device.name==name).first()
    account = device_info.account
    password = device_info.password
    address = device_info.address
    format = "flv"
    port = device_info.port
    state = device_info.detection
    channel = "http://{}:{}/{}/{}.{}".format(stream_name,port,"live",device_info.key,"flv")
    print(channel)
    if state == "关闭":
        return jsonify({"status":"success"})
    if name != '' and channel != '':
        executor.submit(PersonNum(name,channel,device_info.key))
    return jsonify({"status":"success"})

'''平台启动后自动启动人流量检测算法'''
def autoUpdateNum():
    device_infos = db.session.query(device).filter().all();
    for device_info in device_infos:
        name = device_info.name
        port = device_info.port
        state = device_info.detection
        channel = "http://{}:{}/{}/{}.{}".format(stream_name, port, "live", device_info.key,"flv")
        print(channel)
        if state!= "关闭" and name != '' and channel != '':
            PersonNum(name,channel,device_info.key)

'''平台启动后自动启动人脸识别功能'''
def autoFaceRecognition():
    device_infos = db.session.query(device).filter().all();
    for device_info in device_infos:
        name = device_info.name
        key = device_info.key
        port = device_info.port
        store_name = device_info.store
        state = device_info.recognition
        note = device_info.note
        channel_id = device_info.channel_id
        protocol = 'http'
        format = 'flv'
        channel = "{}://{}:{}/{}/{}.{}".format(protocol, stream_name, port, "live", key, format)
        path = "backend/data/store/"
        store_key = db.session.query(store).filter(store.name == store_name).first()
        if store_key is None:
            pass
        else:
            for i, f in enumerate(os.listdir(path)):
                if f.find('{}_'.format(store_key.key)) >= 0:
                    store_path = path + f + '/embedding.h5'
            print(store_path)
            if state == "开启":
                face_recognition(channel, store_path, name, store_name, note, key, channel_id)


# 区域入侵检测功能
@device_bp.route('/api/intrusion_detect', methods=['POST'])
def intrusionDetect():
    data = json.loads(request.get_data())
    name = data['name']
    device_info = db.session.query(device).filter(device.name==name).first()
    intrusion_info = db.session.query(intrusion).filter(intrusion.name == name).first()
    print(device_info)
    key = device_info.key
    if device_info.intrusion == "关闭":
        print("开启算法########")
        db.session.query(device).filter(device.key == key).update({"intrusion": '开启'})
        db.session.commit()
        port = device_info.port
        remark = device_info.note
        area = intrusion_info.area
        protocol = 'http'
        format = 'flv'
        channel = r"{}://{}:{}/{}/{}.{}".format(protocol, stream_name, port, "live", key, format)
        print(channel)
        # intrusiondetect = Process(target=intrusion_detect, args=(channel,area,name, remark), name=f"intrusion-{name}")
        # intrusiondetect.daemon = True
        # intrusiondetect.start()
        executor.submit(intrusion_detect(channel, area, name, remark))
        return jsonify({"status":"success"})
    else:
        db.session.query(device).filter(device.key == key).update({"intrusion": '关闭'})
        db.session.commit()
        return jsonify({"status": "success"})

'''平台启动后自动启动区域入侵检测功能'''
def autoIntrusionDetect():
    device_infos = db.session.query(device).filter().all();
    for device_info in device_infos:
        name = device_info.name
        port = device_info.port
        state = device_info.detection
        remark = device_info.remark
        channel = "http://{}:{}/{}/{}.{}".format(stream_name, port, "live", device_info.key, "flv")
        intrusion_info = db.session.query(intrusion).filter(intrusion.name == name).first()
        area = intrusion_info.area
        if state == '开启' and name != '' and channel != '':
            intrusion_detect(channel, area, name, remark)

#------------------------------------对接------------------------------#

"""查询ipc信息"""
@device_bp.route('/api/ipc/device/get',methods=['GET'])
@jwt_required(optional=False)
def device_get():
    r_json = {}
    r_json['code'] = 0
    r_json['message'] = "ok"
    r_json['data'] = []
    info={}
    info['name'] = 'ipc_bupt'
    info['area'] = ''
    info['ip'] = '10.112.197.219'
    info['port'] = 5000
    info['user'] = 'admin'
    info['password'] = '123456'
    info['online'] = True
    r_json['data'].append(info)
    return jsonify(r_json)

"""修改ipc信息"""


"""返回流地址信息"""
@device_bp.route('/api/ipc/video/play/camera',methods=['POST'])
@jwt_required(optional=False)
def get_flow_address():
    data = json.loads(str(request.get_data(), 'utf-8'))
    channel_id = data['ip_channel']
    address = data['ip']  # 必填
    port = data['port']  # 必填
    account = data['user']  # 必填
    password = data['password']  # 必填
    # protocol = data['protocol'] #必填
    device_info = device().query.filter_by(address=address).first()
    if device_info is None:
        return jsonify({'code': 10001, 'message': '北邮nvr中查无此设备ip', 'videourl':''})
    if device_info.account != account or device_info.password != password:
        return jsonify({'code': 10001, 'message': '摄像头账号或者密码错误', 'videourl': ''})
    # if device_info.port != str(port):
    #     return jsonify({'code': 10001, 'message': '摄像头端口号错误', 'videourl': ''})
    # if device_info.channel_id != channel_id:
    #     return jsonify({'code': 10001, 'message': 'ip_channel错误', 'videourl': ''})


    # url = "http://" + ws_stream_name + ":8011/cms/video/play/camera"
    url = "http://10.112.197.219:8011/cms/video/play/camera"
    payload = {
        "ip": address,
        "ip_channel": 1,
        "port": 8000,
        "user": account,
        "password": password
    }
    headers = {
        "Content-Type": "application/json; charset=UTF-8",
        "Authorization": "Bear eyJ0eXAiOiJKV1QiLCAiYWxnIjoiQUVTIn0=.eyJuYW1lIjoiYWRtaW4ifQ==.ppfijsda1S0glhfNJg+eSVJ2J0xd9z1IzHouscKsfg0kGvCD65nZz+JEACX79vxX8SHir7yzzt50uwDtwnb6dA=="
    }
    response = requests.post(url, data=json.dumps(payload), headers=headers).json()
    print(response)
    # channel = "http://{}:{}/{}/{}.flv".format(stream_name, port, "live", device_info.key) if device_info.name != "0" else 0
    return jsonify({'code': 0, 'message': 'ok', 'videourl': response["videourl"]})

"""增加通道（新增摄像头接入）"""
@device_bp.route('/api/ipc/device/channel/add',methods=['POST'])
@jwt_required(optional=False)
def add_device_channel():
    data = json.loads(str(request.get_data(),'utf-8'))
    key = db.session.query(func.max(device.key)).first()[0];#查询`key`最大值
    channel_id = data['channel_id']
    name = data['name'] #必填
    area = data['area'] if "area" in data else ""
    #protocol = data['protocol'] #必填
    address = data['ip'] #必填
    port = data['port'] #必填
    account = data['user'] #必填
    password = data['password'] #必填
    recognition = "关闭"#"开启" if data['recognition'] is 1 or "开启" else "关闭"
    detection = "关闭"#"开启" if data['detection'] is 1 or "开启" else "关闭"
    store = data['store'] if "store" in data else "无"
    serial = data['serial'] if "serial" in data else ""
    brand = data['brand'] if "brand" in data else ""
    type = data['type'] if "type" in data else ""
    version = data['version'] if "version" in data else ""
    note=data['note'] if "note" in data else "nvr"
    tmp_info = device().query.filter_by(address=address).first()
    if tmp_info is None:
        device_info = device(
            key=key + 1,
            name=name,
            area=area,
            protocol="rtsp",
            address=address,
            port=port,
            account=account,
            password=password,
            serial=serial,
            brand=brand,
            type=type,
            version=version,
            time=datetime.datetime.now(),
            note=note,
            recognition=recognition,
            detection=detection,
            store=store,
            channel_id=channel_id
        )
        db.session.add(device_info)
        db.session.commit()
        from_stream = "{}://{}:{}@{}".format("rtsp", account, password, address)
            # "rtsp://admin:bupt123456@192.168.3.64"
        # from_stream = "/home/td/detection.flv"
        to_stream = "{}://{}/{}/{}".format("rtmp", stream_ip, "live", device_info.key)
        log_name = "log_ffmpeg{}.out".format(device_info.key)
        url = "http://"+stream_name+":6666/api/push_stream"
        payload = {
            "type": 0,
            "fromStream": from_stream,
            "toStream": to_stream,
            "logName": log_name
        }
        headers = {
            "Content-Type": "application/json; charset=UTF-8",
            "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJmcmVzaCI6ZmFsc2UsImlhdCI6MTY2MTc0MzQxMCwianRpIjoiZGNjMjg0ZTgtODhiNC00MzM4LTlkOTUtZWI3ZjU5NWQzMThhIiwidHlwZSI6ImFjY2VzcyIsInN1YiI6ImFkbWluIiwibmJmIjoxNjYxNzQzNDEwLCJleHAiOjQ4MTUzNDM0MTB9.VUOQwVxzHZXKgBu9aHz0D7NWXf19dcMmMDeIck57Yf8"
        }
        response = requests.post(url, data=json.dumps(payload), headers=headers).text
        print(response)
    #如果已经作为人流量摄像头被手动接入，则更新配置信息
    else:
        db.session.query(device).filter(device.key == tmp_info.key).update({"name": name,
            "area": area,
            "protocol": "rtsp",
            "address": address,
            "port": port,
            "account": account,
            "password": password,
            "serial": serial,
            "brand": brand,
            "type": type,
            "note": note,
            "store": store,
            "channel_id": channel_id
            })
        db.session.commit()
    return jsonify({'code': 0, 'message': 'ok'})


'''删除设备信息'''
@device_bp.route('/api/ipc/device/channel/delete',methods=['POST'])
@jwt_required(optional=False)
def delete_device_channel():
    data = json.loads(str(request.get_data(),'utf-8'))
    channel_id = data['channel_id']
    device_info = device().query.filter_by(channel_id=channel_id).first()
    if device_info is None:
        return jsonify({'code':1,"message":"error"})
    if device_info.detection == "开启":
        db.session.query(device).filter(device.key == device_info.key).update({
            "store": "无",
            "channel_id": 0,
            "mixed": "",
            "important": "",
            "stranger": "",
            "recognition": "关闭",
        })
        db.session.commit()
        return jsonify({'code': 0, 'message': 'ok'})
    key = device_info.key
    traffic_info = traffic().query.filter_by(key=key).first()
    if traffic_info is not None:
        db.session.delete(traffic_info)
    realTimeFlow_info = realTimeFlow().query.filter_by(key=key).first()
    if realTimeFlow_info is not None:
        db.session.delete(realTimeFlow_info)
    db.session.delete(device_info)
    db.session.commit()
    to_stream = "{}://{}/{}/{}".format("rtmp", stream_ip, "live", device_info.key)
    url = "http://"+stream_name+":6666/api/kill_stream"
    payload = {
        "toStream": to_stream
    }
    headers = {
        "Content-Type": "application/json; charset=UTF-8",
        "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJmcmVzaCI6ZmFsc2UsImlhdCI6MTY2MTc0MzQxMCwianRpIjoiZGNjMjg0ZTgtODhiNC00MzM4LTlkOTUtZWI3ZjU5NWQzMThhIiwidHlwZSI6ImFjY2VzcyIsInN1YiI6ImFkbWluIiwibmJmIjoxNjYxNzQzNDEwLCJleHAiOjQ4MTUzNDM0MTB9.VUOQwVxzHZXKgBu9aHz0D7NWXf19dcMmMDeIck57Yf8"
    }
    response = requests.post(url, data=json.dumps(payload), headers=headers).text
    print(response)
    return jsonify({'code':0,'message':'ok'})

'''修改设备信息'''
@device_bp.route('/api/ipc/device/channel/modify',methods=['POST'])
@jwt_required(optional=False)
def modify_device_channel():
    data = json.loads(str(request.get_data(),'utf-8'))
    channel_id = data['channel_id']
    name = data['name']
    device_info = device(channel_id=channel_id)
    if device_info is None:
        return jsonify({'code':1,"message":"error"})
    key = device_info.key
    traffic_info = traffic().query.filter_by(key=key).first()
    realTimeFlow_info = realTimeFlow().query.filter_by(key=key).first()
    if traffic_info is not None:
        db.session.query(traffic).filter(traffic.key == key).update({"name": name})
    if realTimeFlow_info is not None:
        db.session.query(realTimeFlow).filter(realTimeFlow.key == key).update({"name": name})
    db.session.query(device).filter(device.channel_id==channel_id).update({"name":name})
    db.session.commit()
    return jsonify({'code':0,'message':'ok'})

'''查询设备信息'''
@device_bp.route('/api/ipc/device/channel/status/get',methods=['GET'])
@jwt_required(optional=False)
def get_device_channel():
    device_infos = device().query.filter_by(note='nvr').all()
    r_json = {}
    r_json['code'] = 0
    r_json['message'] = "ok"
    r_json['data'] = []

    for info in device_infos:
        tmp_info = {}
        tmp_info['channel_id'] = info.channel_id
        tmp_info['ip'] = info.address
        tmp_info['online'] = discernVedio("http://"+stream_name+":8979/live/{}.flv".format(info.key))
        tmp_info['name'] = info.name
        tmp_info['record_plan'] = False
        print(info.recognition)
        tmp_info['important_recognize'] = True if info.important != "" else False
        tmp_info['passerby_recognize'] = True if info.detection is "开启" else False
        tmp_info['stranger_recogniz'] = True if info.stranger != "" else False
        tmp_info['human_recogniz'] = False
        tmp_info['highfrequency_recogniz'] = False
        tmp_info['vehicle_recogniz'] = False
        r_json['data'].append(tmp_info)
    return jsonify(r_json)
#计划的增删改查
'''增加计划'''
@device_bp.route('/api/ipc/recognize/plan/create',methods=['POST'])
@jwt_required(optional=False)
def add_plan():
    data = json.loads(str(request.get_data(),'utf-8'))
    channel_id = data['channel_id']
    name = data['name']
    type = data['plan_type']
    facelib_name_list = data['facelib_name_list']  if "facelib_name_list" in data else "无" #暂时只有单个库

    #还需要判断这个库是否存在

    # name_store = facelib_name_list.split(',')
    schedule_time = data['schedule_time'] #暂时废弃
    # device_info = device(key=key)
    device_infos = db.session.query(device).filter(device.channel_id== channel_id).all()
    if len(device_infos)== 0:
        return jsonify({'code':1,"message":"error"})
    device_info = device_infos[0]
    # if device_info is None or device_info.store is not '' or type is not ('important' or 'stranget' or 'mixed'):
    #     return jsonify({'code':1,"message":"error"})

    if type != 'important' and type != 'stranger' and type != 'mixed':
        return jsonify({'code': 3, "message": type})
    elif type == 'mixed':
        db.session.query(device).filter(device.channel_id == channel_id).update(
            {"mixed": name, "store": facelib_name_list})
        db.session.commit()
        return jsonify({'code': 0, 'message': 'ok'})
    else:
        if type == 'important':
            db.session.query(device).filter(device.channel_id==channel_id).update({"important":name,"store":facelib_name_list,"recognition":"开启"})
            db.session.commit()
        elif type == 'stranger':
            db.session.query(device).filter(device.channel_id == channel_id).update(
                {"stranger": name, "store": facelib_name_list, "recognition": "开启"})
            db.session.commit()

        device_infos = db.session.query(device).filter(device.channel_id == channel_id).all()
        if len(device_infos) == 0:
            return jsonify({'code': 1, "message": "error"})
        device_info = device_infos[0]

        if device_info.stranger!= '' and device_info.important!='':
            return jsonify({'code':0,'message':'ok'})

        # 如果算法开启则不需要开启
        #开启算法
        account = device_info.account
        key = device_info.key
        password = device_info.password
        address = device_info.address
        port = device_info.port
        store_name = device_info.store
        state = device_info.recognition
        note = device_info.note
        # channel = "rtsp://{}:{}@{}".format(account, password, address)
        # channel = "rtsp://82.156.19.17/test"
        protocol = 'http'
        format = 'flv'
        channel = "{}://{}:{}/{}/{}.{}".format(protocol,stream_name,port,"live", key,format)
        print(channel)
        # # channel = "http://10.112.197.219:8000/live/livestream.m3u8"
        path = "backend/data/store/"
        store_key = db.session.query(store).filter(store.name == store_name).first()
        if store_key is None:
            pass
        else:
            for i, f in enumerate(os.listdir(path)):
                if f.find('{}_'.format(store_key.key)) >= 0:
                    store_path = path + f + '/embedding.h5'
            print(store_path)
            if state == "开启":
                executor.submit(face_recognition(channel, store_path, name, store_name, note, key, channel_id))
        return jsonify({'code':0,'message':'ok'})

'''修改计划'''
@device_bp.route('/api/ipc/recognize/plan/modify',methods=['POST'])
@jwt_required(optional=False)
def modify_plan():
    data = json.loads(str(request.get_data(),'utf-8'))
    channel_id = data['channel_id']
    name = data['name']
    type = data['plan_type']
    # facelib_name_list = data['facelib_name_list']
    # name_store = facelib_name_list.split(',')
    schedule_time = data['schedule_time']
    device_infos = db.session.query(device).filter(device.channel_id == channel_id).all()
    if len(device_infos) == 0:
        return jsonify({'code': 1, "message": "error"})
    device_info = device_infos[0]
    if device_info.store == '无':
        return jsonify({'code': 2, "message": device_info.store})
    elif type != 'important' and type != 'stranger' and type != 'mixed':
        return jsonify({'code': 3, "message": type})
    else:
        if type == 'mixed':
            db.session.query(device).filter(device.channel_id == channel_id).update(
                {"mixed": name})
            db.session.commit()
        elif type == 'important':
            db.session.query(device).filter(device.channel_id == channel_id).update(
                {"important": name})
            db.session.commit()
        elif type == 'stranger':
            db.session.query(device).filter(device.channel_id == channel_id).update(
                {"stranger": name})
            db.session.commit()

        return jsonify({'code':0,'message':'ok'})

'''删除计划'''
@device_bp.route('/api/ipc/recognize/plan/delete',methods=['POST'])
@jwt_required(optional=False)
def delete_plan():
    data = json.loads(str(request.get_data(),'utf-8'))
    # key = data['channel_id']
    name = data['name']
    print(name)
    # type = data['plan_type']
    # facelib_name_list = data['facelib_name_list']
    # name_store = facelib_name_list.split(',')
    # schedule_time = data['schedule_time']

    device_infos = db.session.query(device).filter(device.mixed == name).all()
    if len(device_infos) != 0:
        device_info = device_infos[0]
        db.session.query(device).filter(device.mixed == name).update(
            {"store": '无', "recognition": '关闭', "mixed": '', "important": '', "stranger":''})
        db.session.commit()
        return jsonify({'code': 0, 'message': 'ok'})

    device_infos = db.session.query(device).filter(device.important == name).all()
    if len(device_infos) != 0:
        device_info = device_infos[0]
        if device_info.stranger == '':
            db.session.query(device).filter(device.important == name).update(
                {"store": '无', "recognition": '关闭', "important": ''})
            db.session.commit()
        else:
            db.session.query(device).filter(device.important == name).update(
                {"important": ''})
            db.session.commit()
        return jsonify({'code': 0, 'message': 'ok'})

    device_infos = db.session.query(device).filter(device.stranger == name).all()
    if len(device_infos) != 0:
        device_info = device_infos[0]
        if device_info.important == '':
            db.session.query(device).filter(device.stranger == name).update(
                {"store": '无', "recognition": '关闭', "stranger": ''})
            db.session.commit()
        else:
            db.session.query(device).filter(device.stranger == name).update(
                {"stranger": ''})
            db.session.commit()
        return jsonify({'code': 0, 'message': 'ok'})
    return jsonify({'code': 1, "message": "error"})

'''查询计划'''
@device_bp.route('/api/ipc/recognize/plan/get',methods=['GET'])
@jwt_required(optional=False)
def get_plan():
    device_info = db.session.query(device).filter(device.note == "nvr",device.mixed != "").all()

    r_json = {}
    r_json['code'] = 0
    r_json['message'] = "ok"
    r_json['data'] = []

    for info in device_info:
        if info.mixed != '':
            tmp_info = {}
            tmp_info['facelib_name_list'] = info.store
            tmp_info['channel_id'] = info.channel_id
            tmp_info['resource_ip'] = '10.112.197.219'
            tmp_info['camera_name'] = info.name
            tmp_info['schedule_time'] = 1
            tmp_info['plan_type'] = 'mixed'
            tmp_info['name'] = info.mixed
            r_json['data'].append(tmp_info)
        if info.important != '':
            tmp_info = {}
            tmp_info['facelib_name_list'] = info.store
            tmp_info['channel_id'] = info.channel_id
            tmp_info['resource_ip'] = '10.112.197.219'
            tmp_info['camera_name'] = info.name
            tmp_info['schedule_time'] = 1
            tmp_info['plan_type'] = 'important'
            tmp_info['name'] = info.important
            r_json['data'].append(tmp_info)
        if info.stranger != '':
            tmp_info = {}
            tmp_info['facelib_name_list'] = info.store
            tmp_info['channel_id'] = info.channel_id
            tmp_info['resource_ip'] = '10.112.197.219'
            tmp_info['camera_name'] = info.name
            tmp_info['schedule_time'] = 1
            tmp_info['plan_type'] = 'stranger'
            tmp_info['name'] = info.stranger
            r_json['data'].append(tmp_info)
    return jsonify(r_json)