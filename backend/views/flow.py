from flask import Blueprint, jsonify, request, Response, session
from flask_jwt_extended import jwt_required

from backend import db
from backend.models.trafficBase import traffic
import json
import os
from sqlalchemy import func
import datetime
import time
from backend.models.flowBase import flow
from backend.models.deviceBase import device
from backend.utils.date_util import get_weekday,dayToTime,get_month,get_month_week,get_year_week,get_year_month
flow_bp = Blueprint('flow',__name__)

@flow_bp.route('/api/flow',methods=['GET'])
def flow_list():
    r_json = flow.init_flow()
    return r_json

#添加(用于测试）
@flow_bp.route('/api/add_flow', methods=['POST'])
def add_flow():
    data = json.loads(str(request.get_data(),'utf-8'))
    identifier= data['identifier']
    num = data['num']
    date = data['date']
    interval = data['interval']
    key = db.session.query(func.max(flow.key)).first()[0]
    if key is None:
        key = 0
    flow_info = flow(
        key=key+1,
        num=num,
        date=date,
        identifier=identifier,
        interval=interval
    )
    if flow_info is None:
        return jsonify({'status':'fail'})
    else:
        db.session.add(flow_info)
        db.session.commit()
        return jsonify({'status':'success'})

#查询任一时期内每天的人流量
@flow_bp.route('/api/query_period_flow', methods=['POST'])
def query_period_flow():
    data = json.loads(str(request.get_data(), 'utf-8'))
    maxtime = data['maxtime'] if "maxtime" in data else datetime.datetime.now().strftime('%Y-%m-%d')
    mintime = data['mintime'] if "mintime" in data else (datetime.datetime.now() + datetime.timedelta(days=-6)).strftime('%Y-%m-%d')
    query_name = db.session.query(flow).filter(flow.date >= mintime, flow.date <= maxtime, flow.interval==0).all()
    r_json = {}
    r_json['data'] = []
    for name in query_name:
        r_json['data'].append(flow.return_json(name))
    return jsonify(r_json)

#查询任意一天所有时间段
@flow_bp.route('/api/query_anyday_flow', methods=['POST'])
def query_anyday_flow():
    data = json.loads(str(request.get_data(),'utf-8'))
    date = data['date'] if "date" in data else datetime.datetime.now().strftime('%Y-%m-%d')
    query_name = db.session.query(flow).filter(flow.date == date, flow.interval!=0).all()
    r_json = {}
    r_json['data']=[]
    for name in query_name:
        r_json['data'].append(flow.return_json(name))
    return jsonify(r_json)


#删除(用于测试)
@flow_bp.route('/api/del_flow',methods=['POST'])
def del_flow():
    data = json.loads(str(request.get_data(),'utf-8'))
    key = data['key']
    flow_info = flow().query.filter_by(key=key).first()
    if flow_info is None:
        return jsonify({'status':'fail'})
    else:
        db.session.delete(flow_info)
        db.session.commit()
        return jsonify({'status':'success'})

#------------------------------------对接------------------------------#
#按指定日期查询
@flow_bp.route('/api/ipc/recognize/pdc/get', methods=['GET'])
@jwt_required(optional=False)
def query_pdc():
    ip = request.args.get("ip")
    report_type = int(request.args.get("report_type"))
    #默认为3，即进出人数都要返回
    stat_type = int(request.args.get("stat_type"))  if "stat_type" in request.args else 3
    stat_interval = int(request.args.get("stat_interval"))
    end_time = int(request.args.get("end_time")) if "end_time" in request.args else time.mktime(
        datetime.datetime.now().timetuple())
    #默认是该日期零点
    start_time = int(request.args.get("start_time")) if "start_time" in request.args else 0
    maxtime = time.strftime("%Y-%m-%d", time.localtime(end_time))
    mintime = time.strftime("%Y-%m-%d", time.localtime(start_time))
    start_time = dayToTime(mintime)
    print(start_time)
    day = time.localtime()
    print(ip)
    device_info = db.session.query(device).filter(device.address==ip,device.note=='nvr').first()
    print(device_info.key)
    device_key = device_info.key

    r_json = {}
    r_json['code'] = 0
    r_json['message'] = "ok"
    r_json['ip'] = ip
    r_json['name'] = device_info.name
    r_json['data'] = []
    #日报
    if report_type==1:
        query_name = db.session.query(flow).filter(flow.identifier==device_key,flow.date == mintime).order_by(flow.interval).all()
        if stat_interval==1:
            # print(query_name)
            for query_info in query_name:
                info = {}
                info['start_time'] = start_time + (query_info.interval - 1) * 30 * 60
                info['end_time'] = start_time + query_info.interval * 30 * 60 -1
                info['enter_num'] = query_info.up
                info['leave_num'] = query_info.down
                r_json['data'].append(info)
                # print(r_json)
        elif stat_interval==2:
            for query_info in query_name:
                info = {}
                info['start_time'] = start_time +(query_info.interval-1)*30*60
                info['end_time'] = start_time + query_info.interval*30*60-1
                info['enter_num'] = query_info.up
                info['leave_num'] = query_info.down
                r_json['data'].append(info)
        elif stat_interval==3:
            up = 0
            down = 0
            for query_info in query_name:
                if query_info.interval % 2!=0:
                    up = query_info.up
                    down = query_info.down
                else:
                    info = {}
                    info['start_time'] = start_time + (query_info.interval - 2) * 30 * 60
                    info['end_time'] = start_time + query_info.interval * 30 * 60 - 1
                    info['enter_num'] = query_info.up + up
                    info['leave_num'] = query_info.down + down
                    r_json['data'].append(info)
    elif report_type==2:
        if stat_interval==4:
            date_list = get_weekday(mintime)
            for date_tmp in date_list:
                query_name = db.session.query(flow).filter(flow.identifier==device_key,flow.date == date_tmp).all()
                up = 0
                down = 0
                for query_info in query_name:
                    up = up + query_info.up
                    down = down + query_info.down
                sec = dayToTime(date_tmp)
                info = {}
                info['start_time'] = sec
                info['end_time'] = sec + 24*60*60-1
                info['enter_num'] = up
                info['leave_num'] = down
                r_json['data'].append(info)
    elif report_type==3:
        if stat_interval == 4:
            date_list = get_month(mintime)
            for date_tmp in date_list:
                query_name = db.session.query(flow).filter(flow.identifier==device_key,flow.date == date_tmp).all()
                up = 0
                down = 0
                for query_info in query_name:
                    up = up + query_info.up
                    down = down + query_info.down
                sec = dayToTime(date_tmp)
                info = {}
                info['start_time'] = sec
                info['end_time'] = sec + 24 * 60 * 60 - 1
                info['enter_num'] = up
                info['leave_num'] = down
                r_json['data'].append(info)
        if stat_interval == 5:
            week_list = get_month_week(mintime)
            for date_list in week_list:
                up = 0
                down = 0
                info = {}
                info['start_time'] = dayToTime(date_list[0])
                info['end_time'] = dayToTime(date_list[len(date_list)-1])+ 24*60*60-1
                # print(date_list[len(date_list)-1])
                for date_tmp in date_list:
                    query_name = db.session.query(flow).filter(flow.identifier == device_key,
                                                               flow.date == date_tmp).all()
                    for query_info in query_name:
                        up = up + query_info.up
                        down = down + query_info.down

                info['enter_num'] = up
                info['leave_num'] = down
                r_json['data'].append(info)
    elif report_type==4:
        if stat_interval == 5:
            week_list = get_year_week(mintime)
            for date_list in week_list:
                up = 0
                down = 0
                info = {}
                info['start_time'] = dayToTime(date_list[0])
                info['end_time'] = dayToTime(date_list[len(date_list) - 1]) + 24 * 60 * 60 - 1
                print(date_list[len(date_list) - 1])
                for date_tmp in date_list:
                    query_name = db.session.query(flow).filter(flow.identifier == device_key,
                                                               flow.date == date_tmp).all()
                    for query_info in query_name:
                        up = up + query_info.up
                        down = down + query_info.down

                info['enter_num'] = up
                info['leave_num'] = down
                r_json['data'].append(info)
        if stat_interval == 6:
            month_list = get_year_month(mintime)
            for date_list in month_list:
                up = 0
                down = 0
                info = {}
                info['start_time'] = dayToTime(date_list[0])
                info['end_time'] = dayToTime(date_list[len(date_list) - 1]) + 24 * 60 * 60 - 1
                # print(date_list[len(date_list) - 1])
                for date_tmp in date_list:
                    query_name = db.session.query(flow).filter(flow.identifier == device_key,
                                                               flow.date == date_tmp).all()
                    for query_info in query_name:
                        up = up + query_info.up
                        down = down + query_info.down

                info['enter_num'] = up
                info['leave_num'] = down
                r_json['data'].append(info)
    return jsonify(r_json)