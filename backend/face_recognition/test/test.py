# coding: utf-8

# In[1]:
from backend.settings import stream_ip,stream_name
import requests,json
import tensorflow as tf
import numpy as np
from PIL import Image
from backend.face_recognition.test.embeddings import load_model
# sys.path.append('../backend/face_recognition/align/')
from backend.face_recognition.test.MtcnnDetector import MtcnnDetector
from backend.face_recognition.test.detector import Detector
from backend.face_recognition.test.fcn_detector import FcnDetector
from backend.face_recognition.test.model import P_Net, R_Net, O_Net
from backend.face_recognition.test import config
import cv2
from PIL import ImageFont, ImageDraw, Image
import h5py
import time
from datetime import datetime
from backend.models.faceBase import face
from backend.models.deviceBase import device
from backend.views.faceLib import face_box

from sqlalchemy import func
from backend import db
from backend import minio_obj
from backend.models.historyBase import history

# 识别人脸阈值
THRED = 0.003
video = 0
# video="http://admin:admin@10.28.177.12:8081/"
# cv2.namedWindow("camera",1)
# In[2]:

'''
def face_recognition():
    #读取对比图片的embeddings和class_name
    f=h5py.File('backend/face_recognition/pictures/embeddings.h5','r')
    class_arr=f['class_name'][:]
    class_arr=[k.decode() for k in class_arr]
    emb_arr=f['embeddings'][:]
    cap=cv2.VideoCapture(video)
    #fourcc = cv2.VideoWriter_fourcc(*'XVID')
    #path='../output'
    #if not os.path.exists(path):
    #    os.mkdir(path)
    #out = cv2.VideoWriter(path+'/out.mp4' ,fourcc,10,(640,480))
    mtcnn_detector=load_align()
    with tf.Graph().as_default():
        with tf.Session() as sess:
            load_model('backend/face_recognition/test/model')
            images_placeholder = tf.get_default_graph().get_tensor_by_name("input:0")
            embeddings = tf.get_default_graph().get_tensor_by_name("embeddings:0")
            phase_train_placeholder = tf.get_default_graph().get_tensor_by_name("phase_train:0")
            keep_probability_placeholder= tf.get_default_graph().get_tensor_by_name('keep_probability:0')
            while True:
                t1=cv2.getTickCount()
                ret,frame = cap.read()
                if ret == True:
                    img,scaled_arr,bb_arr=align_face(frame,mtcnn_detector)
                    if scaled_arr is not None:
                        feed_dict = { images_placeholder: scaled_arr, phase_train_placeholder:False ,keep_probability_placeholder:1.0}
                        embs = sess.run(embeddings, feed_dict=feed_dict)
                        face_num=embs.shape[0]
                        face_class=['Others']*face_num
                        for i in range(face_num):
                            diff=np.mean(np.square(embs[i]-emb_arr),axis=1)
                            min_diff=min(diff)
                            print(min_diff)
                            if min_diff<THRED:
                                index=np.argmin(diff)
                                face_class[i]=class_arr[index]

                        t2=cv2.getTickCount()
                        t=(t2-t1)/cv2.getTickFrequency()
                        fps=1.0/t
                        for i in range(face_num):
                            bbox=bb_arr[i]
                            fontpath = "backend/face_recognition/data/msyh.ttc" # <== 这里是宋体路径 
                            font = ImageFont.truetype(fontpath, 16)
                            img_pil = Image.fromarray(img)
                            draw = ImageDraw.Draw(img_pil)
                            draw.text((bbox[0], bbox[1] - 20),  face_class[i], font = font,fill=(0,255,0,0))
                            img = np.array(img_pil)
                            #cv2.putText(img, '{}'.format(face_class[i]), 
                            #            (bbox[0], bbox[1] - 2), 
                            #            cv2.FONT_HERSHEY_SIMPLEX,
                            #            0.5,(0, 255, 0), 2)

                                    #画fps值
                            #cv2.putText(img, '{:.4f}'.format(t) + " " + '{:.3f}'.format(fps), (10, 20),
                            #            cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 0, 255), 2)
                            cv2.putText(img, datetime.now().strftime("%Y-%m-%d %H:%M:%S"), (10, 20),
                                        cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 0, 255), 2)
                            cv2.imwrite('backend/face_recognition/result/{}.jpg'.format(datetime.now().strftime("%Y%m%d_%H_%M_%S")), img)
                            print(face_class[i])
                        time.sleep(3)
                    else:
                        img=frame
                        #a = out.write(img)
'''


def face_recognition(video, path, device_name, store, remark, key_device,channel_id):
    # 读取对比图片的embeddings和class_name

    f = h5py.File(path, 'r')
    class_arr = f['class_name'][:]
    class_arr = [k.decode() for k in class_arr]
    emb_arr = f['embeddings'][:]
    cap = cv2.VideoCapture(video)
    # fourcc = cv2.VideoWriter_fourcc(*'XVID')
    # path='../output'
    # if not os.path.exists(path):
    #    os.mkdir(path)
    # out = cv2.VideoWriter(path+'/out.mp4' ,fourcc,10,(640,480))
    mtcnn_detector = load_align()
    with tf.Graph().as_default():
        with tf.Session() as sess:
            load_model('backend/face_recognition/test/model')
            images_placeholder = tf.get_default_graph().get_tensor_by_name("input:0")
            embeddings = tf.get_default_graph().get_tensor_by_name("embeddings:0")
            phase_train_placeholder = tf.get_default_graph().get_tensor_by_name("phase_train:0")
            keep_probability_placeholder = tf.get_default_graph().get_tensor_by_name('keep_probability:0')
            while True:

                # 关闭算法
                device_info = db.session.query(device).filter(device.key == key_device).first()
                if device_info.recognition == "关闭":
                    print("人脸识别算法关闭")
                    to_stream = "{}://{}/{}/{}".format("rtmp", stream_ip, "live", device_info.key)
                    url = "http://" + stream_name + ":6666/api/kill_stream"
                    payload = {
                        "toStream": to_stream
                    }
                    headers = {
                        "Content-Type": "application/json; charset=UTF-8",
                        "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJmcmVzaCI6ZmFsc2UsImlhdCI6MTY2MTc0MzQxMCwianRpIjoiZGNjMjg0ZTgtODhiNC00MzM4LTlkOTUtZWI3ZjU5NWQzMThhIiwidHlwZSI6ImFjY2VzcyIsInN1YiI6ImFkbWluIiwibmJmIjoxNjYxNzQzNDEwLCJleHAiOjQ4MTUzNDM0MTB9.VUOQwVxzHZXKgBu9aHz0D7NWXf19dcMmMDeIck57Yf8"
                    }
                    response = requests.post(url, data=json.dumps(payload), headers=headers).text
                    print(response)
                    break

                cap = cv2.VideoCapture(video)
                # t1=cv2.getTickCount()
                ret, frame = cap.read()
                # 读取每帧图片
                while frame is None:
                    from_stream = "{}://{}:{}@{}".format("rtsp", device_info.account, device_info.password, device_info.address)
                    # "rtsp://admin:bupt123456@192.168.3.64"
                    print(from_stream)
                    to_stream = "{}://{}/{}/{}".format("rtmp", stream_ip, "live", device_info.key)
                    log_name = "log_ffmpeg{}.out".format(device_info.key)

                    # url = "http://10.112.197.219:6666/api/push_stream"
                    url = "http://" + stream_name + ":6666/api/push_stream"
                    payload = {
                        "type": 0,
                        "fromStream": from_stream,
                        "toStream": to_stream,
                        "logName": log_name
                    }
                    headers = {
                        "Content-Type": "application/json; charset=UTF-8",
                        "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJmcmVzaCI6ZmFsc2UsImlhdCI6MTY2MTc0MzQxMCwianRpIjoiZGNjMjg0ZTgtODhiNC00MzM4LTlkOTUtZWI3ZjU5NWQzMThhIiwidHlwZSI6ImFjY2VzcyIsInN1YiI6ImFkbWluIiwibmJmIjoxNjYxNzQzNDEwLCJleHAiOjQ4MTUzNDM0MTB9.VUOQwVxzHZXKgBu9aHz0D7NWXf19dcMmMDeIck57Yf8"
                    }
                    response = requests.post(url, data=json.dumps(payload), headers=headers).text
                    print(response)
                    time.sleep(0.1)
                    capture = cv2.VideoCapture(video)
                    print(video)
                    _, im = capture.read()
                #清除缓存
                db.session.commit()
                if ret == True:
                    img, scaled_arr, bb_arr = align_face(frame, mtcnn_detector)
                    if scaled_arr is not None:
                        feed_dict = { images_placeholder: scaled_arr, phase_train_placeholder:False ,keep_probability_placeholder:1.0}
                        embs = sess.run(embeddings, feed_dict=feed_dict)
                        face_num = embs.shape[0]
                        face_class = ['Others'] * face_num
                        for i in range(face_num):
                            diff = np.mean(np.square(embs[i] - emb_arr), axis=1)
                            min_diff = min(diff)
                            print(min_diff)
                            if min_diff < THRED:
                                index = np.argmin(diff)
                                face_class[i] = class_arr[index]
                        print("人脸数目：", face_num)
                        # t2=cv2.getTickCount()
                        # t=(t2-t1)/cv2.getTickFrequency()
                        # fps=1.0/t
                        for i in range(face_num):
                            # face_key = face_class[i].split('_')[0]
                            # print(face_key)
                            # face_infos = db.session.query(face).filter(face.key == face_key).all()
                            face_key ="images/"+ face_class[i]+".jpg"
                            print("info:"+face_key)
                            face_infos = db.session.query(face).filter(face.photo == face_key).all()
                            if len(face_infos) == 0:
                            # if face_info.count == 0:
                                name = "未知"
                                print(name)
                                age = 0
                                gender = "未知"
                                id = 0
                                part = "未知"
                                phone = "未知"
                                label = "未知"
                            else:
                                face_info = face_infos[0]
                                name = face_info.name
                                print(name)
                                age = face_info.age
                                gender = face_info.gender
                                id = face_info.id
                                part = face_info.part
                                phone = face_info.phone
                                label = face_info.label
                            bbox = bb_arr[i]
                            fontpath = "backend/face_recognition/data/msyh.ttc"  # <== 这里是宋体路径
                            font = ImageFont.truetype(fontpath, 16)
                            img_pil = Image.fromarray(img)
                            draw = ImageDraw.Draw(img_pil)
                            draw.text((bbox[0], bbox[1] - 20), name, font=font, fill=(0, 255, 0, 0))
                            img = np.array(img_pil)
                            # cv2.putText(img, '{}'.format(face_class[i]),
                            #            (bbox[0], bbox[1] - 2),
                            #            cv2.FONT_HERSHEY_SIMPLEX,
                            #            0.5,(0, 255, 0), 2)

                            # 画fps值
                            # cv2.putText(img, '{:.4f}'.format(t) + " " + '{:.3f}'.format(fps), (10, 20),
                            #            cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 0, 255), 2)
                            # cv2.putText(img, datetime.now().strftime("%Y-%m-%d %H:%M:%S"), (10, 20),
                            #           cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 0, 255), 2)
                            # img = Image.fromarray(img)
                            # img_result = open('test.jpg','rb').read()
                            # i = i.tobytes()
                            # print(img)
                            # image_url = face_box(img_result)

                            # print(image_url)
                            # photo = image_url
                            if remark == 'nvr':
                                if device_info.important == '' and name != '未知':
                                    continue
                                elif device_info.stranger == '' and name == '未知':
                                    continue
                            key = db.session.query(func.max(history.key)).first()[0];  # 查询`key`最大值
                            if key is None:
                                key = 0
                            key = key + 3
                            cv2.imwrite('backend/data/history/1.jpg', img)
                            img_url = '/history/{}.jpg'.format(key)
                            minio_obj.upload_file("edge", 'history/{}.jpg'.format(key),
                                                  'backend/data/history/1.jpg', ".jpg")
                            # cv2.imwrite('test.jpg', img)
                            history_info = history(
                                key=key,
                                name=name,
                                age=age,
                                gender=gender,
                                id=id,
                                part=part,
                                phone=phone,
                                photo=img_url,
                                label=label,
                                device=device_name,
                                store=store,
                                remark=remark,
                                time=datetime.now(),
                                key_device=key_device,
                                channel_id=channel_id
                            )
                            db.session.add(history_info)
                            db.session.commit()
                    else:
                        img = frame
                    time.sleep(2)


def load_align():
    thresh = config.thresh
    min_face_size = config.min_face
    stride = config.stride
    test_mode = config.test_mode
    detectors = [None, None, None]
    # 模型放置位置
    model_path = ['backend/face_recognition/test/model/PNet/', 'backend/face_recognition/test/model/RNet/',
                  'backend/face_recognition/test/model/ONet']
    batch_size = config.batches
    PNet = FcnDetector(P_Net, model_path[0])
    detectors[0] = PNet

    if test_mode in ["RNet", "ONet"]:
        RNet = Detector(R_Net, 24, batch_size[1], model_path[1])
        detectors[1] = RNet

    if test_mode == "ONet":
        ONet = Detector(O_Net, 48, batch_size[2], model_path[2])
        detectors[2] = ONet

    mtcnn_detector = MtcnnDetector(detectors=detectors, min_face_size=min_face_size,
                                   stride=stride, threshold=thresh)
    return mtcnn_detector


def align_face(img, mtcnn_detector):
    try:
        boxes_c, _ = mtcnn_detector.detect(img)
    except:
        print('找不到脸')
        return None, None, None
    # 人脸框数量
    num_box = boxes_c.shape[0]
    bb_arr = []
    scaled_arr = []
    if num_box > 0:
        det = boxes_c[:, :4]
        det_arr = []
        img_size = np.asarray(img.shape)[:2]
        for i in range(num_box):
            det_arr.append(np.squeeze(det[i]))

        for i, det in enumerate(det_arr):
            det = np.squeeze(det)
            bb = [int(max(det[0], 0)), int(max(det[1], 0)), int(min(det[2], img_size[1])),
                  int(min(det[3], img_size[0]))]
            cv2.rectangle(img, (bb[0], bb[1]), (bb[2], bb[3]), (0, 255, 0), 2)
            bb_arr.append([bb[0], bb[1]])
            cropped = img[bb[1]:bb[3], bb[0]:bb[2], :]
            scaled = cv2.resize(cropped, (160, 160), interpolation=cv2.INTER_LINEAR)
            scaled = cv2.cvtColor(scaled, cv2.COLOR_BGR2RGB) - 127.5 / 128.0
            scaled_arr.append(scaled)
        scaled_arr = np.array(scaled_arr)
        return img, scaled_arr, bb_arr
    else:
        print('找不到脸 ')
        return None, None, None


if __name__ == '__main__':
    face_recognition()

