import numpy as np
from backend.yolov5_deepsort import objtracker
from backend.yolov5_deepsort.objdetector import Detector
from backend.utils.discernVedio import discernVedio
import cv2
import os
from backend import db
from backend.models.realTimeFlowBase import realTimeFlow
from backend.models.deviceBase import device
from backend.settings import stream_ip,stream_name
import requests,json
import time
from PIL import Image
os.environ["KMP_DUPLICATE_LIB_OK"] = "TRUE"

# VIDEO_PATH = './video/test_person.mp4'

# def PersonNum(Flag,name,channel):
#     while True:
#         #img = input('Input image filename:')
#         cap=cv2.VideoCapture(channel)
#         ret,img = cap.read()
#         img = Image.fromarray(img.astype('uint8')).convert('RGB')
#         out_boxes = yolo.detect_image(img)
#         print(out_boxes)
#         db.session.query(realTimeFlow).filter(realTimeFlow.name == name).update({"num":out_boxes})
#         db.session.commit()



def PersonNum(name,channel,key_device):
    # 根据视频尺寸，填充供撞线计算使用的polygon
    width = 1920
    height = 1080
    mask_image_temp = np.zeros((height, width), dtype=np.uint8)

    # 填充第一个撞线polygon（蓝色）
    list_pts_blue = [[204, 305], [227, 431], [605, 522], [1101, 464], [1900, 601], [1902, 495], [1125, 379], [604, 437],
                     [299, 375], [267, 289]]
    ndarray_pts_blue = np.array(list_pts_blue, np.int32)
    polygon_blue_value_1 = cv2.fillPoly(mask_image_temp, [ndarray_pts_blue], color=1)
    polygon_blue_value_1 = polygon_blue_value_1[:, :, np.newaxis]

    # 填充第二个撞线polygon（黄色）
    mask_image_temp = np.zeros((height, width), dtype=np.uint8)
    list_pts_yellow = [[181, 305], [207, 442], [603, 544], [1107, 485], [1898, 625], [1893, 701], [1101, 568],
                       [594, 637], [118, 483], [109, 303]]
    ndarray_pts_yellow = np.array(list_pts_yellow, np.int32)
    polygon_yellow_value_2 = cv2.fillPoly(mask_image_temp, [ndarray_pts_yellow], color=2)
    polygon_yellow_value_2 = polygon_yellow_value_2[:, :, np.newaxis]

    # 撞线检测用的mask，包含2个polygon，（值范围 0、1、2），供撞线计算使用
    polygon_mask_blue_and_yellow = polygon_blue_value_1 + polygon_yellow_value_2

    # 缩小尺寸，1920x1080->960x540
    polygon_mask_blue_and_yellow = cv2.resize(polygon_mask_blue_and_yellow, (width//2, height//2))

    # 蓝 色盘 b,g,r
    blue_color_plate = [255, 0, 0]
    # 蓝 polygon图片
    blue_image = np.array(polygon_blue_value_1 * blue_color_plate, np.uint8)

    # 黄 色盘
    yellow_color_plate = [0, 255, 255]
    # 黄 polygon图片
    yellow_image = np.array(polygon_yellow_value_2 * yellow_color_plate, np.uint8)

    # 彩色图片（值范围 0-255）
    color_polygons_image = blue_image + yellow_image
    
    # 缩小尺寸，1920x1080->960x540
    color_polygons_image = cv2.resize(color_polygons_image, (width//2, height//2))

    # list 与蓝色polygon重叠
    list_overlapping_blue_polygon = []

    # list 与黄色polygon重叠
    list_overlapping_yellow_polygon = []

    # 下行数量
    down_count = 0
    # 上行数量
    up_count = 0

    font_draw_number = cv2.FONT_HERSHEY_SIMPLEX
    draw_text_postion = (int((width/2) * 0.01), int((height/2) * 0.05))

    # 实例化yolov5检测器
    detector = Detector()



    # 打开视频
    capture = cv2.VideoCapture(channel)
    print("channel:"+channel)
    cnt = 0;
    while True:
        # 关闭算法
        device_info = db.session.query(device).filter(device.key == key_device).first()
        if device_info.detection == "关闭":
            print("turn off")
            to_stream = "{}://{}/{}/{}".format("rtmp", stream_ip, "live", device_info.key)
            url = "http://" + stream_name + ":6666/api/kill_stream"
            payload = {
                "toStream": to_stream
            }
            headers = {
                "Content-Type": "application/json; charset=UTF-8",
                "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJmcmVzaCI6ZmFsc2UsImlhdCI6MTY2MTc0MzQxMCwianRpIjoiZGNjMjg0ZTgtODhiNC00MzM4LTlkOTUtZWI3ZjU5NWQzMThhIiwidHlwZSI6ImFjY2VzcyIsInN1YiI6ImFkbWluIiwibmJmIjoxNjYxNzQzNDEwLCJleHAiOjQ4MTUzNDM0MTB9.VUOQwVxzHZXKgBu9aHz0D7NWXf19dcMmMDeIck57Yf8"
            }
            response = requests.post(url, data=json.dumps(payload), headers=headers).text
            print(response)
            break

        # 读取每帧图片
        _, im = capture.read()
        while im is None:
            from_stream = "/home/td/detection.flv"
            # from_stream = "{}://{}:{}@{}".format("rtsp", device_info.account, device_info.password, device_info.address)
            to_stream = "{}://{}/{}/{}".format("rtmp", stream_ip, "live", device_info.key)
            log_name = "log_ffmpeg{}.out".format(device_info.key)

            # url = "http://10.112.197.219:6666/api/push_stream"
            url = "http://" + stream_name + ":6666/api/push_stream"
            payload = {
                "type": 1,
                "fromStream": from_stream,
                "toStream": to_stream,
                "logName": log_name
            }
            headers = {
                "Content-Type": "application/json; charset=UTF-8",
                "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJmcmVzaCI6ZmFsc2UsImlhdCI6MTY2MTc0MzQxMCwianRpIjoiZGNjMjg0ZTgtODhiNC00MzM4LTlkOTUtZWI3ZjU5NWQzMThhIiwidHlwZSI6ImFjY2VzcyIsInN1YiI6ImFkbWluIiwibmJmIjoxNjYxNzQzNDEwLCJleHAiOjQ4MTUzNDM0MTB9.VUOQwVxzHZXKgBu9aHz0D7NWXf19dcMmMDeIck57Yf8"
            }
            response = requests.post(url, data=json.dumps(payload), headers=headers).text
            print(response)
            time.sleep(0.1)
            capture = cv2.VideoCapture(channel)
            _, im = capture.read()
        # 清除缓存
        db.session.commit()
        cnt = cnt + 1
        if cnt%3!=1:
            # cnt = cnt % 3
            continue
        # 缩小尺寸，1920x1080->960x540
        im = cv2.resize(im, (width//2, height//2))

        list_bboxs = []
        # 更新跟踪器
        output_image_frame, list_bboxs = objtracker.update(detector, im)
        # 输出图片
        output_image_frame = cv2.add(output_image_frame, color_polygons_image)

        person_num = len(list_bboxs)

        if len(list_bboxs) > 0:
            # ----------------------判断撞线----------------------
            for item_bbox in list_bboxs:
                x1, y1, x2, y2, _, track_id = item_bbox
                # 撞线检测点，(x1，y1)，y方向偏移比例 0.0~1.0
                y1_offset = int(y1 + ((y2 - y1) * 0.6))
                # 撞线的点
                y = y1_offset
                x = x1
                if polygon_mask_blue_and_yellow[y, x] == 1:
                    # 如果撞 蓝polygon
                    if track_id not in list_overlapping_blue_polygon:
                        list_overlapping_blue_polygon.append(track_id)
                    # 判断 黄polygon list里是否有此 track_id
                    # 有此track_id，则认为是 UP (上行)方向
                    if track_id in list_overlapping_yellow_polygon:
                        # 上行+1
                        up_count += 1
                        print('up count:', up_count, ', up id:', list_overlapping_yellow_polygon)
                        # 删除 黄polygon list 中的此id
                        list_overlapping_yellow_polygon.remove(track_id)

                elif polygon_mask_blue_and_yellow[y, x] == 2:
                    # 如果撞 黄polygon
                    if track_id not in list_overlapping_yellow_polygon:
                        list_overlapping_yellow_polygon.append(track_id)
                    # 判断 蓝polygon list 里是否有此 track_id
                    # 有此 track_id，则 认为是 DOWN（下行）方向
                    if track_id in list_overlapping_blue_polygon:
                        # 下行+1
                        down_count += 1
                        print('down count:', down_count, ', down id:', list_overlapping_blue_polygon)
                        # 删除 蓝polygon list 中的此id
                        list_overlapping_blue_polygon.remove(track_id)
            # ----------------------清除无用id----------------------
            list_overlapping_all = list_overlapping_yellow_polygon + list_overlapping_blue_polygon
            for id1 in list_overlapping_all:
                is_found = False
                for _, _, _, _, _, bbox_id in list_bboxs:
                    if bbox_id == id1:
                        is_found = True
                if not is_found:
                    # 如果没找到，删除id
                    if id1 in list_overlapping_yellow_polygon:
                        list_overlapping_yellow_polygon.remove(id1)

                    if id1 in list_overlapping_blue_polygon:
                        list_overlapping_blue_polygon.remove(id1)
            list_overlapping_all.clear()
            # 清空list
            list_bboxs.clear()
        else:
            # 如果图像中没有任何的bbox，则清空list
            list_overlapping_blue_polygon.clear()
            list_overlapping_yellow_polygon.clear()
            
        # 输出计数信息
        text_draw = 'DOWN: ' + str(down_count) + \
                    ' , UP: ' + str(up_count) + \
                    ' , SUM: ' + str(person_num)
        output_image_frame = cv2.putText(img=output_image_frame, text=text_draw,
                                         org=draw_text_postion,
                                         fontFace=font_draw_number,
                                         fontScale=0.75, color=(0, 0, 255), thickness=2)
        # cv2.imshow('Counting Demo', output_image_frame)

        # if cnt % 100 == 1:
        #     print(text_draw)
        db.session.query(realTimeFlow).filter(realTimeFlow.name == name).update({"num": person_num,"up":up_count,"down":down_count})
        db.session.commit()
        cv2.waitKey(1)
        # time.sleep(1)

    capture.release()
    # cv2.destroyAllWindows()
