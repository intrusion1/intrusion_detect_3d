# coding=utf-8
#  路由
from backend.views.org import orgnization
from flask import render_template
from backend import app

from backend.views.user import user_bp
from backend.views.faceLib import face_bp
from backend.views.videoSur import face_detect_bp
from backend.views.videoSur import intrusion_channel_bp
from backend.views.history import history_bp
from backend.views.org import org_bp
from backend.views.storeroom import store_bp
from backend.views.device import device_bp
from backend.views.label import label_bp
from backend.views.traffic import traffic_bp
from backend.views.flow import flow_bp
from backend.views.realTimeFlow import real_time_flow_bp
from backend.views.intrusion import intrusion_bp
from backend.views.historyIntrusion import history_intrusion_bp

from flask import(
    redirect,
    url_for,
)

app.register_blueprint(user_bp)
app.register_blueprint(face_bp)
app.register_blueprint(face_detect_bp)
app.register_blueprint(history_bp)
app.register_blueprint(org_bp)
app.register_blueprint(store_bp)
app.register_blueprint(device_bp)
app.register_blueprint(label_bp)
app.register_blueprint(traffic_bp)
app.register_blueprint(flow_bp)
app.register_blueprint(real_time_flow_bp)
app.register_blueprint(intrusion_bp)
app.register_blueprint(history_intrusion_bp)
app.register_blueprint(intrusion_channel_bp)


@app.route('/')
def index():
    return render_template('index.html')