from backend.yolov5_intrusion.detect_class import Detect

def intrusion_detect(channel, area, device, remark):
    detect_area = area.split(',')
    detect = Detect(channel, detect_area, device, remark)
    # opt = detect.parse_opt()
    # print(opt)
    detect.main()