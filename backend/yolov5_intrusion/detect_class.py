import argparse
import os
from datetime import datetime
from pathlib import Path
import time

import numpy as np
import torch

from backend.yolov5_intrusion.models.common import DetectMultiBackend
from backend.yolov5_intrusion.utils.dataloaders import IMG_FORMATS, VID_FORMATS, LoadImages, LoadScreenshots, LoadStreams
from backend.yolov5_intrusion.utils.general import (LOGGER, Profile, check_file, check_img_size, check_imshow, check_requirements, colorstr, cv2,
                           increment_path, non_max_suppression, print_args, scale_boxes, strip_optimizer, xyxy2xywh)
from backend.yolov5_intrusion.utils.plots import Annotator, colors, save_one_box
from backend.yolov5_intrusion.utils.torch_utils import select_device, smart_inference_mode

from backend.models.deviceBase import device as device_table
from backend.models.historyIntrusionBase import history
from backend import db
from sqlalchemy import func
from backend import minio_obj

class Detect(object):
    def __init__(self, channel, area, device, remark):

        self.channel = channel
        self.x1 = int(area[0])
        self.y1 = int(area[1])
        self.x2 = int(area[2])
        self.y2 = int(area[3])
        self.x3 = int(area[4])
        self.y3 = int(area[5])
        self.x4 = int(area[6])
        self.y4 = int(area[7])
        self.device = device
        self.remark = remark
        self.path = os.path.split(os.path.realpath(__file__))[0]
    def run(
            self,
            imgsz=(640, 640),  # inference size (height, width)
            conf_thres=0.25,  # confidence threshold
            iou_thres=0.45,  # NMS IOU threshold
            max_det=1000,  # maximum detections per image
            device='',  # cuda device, i.e. 0 or 0,1,2,3 or cpu
            # view_img=False,  # show results
            nosave=False,  # do not save images/videos
            classes=None,  # filter by class: --class 0, or --class 0 2 3
            name='test',  # save results to project/name
            line_thickness=3,  # bounding box thickness (pixels)
            hide_labels=False,  # hide labels
            vid_stride=1,  # video frame-rate stride
    ):
        weights = self.path + '/yolov5s.pt'
        source = str(self.channel)
        data = self.path + '/data/coco128.yaml'
        project = self.path + '/runs/detect'

        save_img = not nosave and not source.endswith('.txt')  # save inference images
        is_file = Path(source).suffix[1:] in (IMG_FORMATS + VID_FORMATS)
        is_url = source.lower().startswith(('rtsp://', 'rtmp://', 'http://', 'https://'))
        webcam = source.isnumeric() or source.endswith('.txt') or (is_url and not is_file)
        screenshot = source.lower().startswith('screen')
        if is_url and is_file:
            source = check_file(source)  # download

        # Directories
        save_dir = increment_path(Path(project) / name, exist_ok=False)  # increment run
        save_dir.mkdir(parents=True, exist_ok=True)  # make dir

        # Load model
        device = select_device(device)
        model = DetectMultiBackend(weights, device=device, dnn=False, data=data, fp16=False)
        stride, names, pt = model.stride, model.names, model.pt
        imgsz = check_img_size(imgsz, s=stride)  # check image size

        # Dataloader
        bs = 1  # batch_size
        if webcam:
            view_img = check_imshow()
            dataset = LoadStreams(source, img_size=imgsz, stride=stride, auto=pt, vid_stride=vid_stride)
            bs = len(dataset)
        elif screenshot:
            dataset = LoadScreenshots(source, img_size=imgsz, stride=stride, auto=pt)
        else:
            dataset = LoadImages(source, img_size=imgsz, stride=stride, auto=pt, vid_stride=vid_stride)
        vid_path, vid_writer = [None] * bs, [None] * bs

        # Run inference
        model.warmup(imgsz=(1 if pt or model.triton else bs, 3, *imgsz))  # warmup
        seen, windows, dt = 0, [], (Profile(), Profile(), Profile())
        for path, im, im0s, vid_cap, s in dataset:
            time.sleep(5)
            device_info = db.session.query(device_table).filter(device_table.name==self.device).first()
            # print(device_info.intrusion)
            if device_info.intrusion == '关闭':
                print("#区域入侵检测算法关闭#")
                break;
            # mask for certain region
            # 1,2,3,4 分别对应左上，右上，右下，左下四个点
            hl1 = (225 - self.y1) / 225  # 监测区域高度距离图片顶部比例
            wl1 = self.x1 / 400  # 监测区域高度距离图片左部比例
            hl2 = (225 - self.y2) / 225  # 监测区域高度距离图片顶部比例
            wl2 = self.x2 / 400  # 监测区域高度距离图片左部比例
            hl3 = (225 - self.y3) / 225  # 监测区域高度距离图片顶部比例
            wl3 = self.x3 / 400  # 监测区域高度距离图片左部比例
            hl4 = (225 - self.y4) / 225  # 监测区域高度距离图片顶部比例
            wl4 = self.x4 / 400  # 监测区域高度距离图片左部比例
            if webcam:
                for b in range(0, im.shape[0]):
                    mask = np.zeros([im[b].shape[1], im[b].shape[2]], dtype=np.uint8)
                    # mask[round(im[b].shape[1] * hl1):im[b].shape[1], round(img[b].shape[2] * wl1):img[b].shape[2]] = 255
                    pts = np.array([[int(im[b].shape[2] * wl1), int(im[b].shape[1] * hl1)],  # pts1
                                    [int(im[b].shape[2] * wl2), int(im[b].shape[1] * hl2)],  # pts2
                                    [int(im[b].shape[2] * wl3), int(im[b].shape[1] * hl3)],  # pts3
                                    [int(im[b].shape[2] * wl4), int(im[b].shape[1] * hl4)]], np.int32)
                    mask = cv2.fillPoly(mask, [pts], (255, 255, 255))
                    imgc = im[b].transpose((1, 2, 0))
                    imgc = cv2.add(imgc, np.zeros(np.shape(imgc), dtype=np.uint8), mask=mask)
                    im[b] = imgc.transpose((2, 0, 1))

            else:
                mask = np.zeros([im.shape[1], im.shape[2]], dtype=np.uint8)
                # mask[round(im.shape[1] * hl1):im.shape[1], round(im.shape[2] * wl1):im.shape[2]] = 255
                pts = np.array([[int(im.shape[2] * wl1), int(im.shape[1] * hl1)],  # pts1
                                [int(im.shape[2] * wl2), int(im.shape[1] * hl2)],  # pts2
                                [int(im.shape[2] * wl3), int(im.shape[1] * hl3)],  # pts3
                                [int(im.shape[2] * wl4), int(im.shape[1] * hl4)]], np.int32)
                mask = cv2.fillPoly(mask, [pts], (255, 255, 255))
                im = im.transpose((1, 2, 0))
                im = cv2.add(im, np.zeros(np.shape(im), dtype=np.uint8), mask=mask)
                im = im.transpose((2, 0, 1))

            with dt[0]:
                im = torch.from_numpy(im).to(model.device)
                im = im.half() if model.fp16 else im.float()  # uint8 to fp16/32
                im /= 255  # 0 - 255 to 0.0 - 1.0
                if len(im.shape) == 3:
                    im = im[None]  # expand for batch dim

            # Inference
            with dt[1]:
                pred = model(im, augment=False, visualize=False)

            # NMS
            with dt[2]:
                pred = non_max_suppression(pred, conf_thres, iou_thres, classes, False, max_det=max_det)

            # Process predictions
            for i, det in enumerate(pred):  # per image
                seen += 1

                if webcam:  # batch_size >= 1
                    p, s, im0, frame = path[i], f'{i}: ', im0s[i].copy(), dataset.count
                    cv2.putText(im0, "Detection_Region", (int(im0.shape[1] * wl1 - 5), int(im0.shape[0] * hl1 - 5)),
                                cv2.FONT_HERSHEY_SIMPLEX,
                                1.0, (255, 255, 0), 2, cv2.LINE_AA)

                    pts = np.array([[int(im0.shape[1] * wl1), int(im0.shape[0] * hl1)],  # pts1
                                    [int(im0.shape[1] * wl2), int(im0.shape[0] * hl2)],  # pts2
                                    [int(im0.shape[1] * wl3), int(im0.shape[0] * hl3)],  # pts3
                                    [int(im0.shape[1] * wl4), int(im0.shape[0] * hl4)]], np.int32)  # pts4
                    # pts = pts.reshape((-1, 1, 2))
                    zeros = np.zeros(im0.shape, dtype=np.uint8)
                    mask = cv2.fillPoly(zeros, [pts], color=(0, 165, 255))
                    im0 = cv2.addWeighted(im0, 1, mask, 0.2, 0)
                    cv2.polylines(im0, [pts], True, (255, 255, 0), 3)
                    # plot_one_box(dr, im0, label='Detection_Region', color=(0, 255, 0), line_thickness=2)
                else:
                    p, s, im0, frame = path, '', im0s.copy(), getattr(dataset, 'frame', 0)
                    cv2.putText(im0, "Detection_Region", (int(im0.shape[1] * wl1 - 5), int(im0.shape[0] * hl1 - 5)),
                                cv2.FONT_HERSHEY_SIMPLEX,
                                1.0, (255, 255, 0), 2, cv2.LINE_AA)
                    pts = np.array([[int(im0.shape[1] * wl1), int(im0.shape[0] * hl1)],  # pts1
                                    [int(im0.shape[1] * wl2), int(im0.shape[0] * hl3)],  # pts2
                                    [int(im0.shape[1] * wl3), int(im0.shape[0] * hl3)],  # pts3
                                    [int(im0.shape[1] * wl4), int(im0.shape[0] * hl4)]], np.int32)  # pts4
                    # pts = pts.reshape((-1, 1, 2))
                    zeros = np.zeros((im0.shape), dtype=np.uint8)
                    mask = cv2.fillPoly(zeros, [pts], color=(0, 165, 255))
                    im0 = cv2.addWeighted(im0, 1, mask, 0.2, 0)

                    cv2.polylines(im0, [pts], True, (255, 255, 0), 3)

                p = Path(p)  # to Path
                save_path = str(save_dir / p.name)  # im.jpg
                txt_path = str(save_dir / 'labels' / p.stem) + ('' if dataset.mode == 'image' else f'_{frame}')  # im.txt
                s += '%gx%g ' % im.shape[2:]  # print string
                gn = torch.tensor(im0.shape)[[1, 0, 1, 0]]  # normalization gain whwh
                imc = im0  # for save_crop
                annotator = Annotator(im0, line_width=line_thickness, example=str(names))
                if len(det):
                    # Rescale boxes from img_size to im0 size
                    det[:, :4] = scale_boxes(im.shape[2:], det[:, :4], im0.shape).round()

                    # Print results
                    for c in det[:, 5].unique():
                        n = (det[:, 5] == c).sum()  # detections per class
                        s += f"{n} {names[int(c)]}{'s' * (n > 1)}, "  # add to string

                    # Write results
                    for *xyxy, conf, cls in reversed(det):

                        if save_img or view_img:  # Add bbox to image
                            c = int(cls)  # integer class
                            label = None if hide_labels else f'{names[c]} {conf:.2f}'
                            annotator.box_label(xyxy, label, color=colors(c, True))

                if(len(det)):
                    label = s.split(' ')[3][:-1]
                # Stream results
                im0 = annotator.result()

                # Save results (image with detections)
                if save_img:
                    if vid_path[i] != save_path:  # new video
                        vid_path[i] = save_path
                        if isinstance(vid_writer[i], cv2.VideoWriter):
                            vid_writer[i].release()  # release previous video writer
                        fps, w, h = 30, im0.shape[1], im0.shape[0]
                        save_path = str(Path(save_path).with_suffix('.mp4'))  # force *.mp4 suffix on results videos
                        print(save_path)
                        vid_writer[i] = cv2.VideoWriter(save_path, cv2.VideoWriter_fourcc(*"mp4v"), fps, (w, h))
                    vid_writer[i].write(im0)
                if len(det) and (label == 'persons' or label == 'person'):
                    key = db.session.query(func.max(history.key)).first()[0];  # 查询`key`最大值
                    if key is None:
                        key = 0
                    key = key + 1

                    cv2.imwrite('backend/data/history_intrusion/1.jpg', im0)
                    img_url = '/history_intrusion/{}.jpg'.format(key)
                    minio_obj.upload_file("edge", 'history_intrusion/{}.jpg'.format(key), 'backend/data/history_intrusion/1.jpg', "image/jpeg")
                    history_info = history(
                        key=key,
                        photo=img_url,
                        label=label,
                        device=self.device,
                        remark=self.remark,
                        time=datetime.now(),
                    )
                    db.session.add(history_info)
                    db.session.commit()



        # Print results
        t = tuple(x.t / seen * 1E3 for x in dt)  # speeds per image
        LOGGER.info(f'Speed: %.1fms pre-process, %.1fms inference, %.1fms NMS per image at shape {(1, 3, *imgsz)}' % t)



    def main(self):
        check_requirements(exclude=('tensorboard', 'thop'))
        self.run()

