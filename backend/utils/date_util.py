from datetime import datetime, timedelta, date
import calendar
import time
import copy
def get_weekday(today):
    today = datetime.strptime(today, '%Y-%m-%d')    # 将字符串格式化为datetime类型
    weekday = today.weekday()   # 获取输入的日期是周几：int 周一为0
    # print(weekday)
    ret = list()
    # 这周开始的时间：这周开始的日期为今天的日期减去周几，如周一的减0，所以开始日期就是输入日期了
    start_day = today - timedelta(weekday)
    for i in range(7):
        wd = start_day + timedelta(i)   # 从开始日期加一整周的时间
        ret.append(wd.strftime('%Y-%m-%d')) # 转换为字符串后加入列表中
    return ret

def dateTOTime(day):
    # st = "2017-11-23 16:10:10"
    return int(time.mktime(time.strptime(day, "%Y-%m-%d %H:%M:%S")))

def dayToTime(day):
    day = day + " 00:00:00"
    return int(time.mktime(time.strptime(day, "%Y-%m-%d %H:%M:%S")))

def get_month(today_string):
    today = datetime.strptime(today_string, '%Y-%m-%d')    # 将字符串格式化为datetime类型
    day = today.day
    year = today.year
    month = today.month
    monthRange = calendar.monthrange(year, month)
    days_month = monthRange[1]
    # print(weekday)
    ret = list()
    start_day = today - timedelta(day)

    for i in range(days_month):
        wd = start_day + timedelta(i+1)
        ret.append(wd.strftime('%Y-%m-%d')) # 转换为字符串后加入列表中
    return ret

def get_month_week(today_string):
    today = datetime.strptime(today_string, '%Y-%m-%d')  # 将字符串格式化为datetime类型
    day = today.day
    year = today.year
    month = today.month
    monthRange = calendar.monthrange(year, month)
    days_month = monthRange[1]
    # print(weekday)
    ret = list()
    start_day = today - timedelta(day)
    date_list = list()
    for i in range(days_month):
        wd = start_day + timedelta(i+1)
        date_wd = wd.strftime('%Y-%m-%d')
        date_list.append(date_wd)
        if wd.weekday()==6:
            tmp_list = copy.deepcopy(date_list)
            ret.append(tmp_list)
            date_list.clear()
    if len(date_list)!=0:
        ret.append(date_list)
    # print(ret)
    return ret

def get_year_week(today_string):
    today = datetime.strptime(today_string, '%Y-%m-%d')  # 将字符串格式化为datetime类型
    year = today.year
    begin = date(year, 1, 1)
    now = begin
    end = date(year, 12, 31)
    delta = timedelta(days=1)
    ret = list()
    date_list = list()
    while now <= end:
        date_str = now.strftime("%Y-%m-%d")
        date_list.append(date_str)
        if now.weekday()==6:
            tmp_list = copy.deepcopy(date_list)
            ret.append(tmp_list)
            date_list.clear()
        now += delta
    if len(date_list)!=0:
        ret.append(date_list)
    # print(ret)
    return ret

def get_year_month(today_string):
    today = datetime.strptime(today_string, '%Y-%m-%d')  # 将字符串格式化为datetime类型
    year = today.year
    begin = date(year, 1, 1)
    now = begin
    end = date(year, 12, 31)
    delta = timedelta(days=1)
    ret = list()
    date_list = list()
    while now <= end:
        date_str = now.strftime("%Y-%m-%d")
        date_list.append(date_str)
        if now.month!=(now+delta).month:
            tmp_list = copy.deepcopy(date_list)
            ret.append(tmp_list)
            date_list.clear()
        now += delta
    # if len(date_list) != 0:
    #     ret.append(date_list)
    # print(ret[1])
    return ret