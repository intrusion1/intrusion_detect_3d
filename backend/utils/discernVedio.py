
from urllib import request
from urllib.parse import urlparse

import requests


#替换字符串string中指定位置p的字符为c
def sub(string,p,c):
    new = []
    for s in string:
        new.append(s)
    new[p] = c
    return ''.join(new)

def readM3U8(url):
    s_url = None
    response = requests.get(url)
    response.encoding = 'utf-8'
    str = response.text
    result = urlparse(url)
    url_tou = result[0] + '://' + result[1]

    # 获取m3u8中的片段ts文件
    # 需要替换 ../
    list = str.split("\n");
    for str in list:

        if str.find(".ts") != -1:
            # 特殊格式==>回退目录
            if (str.find("../../../../..") != -1):
                s_url = str.replace("../../../../..", url_tou)
            # 普通格式，直接替换，如果ts文件的开头是/则表示根目录
            else:
                if str[0:1] == '/':
                    s_url = sub(str, 0,url_tou+"/")
                else:
                    pos = url.rfind("/")
                    s_url = url.replace(url[pos:], "/"+str)

            break

    return discernVedio(s_url)

# 返回True可播放，false不可播放
def discernVedio(url):
    try:
        with request.urlopen(url,timeout=1) as file:
            if file.status != 200:
                return False
            else:
                return True
    except BaseException as err:
        return False


if __name__ == '__main__':
   print(discernVedio('http://10.112.197.219:8979/live/16.m3u8'))



