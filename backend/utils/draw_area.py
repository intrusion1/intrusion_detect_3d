from PIL import Image,ImageDraw
from backend import minio_obj
import os

def pilImgDraw(filename,x1,y1,x2,y2):
    img = Image.open("./backend/data/area/original/"+filename)
    img.thumbnail((400,225)) #修改图片分辨率
    width = img.width
    height = img.height
    draw = ImageDraw.Draw(img)
    draw.line([(x1, height-y1), (x2, height-y1), (x2, height-y2), (x1, height-y2), (x1, height-y1)], fill=(255, 0, 0), width=3)
    # img.show()
    img.save("./backend/data/area/amendment/"+filename)
    minio_obj.upload_file("edge", 'area/amendment/' + filename, "./backend/data/area/amendment/"+filename, "image/jpeg")

def pilImgDraw_intrusion(filename,x1,y1,x2,y2,x3,y3,x4,y4):
    img = Image.open("./backend/data/intrusion/original/"+filename)
    img.thumbnail((400,225)) #修改图片分辨率
    width = img.width
    height = img.height
    draw = ImageDraw.Draw(img)
    print("开画！！！")
    print(x1)
    print(x2)
    print(y1)
    print(y2)
    draw.line([(x1, height-y1), (x2, height-y2), (x3, height-y3), (x4, height-y4), (x1, height-y1)], fill=(0, 0, 255), width=3)
    # img.show()
    img.save("./backend/data/intrusion/amendment/"+filename)
    minio_obj.upload_file("edge", 'intrusion/amendment/' + filename, "./backend/data/intrusion/amendment/"+filename, "image/jpeg")


