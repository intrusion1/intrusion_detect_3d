from flask import render_template,jsonify
from flask import Blueprint
from flask import url_for
import json

main = Blueprint('main', __name__, template_folder='templates', static_folder='static', static_url_path="")
#main = Blueprint('main', __name__, template_folder='../front_end/public', static_folder='../front_end', static_url_path="")

#@main.route('/', defaults={'path': ''})
@main.route('/')
def index():
      return render_template('index.html')
