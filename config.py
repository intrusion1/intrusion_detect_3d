# coding=utf-8
# 服务器参数配置

import os
# from backend.views.realTimeFlow import schedule_task
from backend.settings import mysql_uri, jwt_expires
basedir = os.path.abspath(os.path.dirname(__file__))


class Config(object):
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'you-will-never-guess'
    POSTS_PER_PAGE = 100
    # 本地数据库
    '''
    HOSTNAME = 'localhost'
    PORT = '3306'
    DATABASE = 'face'
    USERNAME = 'root'
    PASSWORD = 'fish'
    DB_URI = 'mysql+pymysql://{}:{}@{}:{}/{}'.format(USERNAME, PASSWORD, HOSTNAME, PORT, DATABASE)
    SQLALCHEMY_DATABASE_URI = DB_URI
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    ALLOWED_EXTENSIONS = {'png', 'jpg', 'jpeg'}
    '''
    # HOSTNAME = '10.112.197.219'
    # PORT = '3306'
    # DATABASE = 'face'
    # USERNAME = 'root'
    # PASSWORD = 'Douyin2022'
    # DB_URI = 'mysql+mysqldb://{}:{}@{}:{}/{}'.format(USERNAME, PASSWORD, HOSTNAME, PORT, DATABASE)
    DB_URI = mysql_uri()
    SQLALCHEMY_DATABASE_URI = DB_URI
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    ALLOWED_EXTENSIONS = {'png', 'jpg', 'jpeg'}

    SCHEDULER_API_ENABLED = True
    SCHEDULER_TIMEZONE = 'Asia/Shanghai'
    JWT_SECRET_KEY = 'jwt_bupt'  #os.environ.get('JWT_SECRET_KEY') or
    JWT_COOKIE_CSRF_PROTECT = True
    JWT_CSRF_CHECK_FORM = True
    JWT_ACCESS_TOKEN_EXPIRES = jwt_expires() #os.environ.get('JWT_ACCESS_TOKEN_EXPIRES') or
    PROPAGATE_EXCEPTIONS = True

    
class CeleryConfig(object):
    BROKER_URL = 'redis://localhost:6379/0'  # 使用RabbitMQ作为消息代理
    CELERY_RESULT_BACKEND = 'redis://localhost:6379/0'  # 把任务结果存在了Redis
    CELERY_TASK_SERIALIZER = 'msgpack'  # 任务序列化和反序列化使用msgpack方案
    CELERY_RESULT_SERIALIZER = 'json'  # 读取任务结果一般性能要求不高，所以使用了可读性更好的JSON
    CELERY_TASK_RESULT_EXPIRES = 60 * 60 * 24  # 任务过期时间，不建议直接写86400，应该让这样的magic数字表述更明显
    CELERY_ACCEPT_CONTENT = ['json', 'msgpack']  # 指定接受的内容类型
