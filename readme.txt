backend ：后端代码
	views ：接口
	models ：数据表结构
	face_recognition ：人脸识别脚本
	yolov5_deepsort ：人流量检测脚本
	yolov5_intrusion ：区域入侵检测脚本
	data ：模型文件
	requirements.txt ：环境的相关安装包——pip install -r r  .txt 或 conda install --yes --file r.txt


front ：前端代码
	package.json ：前端代码的相关依赖——yarn install
	src/views：前端界面代码

app.py ：服务启动——python app.py

config.py ：数据库配置文件