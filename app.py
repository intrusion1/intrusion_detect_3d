from backend import app
import flask_cors
import torch
import threading
flask_cors.CORS(app)

# from backend.views.device import autoUpdateNum, autoFaceRecognition, autoIntrusionDetect

# def start_runner():
#     def start_loop():
#         autoUpdateNum()
#     thread = threading.Thread(target=start_loop)
#     thread.start()


if __name__ == "__main__":
    # start_runner()
    torch.multiprocessing.set_start_method('spawn')
    app.run(host='0.0.0.0',
            port=5000,
            debug=True,
            threaded=True
            , use_reloader=False)